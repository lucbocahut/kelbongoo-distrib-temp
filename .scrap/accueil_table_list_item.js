var ACCUEIL_SHOW_GREEN_AFTER_TABLED = 30;

Template.accueil_table_list_item.onCreated(function(){

	this.justArrived = new ReactiveVar(true);
});

Template.accueil_table_list_item.onRendered(function(){

	var self = this;

	Meteor.setTimeout(function(){
		self.justArrived.set(false);
	}, ACCUEIL_SHOW_GREEN_AFTER_TABLED*1000);
});

Template.accueil_table_list_item.helpers({
	justArrived : function(){
		var t = Template.instance();
		return t.justArrived.get();
	}
});

Template.accueil_table_list_item.events({
	'touchend .name-container' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .name-container' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			Utils.confirm("Etes-vous sur de vouloir enlever "+t.data.lastname+" " + t.data.firstname + " de sa table ?", function(){
				Meteor.call('orders.undoOrderArrival', "BOR", t.data.order_id, function(err){
					if(err) return Utils.handleResponse(err);
				});
			});
		}, 800);
		return false;
	},
	'mouseup .name-container' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .name-container' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			Utils.confirm("Etes-vous sur de vouloir enlever "+t.data.lastname+" " + t.data.firstname + " de sa table ?", function(){
				Meteor.call('orders.undoOrderArrival', "BOR", t.data.order_id, function(err){
					if(err) return Utils.handleResponse(err);
				});
			});
		}, 800);
		return false;
	}
});
