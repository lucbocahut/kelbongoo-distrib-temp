Template.accueil_waiting_list_item.onCreated(function(){

	var self = this;
	self.waitingTime = new ReactiveVar(0);
	self.waitingTimer = Meteor.setInterval(function(){
		self.waitingTime.set(self.waitingTime.get() + 1);
	}, 60*1000);
});

Template.accueil_waiting_list_item.helpers({
	waitingTime : function(){
		var t = Template.instance();
		return t.waitingTime.get();
	}
});

Template.accueil_waiting_list_item.events({
	'touchend .waiting-list-item' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .waiting-list-item' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;

			Utils.confirm("Etes-vous sur de vouloir enlever cette commande da la queue ?", function(){
				Meteor.call('orders.undoOrderArrival', "BOR", t.data.order_id, function(err){
					if(err) return Utils.handleResponse(err);
				});
			});
		}, 800);
		return false;
	},
	'mouseup .waiting-list-item' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .waiting-list-item' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			Utils.confirm("Etes-vous sur de vouloir enlever cette commande da la queue ?", function(){
				Meteor.call('orders.undoOrderArrival', "BOR", t.data.order_id, function(err){
					if(err) return Utils.handleResponse(err);
				});
			});
		}, 800);
		return false;
	}
});

Template.accueil_waiting_list_item.onDestroyed(function(){
	Meteor.clearInterval(this.waitingTimer);
});
