Template.distribution_header.onCreated(function(){

	this.globalorder_id = Session.get('currentGlobalOrder');
	this.subscribe('globalorders.detail', "BOR", this.globalorder_id);
	this.subscribe('globalorders.detail.counts', "BOR", this.globalorder_id);
});

Template.distribution_header.helpers({
	ordersArrivedPct : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		var ratio = count && (count.arrived-count.departed)/count.total;
		return count && Math.round(ratio*100);
	},
	ordersDonePct : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		var ratio = count && count.departed/count.total;
		return count && Math.round(ratio*100);
	},
	currentRole : function(){
		FlowRouter.watchPathChange();
		var path = FlowRouter.current().path.split('/');
		var role = path[1] === 'distribution' && path.length === 3 && path[2].split('?')[0];
		if(role === 'couloir'){
			return role + '_' + FlowRouter.getQueryParam('couloir');
		}
		if(role === 'table'){
			return role + '_' + FlowRouter.getQueryParam('table');
		}
		return role;
	}
});

Template.distribution_header.events({
	'change #role' : function(e,t){
		var val = $(e.currentTarget).val();
		var path = '/distribution/';

		Session.set('currentDistributionRole', val);

		if(~val.indexOf('couloir')) path += 'couloir?couloir='+val.split('_')[1];
		else if(~val.indexOf('table')) path += 'table?table='+val.split('_')[1];
		else path += val;

		FlowRouter.go(path);
	}
});