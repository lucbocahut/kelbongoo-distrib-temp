Template.order_remove.onCreated(function(){
	this.running = new ReactiveVar(false);
});

Template.order_remove.helpers({
	running : function(){
		var t = Template.instance();
		return t.running.get();
	}
});

Template.order_remove.events({
	'click .remove-order' : function(e,t){

		if(!this.params.allowRemove) return;
		
		if(confirm("Voulez-vous vraiment annuler cette commande ?")){

			t.running.set(true);

			Meteor.call('orders.remove', "BOR", this.order._id, function(err){
				t.running.set(false);
				Utils.handleResponse(err);
				if(!err) FlowRouter.go('/consumers/orders');
			});
		}
	}
});