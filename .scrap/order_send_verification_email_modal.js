Template.order_send_verification_email_modal.events({

	'click .validate' : function(e,t){

		var email = t.$('#email').val().trim();
		var regex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
		var $modal = t.$('.modal');

		if(!email || !regex.test(email)) return;

		Meteor.call('orders.resendVerificationEmail', "BOR", t.data.order._id, email, function(err){
			Utils.handleResponse(err);
			$modal.modal('hide');
		});
	}
});