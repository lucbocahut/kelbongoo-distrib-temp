// LOGIN

var LoginSchema = new SimpleSchema({
	username : {
		type : String
	},
	password : {
		type : String,
		label : 'Mot de passe'
	}
});

Template.login.onCreated(function(){
	this.isLoading = new ReactiveVar(false);
	this.schema = LoginSchema.newContext();
});

Template.login.helpers({
	'schema' : function(){
		var t = Template.instance();
		return t.schema;
	},
	'keyIsValid' : Utils.keyIsValid,
	'isLoading' : function(){
		var t = Template.instance();
		return t.isLoading.get();
	}
});

Template.login.events({
	'click .validate' : function(e,t){
		e.preventDefault();
		e.stopPropagation();
		var $form = t.$('form'),
		data = $form && Utils.serializeObject($form);
		
		t.isLoading.set(true);
		if(data && t.schema.validate(data)){
			Meteor.loginWithPassword(data.username, data.password, function(err){
				t.isLoading.set(false);
				if(err) {
					console.log('err', err);
					switch(err.reason){
						case "User not found" : Utils.handleResponse(new Error('Utilisateur non trouvé')); break;
						case "Incorrect password" : Utils.handleResponse(new Error('Mot de passe incorrect')); break;
						default : Utils.handleResponse(new Error('La connection a echouée')); break;
					}
				} else {
					FlowRouter.go('/');
				}
			});
		}
	},
	'blur input' : Utils.blurInputHandler
});