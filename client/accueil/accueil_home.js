Template.accueil_home.onCreated(function(){

	var self = this;
	self.currentFilter = new ReactiveVar([{
		title: 'Commandes pas encore arrivées',
		name: 'default',
		active: true
	},{
		title: 'Toutes les commandes pour le local',
		name: 'all-locale',
		active: false
	},{
		title: 'Toutes les commandes pour la globale',
		name: 'all-globalorder',
		active: false
	}]);
	self.currentSearch = new ReactiveVar();
	self.globalorder_id = this.currentGlobalOrder;
	self.autorun(function(){
		var search = self.currentSearch.get();
		var type = self.currentFilter.get().reduce((arr, item) => {
			if(item.active && item.name !== 'default'){
				arr = item.name;
			}
			return arr;
		}, 'default');
		self.subscribe('accueil.home.list', self.currentLocale(), self.currentGlobalOrder(), {
			type: type,
			search: search
		});
	});
	self.subscribe('accueil.home.recent', self.currentLocale(), self.currentGlobalOrder());
});

Template.accueil_home.helpers({
	datatableParams: function(){
		var t = Template.instance();
		return {
			columns: [{
				title: 'Nom'
			},{
				title: 'Non-dispatch',
				classes: 'text-center'
			},{
				title: 'Manquant',
				classes: 'text-center'
			},{
				title: '',
				width: '100px',
				classes: 'text-center'
			}],
			filterOptions: [],
			rowTemplateName: 'accueil_home_datatable_row',
			toggleFilter: function(filter){
				var currentFilter = t.currentFilter.get();
				var currentIndex = currentFilter.findIndex(fil => fil.name === filter);
				currentFilter = currentFilter.map((item, index) => {
					if(currentIndex !== index){
						item.active = false
					}
					else {
						item.active = !item.active;
					}
					return item;
				});
				t.currentFilter.set(currentFilter);
			},
			updateSearch: function(search){
				t.currentSearch.set(search);
			}
		}
	},
	datatableFilters: function(){
		var t = Template.instance();
		return t.currentFilter.get();
	},
	datatableItems: function(){
		return Orders.find({ arrivedAt: { $exists: false }});
	}
});

// Template.accueil_home.events({
// 	'click tbody > tr': function (e,t) {
//   	var dataTable = $(e.target).closest('table').DataTable();
//   	var data = dataTable.row(e.currentTarget).data();
//   	if (!data) return;
//   	FlowRouter.go('/distribution/accueil/'+data._id._str);
// 	},
// 	'change #status_filter' : function(e,t){
// 		var val = Number($(e.currentTarget).val());
// 		t.filter.set(val);
// 	},
// 	'click .clear-status-filter' : function(e,t){
// 		t.filter.set();
// 		t.$('#status_filter')[0].selectedIndex = '0';
// 	}
// });

// function addOrderToLocalePlace(localeplace){
// 	var order = localeplace.getCurrentOrder();
// 	return Object.assign(
// 					order || {},
// 					{
// 						table: localeplace.table,
// 						place: localeplace.place,
// 						alias: localeplace.alias
// 					}
// 				);
// }
//
// function groupByTable(acc, order){
// 	if(!acc[order.table]){
// 		acc[order.table] = [];
// 	}
// 	acc[order.table].push(order);
// 	return acc;
// }

Meteor.methods({
	'orders.markArrived': function(locale, order_id){
		return Orders.update(order_id, { $set: { arrivedAt: new Date() }})
	}
})
