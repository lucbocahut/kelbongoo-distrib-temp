Template.accueil_home_consumers_present_list.helpers({
  consumersPresent : function(){
    var cursor = Orders.find({ status: 3 }, { sort: { lastname: 1, firstname: 1 }});
    return cursor.map(function(order){
      var localeplace = LocalePlaces.findOne({ order_id: order._id });
      if(localeplace){
        order.table = localeplace.getTableAlias();
        order.place = localeplace.getFormattedPlaceNumber();
      }
      return order;
    });
  }
})
