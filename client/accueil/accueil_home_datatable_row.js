Template.accueil_home_datatable_row.helpers({
  showAlert: function(){
    if(this.status === 2){
      return;
    }
    if(this.status === 3){
      return { iconName: 'home', type: 'info' };
    }
    if(this.status === 4){
      return { iconName: 'ok', type: 'success'};
    }
    if(this.status < 2){
      return { iconName: 'trash', type: 'muted' };
    }
    if(this.status === 6){
      return { iconName: 'remove', type: 'danger' }
    }
    if(this.status > 6){
      return { iconName: 'warning-sign', type: 'warning' }
    }
  },
  showLocale: function(){
    var t = Template.instance();
    return this.locale !== t.currentLocale();
  }
})

Template.accueil_home_datatable_row.events({
  'click .mark-arrived': function(e,t){
    var order = t.data;
    return order.markArrived();
  },
  'click .nondispatch-list': function(e,t){
    Utils.createModal('order_special_items_modal', t.data);
  },
  'click .missing-list': function(e,t){
    Utils.createModal('order_special_items_modal', t.data);
  },
});
