Template.accueil_order.onCreated(function(){

	this.globalorder_id = new Mongo.ObjectID(FlowRouter.getParam('globalorder_id'));
	this.order_id = new Mongo.ObjectID(FlowRouter.getParam('order_id'));
	console.log('order_id', this.order_id);
	this.subscribe('orders.detail', this.currentLocale(), this.order_id);
});

Template.accueil_order.helpers({
	currentOrder : function(){
		var t = Template.instance();
		console.log('order', Orders.findOne(t.order_id));
		return Orders.findOne(t.order_id);
	},
	params : function(){
		return {
			allowUpdate : false,
			allowPayment: true
		};
	}
});
