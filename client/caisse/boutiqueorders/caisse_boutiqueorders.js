Template.caisse_boutiqueorders.onRendered(function(){

	var self = this;
	var $table = self.$('table');
	var table = $table.DataTable();

	self.$('.dataTables_wrapper .row:first-child .col-xs-6:first-child').removeClass('col-xs-6');
	self.$('.dataTables_wrapper .row:first-child .col-xs-6').addClass('col-xs-12').removeClass('col-xs-6');
	self.$('.dataTables_filter input[type=search]').removeClass('input-sm').addClass('input-lg datatables-search-input').attr('placeholder', "Rechercher").css({'width': '100%', 'margin-left' : '0px', 'margin-bottom' : '15px' });
	self.$('.dataTables_filter label').css({ width : '100%' });
	self.$('table.dataTable').css({ width : '100%' });
});

Template.caisse_boutiqueorders.helpers({
	selector : function(){
		return {
			globalorder_id : Session.get('currentGlobalOrder')
		};
	}
});

Template.caisse_boutiqueorders.events({
	'click tbody > tr': function (e,t) {
    	var dataTable = $(e.target).closest('table').DataTable();
    	var data = dataTable.row(e.currentTarget).data();
    	if (!data) return;
    	FlowRouter.go('/caisse/boutiqueorders/'+data._id._str);
  	}
});
