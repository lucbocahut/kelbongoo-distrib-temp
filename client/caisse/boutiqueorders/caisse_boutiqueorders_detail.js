Template.caisse_boutiqueorders_detail.onCreated(function(){

	this.order_id = new Mongo.ObjectID(FlowRouter.getParam('boutiqueorder_id'));
	this.subscribe('caisse.boutiqueorders.detail', this.currentLocale(), this.order_id);
});

Template.caisse_boutiqueorders_detail.helpers({
	order : function(){
		var t = Template.instance();
		return BoutiqueOrders.findOne(t.order_id);
	},
	params : function(){
		var t = Template.instance();
		var order = BoutiqueOrders.findOne(t.order_id);
		return {
			allowUpdate : !order || !order.paidAt,
			allowPayment : !order || !order.paidAt
		};
	}
});
