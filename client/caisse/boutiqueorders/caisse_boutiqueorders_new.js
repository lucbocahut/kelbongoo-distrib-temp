Template.caisse_boutiqueorders_new.onCreated(function(){
	this.order_id = _BoutiqueOrders.insert({
		items : [],
		temp : true,
		amount_total : 0,
		units_total : 0,
		globalorder_id : Session.get('currentGlobalOrder')
	});
});

Template.caisse_boutiqueorders_new.helpers({

	currentOrder : function(){
		var t = Template.instance();
		return _BoutiqueOrders.findOne(t.order_id);
	},
	params : function(){
		return {
			showPayment : true,
			allowUpdate : true,
			paymentCallback : function(){
				FlowRouter.go('/distribution/caisse/boutiqueorders');
			}
		};
	}
});
