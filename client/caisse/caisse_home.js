Template.caisse_home.onCreated(function(){

	var self = this;
	self.currentFilter = new ReactiveVar([{
		title: 'Commandes arrivées mais pas encore payées',
		name: 'default',
		active: true
	},{
		title: 'Toutes les commandes pour le local',
		name: 'all-locale',
		active: false
	},{
		title: 'Toutes les commandes pour la globale',
		name: 'all-globalorder',
		active: false
	}]);
	self.currentSearch = new ReactiveVar();
	self.globalorder_id = this.currentGlobalOrder;
	self.autorun(function(){
		var search = self.currentSearch.get();
		var type = self.currentFilter.get().reduce((arr, item) => {
			if(item.active && item.name !== 'default'){
				arr = item.name;
			}
			return arr;
		}, 'default');
		self.subscribe('caisse.home', self.currentLocale(), self.currentGlobalOrder(), {
			type: type,
			search: search
		});
	})
});

// Template.caisse_home.onRendered(function(){
//
// 	var self = this;
// 	var $table = self.$('table');
// 	var table = $table.DataTable();
//
// 	self.$('.dataTables_wrapper .row:first-child .col-xs-6:first-child').removeClass('col-xs-6');
// 	self.$('.dataTables_wrapper .row:first-child .col-xs-6').addClass('col-xs-12').removeClass('col-xs-6');
// 	self.$('.dataTables_filter input[type=search]').removeClass('input-sm').wrap('<div class="input-group input-group-lg search-wrapper"></div>').addClass('datatables-search-input').attr('placeholder', "Rechercher").css({'width': '100%', 'margin-left': 0 });
// 	self.$('.search-wrapper').append('<div class="input-group-btn"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-cog"></span></button><ul class="dropdown-menu dropdown-menu-right block" aria-expanded="false"><li><a href="#">Action</a></li><li><a href="#">Another action</a></li><li><a href="#">Something else here</a></li></ul></div>')
// 	self.$('.dataTables_filter label').css({ width : '100%' });
// 	self.$('table.dataTable').css({ width : '100%' });
//
//
// 	table.columns(1).visible(false);
// 	table.columns(2).visible(false);
// 	table.columns(3).visible(false);
// 	table.columns(6).visible(false);
// 	table.columns(9).visible(false);
// });

Template.caisse_home.helpers({
	datatableParams: function(){
		var t = Template.instance();
		return {
			columns: [{
				title: 'Nom'
			},{
				title: 'Remisés',
				classes: 'text-center'
			},{
				title: 'Manquant',
				classes: 'text-center'
			},{
				title: 'Montant',
				width: '80px',
				classes: 'text-center'
			}],
			filterOptions: [],
			rowTemplateName: 'caisse_home_datatable_row',
			toggleFilter: function(filter){
				var currentFilter = t.currentFilter.get();
				var currentIndex = currentFilter.findIndex(fil => fil.name === filter);
				currentFilter = currentFilter.map((item, index) => {
					if(currentIndex !== index){
						item.active = false
					}
					else {
						item.active = !item.active;
					}
					return item;
				});
				t.currentFilter.set(currentFilter);
			},
			updateSearch: function(search){
				t.currentSearch.set(search);
			}
		}
	},
	datatableFilters: function(){
		var t = Template.instance();
		return t.currentFilter.get();
	},
	datatableItems: function(){
		return Orders.find();
	}
});

Template.caisse_home.events({
	// 'click tbody > tr': function (e,t) {
  //   	var dataTable = $(e.target).closest('table').DataTable();
  //   	var data = dataTable.row(e.currentTarget).data();
  //   	if (!data) return;
  //   	FlowRouter.go('/caisse/'+data._id._str);
  // 	},
	// 'change #status_filter' : function(e,t){
  // 		var val = Number($(e.currentTarget).val());
  // 		t.filter.set(val);
  // 	},
  // 	'click .clear-status-filter' : function(e,t){
  // 		t.filter.set();
  // 		t.$('#status_filter')[0].selectedIndex = '0';
  // 	}
});
