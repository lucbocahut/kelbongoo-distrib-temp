Template.caisse_order.onCreated(function(){
	this.globalorder_id = new Mongo.ObjectID(FlowRouter.getParam('globalorder_id'));
	this.order_id = new Mongo.ObjectID(FlowRouter.getParam('order_id'));

	this.subscribe('orders.detail', this.currentLocale(), this.order_id);
});

Template.caisse_order.helpers({
	currentOrder : function(){
		var t = Template.instance();
		return Orders.findOne(t.order_id);
	},
	params : function(){
		return {
			allowUpdate : true,
			allowPayment : true,
			paymentCallback : function(){
				FlowRouter.go('/distribution/caisse');
			}
		};
	}
});
