Template.couloir_bacs.onCreated(function(){
  var self = this;
  self.selectedBacs = new ReactiveVar([]);
  self.isBacSelected = function(number){
    var selectedBacs = self.selectedBacs.get();
    return selectedBacs.includes(number);
  }
  self.toggleBacSelected = function(number){
    var selectedBacs = new Set(self.selectedBacs.get());
    if(selectedBacs.has(number)){
      selectedBacs.delete(number);
    }
    else {
      selectedBacs.add(number);
    }
    self.selectedBacs.set([...selectedBacs]);
  }
})

Template.couloir_bacs.helpers({
  bacs: function(){
    var t = Template.instance();
    var selectedBacs = t.selectedBacs.get();
    return this.bacsToPickup.map(bac => {
      return Object.assign(bac, {
        selected: t.isBacSelected(bac.number),
        toggleSelected(){
          return t.toggleBacSelected(bac.number)
        }
      });
    });
  },
  showValidate(){
    var t = Template.instance();
    var selectedBacs = t.selectedBacs.get();
    return selectedBacs.length > 0;
  }
})

Template.couloir_bacs.events({
  'click .validate-pickup': function(e,t){
    var orderbacNumbers = t.selectedBacs.get();
    return t.data.validatePickup(orderbacNumbers);
  }
})
