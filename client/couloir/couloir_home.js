const DEFAULT_MAX_BACS = 8;
const DEFAULT_SORT = 'arrival';

Template.couloir_home.onCreated(function(){

	var self = this;
	self.globalorder_id = this.currentGlobalOrder();

	self.currentMaxBacs = new ReactiveVar(
		Session.get('currentMaxBacs')||DEFAULT_MAX_BACS
	);
	self.currentCouloir = new ReactiveVar(
		typeof Session.get('currentCouloir') !== 'undefined'
		? Session.get('currentCouloir')
		: 0
	);
	self.currentSort = new ReactiveVar(DEFAULT_SORT);

	self.showPlaces = new ReactiveVar(false);
	self.bacsStranded = new ReactiveVar([]);
	self.bacsStrandedTimer;

	self.autorun(function(){
		var currentCouloir = self.currentCouloir.get(); // reactive
		var locale = Locales.findOne({ code: self.currentLocale() }); // reactive
		self.subscribe('couloir.home', self.currentLocale(), self.globalorder_id, currentCouloir);

		if(locale){
			self.bacsStrandedTimer = Meteor.setInterval(function(){
				var expiration = moment().subtract(locale.getBacStrandedTimeout(), 'minutes').toDate();
				var strandedBacNumbers = OrderBacs.find({
												couloir: currentCouloir,
												pickedUpAt: { $gt: expiration },
												deliveredAt: { $exists: false }
											}).map(orderbac => orderbac.number);
				self.bacsStranded.set(strandedBacNumbers);

			}, 60*1000);
		}
	})
});

Template.couloir_home.helpers({

	currentMaxBacs : function(){
		var t = Template.instance();
		return t.currentMaxBacs.get();
	},
	bacsStranded: function(){
		var t = Template.instance();
		return t.bacsStranded.get();
	},
	showPlaces : function(){
		var t = Template.instance();
		return t.showPlaces.get();
	},
	currentSort: function(){
		var t = Template.instance();
		return t.currentSort.get();
	},

	bacsToPickup : function(){

		var t = Template.instance();
		var couloir = t.currentCouloir.get(); // reactive
		var currentMaxBacs = t.currentMaxBacs.get(); // reactive
		var currentSort = t.currentSort.get(); // reactive
		var selector = {
			ready: { $exists: true },
			pickedUpAt: { $exists: false },
			couloir: couloir
		};
		var sort = currentSort === 'arrival'
								? { arrivedAt: 1 }
								: { number: 1 };

		var options = {
			sort,
			limit: currentMaxBacs
		}

		return OrderBacs.find(selector, options)
										.map(function(orderbac){
											/* add 'toggleOrderBacPickedUp' method */
											return orderbac.formatForPickup();
										});
	},
	bacsToDeliver : function(){

		var t = Template.instance();
		var couloir = t.currentCouloir.get(); // reactive

		var selector = {
			pickedUpAt: { $exists: true },
			deliveredAt: { $exists: false },
			couloir: couloir
		};

		return OrderBacs.find(selector)
										.fetch()
										.slice()
										.sort((a,b) => a.getPlaceIndex() > b.getPlaceIndex())
										.map(function(orderbac){
											/* add 'localeplace' reference */
											return orderbac.formatForDelivery();
										})

	},
	validatePickup(selectedBacNumbers){
		var t = Template.instance();
		var couloir = t.currentCouloir.get();
		var orderbacIds = OrderBacs.getReadyBacs({
												couloir,
												number: { $in: selectedBacNumbers }
											})
											.map(orderbac => orderbac._id);

		return OrderBacs.batchMarkPickedUp(couloir, orderbacIds);
	},
	markDeliveredOrWaiting(){
		var deliverableBacIds = OrderBacs.getDeliverableBacs()
									.map(bac => bac._id);
		var waitableBacIds = OrderBacs.getWaitableBacs()
									.map(bac => bac._id);
		return () => {
			OrderBacs.batchMarkDeliveredOrWaiting(deliverableBacIds, waitingBacIds);
		}
	}
});

Template.couloir_home.events({

	'click .max-select' : function(e,t){
		var val = Number($(e.currentTarget).prop('id'));
		if(isNaN(val)){
			return;
		}
		t.currentMaxBacs.set(val);
	},
	'change #couloir': function(e,t){
		var val = Number($(e.currentTarget).val());
		if(isNaN(val)){
			return;
		}
		t.currentCouloir.set(val);
		Session.set('currentCouloir', val);
	},
	'click .couloir-sort': function(e,t){
		var sort = $(e.currentTarget).data('sort');
		t.currentSort.set(sort);
	},
	'click .restock-bac': function(e,t){
		Utils.createModal('couloir_home_restock_bac_modal', undefined)
	}
});
