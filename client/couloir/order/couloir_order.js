Template.couloir_order.onCreated(function(){
	this.order_id = new Mongo.ObjectID(FlowRouter.getParam('order_id'));
	this.subscribe('orders.detail', this.currentLocale(), this.order_id);
});

Template.couloir_order.helpers({
	currentOrder : function(){
		var t = Template.instance();
		return Orders.findOne(t.order_id);
	},
	params : function(){
		return {
			hideDistributionDate : true,
			showBacs : true,
			showNonDispatch : true
		};
	}
});
