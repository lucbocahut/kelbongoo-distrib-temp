Template.couloir_tables_order.helpers({
  bacNumbers: function(){
    return this.bacs.map(function(bac){
      return bac.number
    }).join('-');
  },
  nondispatch: function(){
    return this.bacs.find(function(bac){
      return bac.nondispatch;
    });
  },
  delivered: function(){
		return this.bacs.every(function(orderbac){
			return !!orderbac.isDelivered();
		});
	},
});


Template.couloir_tables_order.events({
	'click .place-container' : function(e,t){

		t.data.toggleCurrentBacsDelivered();
	}
});
