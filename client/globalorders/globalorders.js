Template.globalorders.onCreated(function(){

	this.selectedLocale = new ReactiveVar();
	this.selectedGlobalOrder = new ReactiveVar();
	this.subscribe('globalorders');
});

Template.globalorders.helpers({
	locales : function(){

		return Locales.find();
	},
	globalorderlocales : function(){

		var t = Template.instance();
		var selectedLocale = t.selectedLocale.get();
		if(!selectedLocale) return;

		return GlobalOrderLocales.find({ locale : selectedLocale.code }).map(function(gol){
			return {
				globalorder_id : gol.globalorder_id,
				distribution_date : moment(gol.distribution[0].start, "YYYY-MM-DD HH:mm").format("dddd DD MMMM"),
			};
		});
	},
	selectedLocale : function(){
		var t = Template.instance();
		return t.selectedLocale.get();
	},
	selectedGlobalOrder : function(){
		var t = Template.instance();
		return t.selectedGlobalOrder.get();
	},
	employee : function(){
		Employees.findOne({ user_id : Meteor.userId() });
	}
});

Template.globalorders.events({
	'click .select-locale' : function(e,t){

		var val = $(e.currentTarget).prop('id');
		var locale = Locales.findOne({ code : val });
		t.selectedLocale.set(locale);
	},
	'click .select-globalorder' : function(e,t){

		var val = $(e.currentTarget).prop('id');
		if(val.length !== 24) return;
		var globalorder = GlobalOrderLocales.findOne({ globalorder_id : new Mongo.ObjectID(val) });
		t.selectedGlobalOrder.set(globalorder);
	},
	'click .validate' : function(e,t){

		var selectedLocale = t.selectedLocale.get().code;
		var selectedGlobalOrder = t.selectedGlobalOrder.get().globalorder_id;

		if(!selectedLocale || !selectedGlobalOrder) return;

		Session.set({
			currentLocale : selectedLocale,
			currentGlobalOrder : selectedGlobalOrder
		});
		FlowRouter.go('/');
	},
	'touchend .selected-locale' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .selected-locale' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			e.preventDefault();
			t.pressTimer = undefined;
			t.selectedLocale.set();
			t.selectedGlobalOrder.set();
		}, 800);
		return false;
	},
	'mouseup .selected-locale' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .selected-locale' : function(e,t){

		t.pressTimer = Meteor.setTimeout(function(){
			e.preventDefault();
			t.pressTimer = undefined;
			t.selectedLocale.set();
			t.selectedGlobalOrder.set();
		}, 800);
		return false;
	},
	'touchend .selected-globalorder' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .selected-globalorder' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			e.preventDefault();
			t.pressTimer = undefined;
			t.selectedGlobalOrder.set();
		}, 800);
		return false;
	},
	'mouseup .selected-globalorder' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .selected-globalorder' : function(e,t){

		t.pressTimer = Meteor.setTimeout(function(){
			e.preventDefault();
			t.pressTimer = undefined;
			t.selectedGlobalOrder.set();
		}, 800);
		return false;
	}
});
