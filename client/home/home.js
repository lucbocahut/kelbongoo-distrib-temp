Template.home.onCreated(function(){
	this.globalorder_id = this.currentGlobalOrder();
	this.subscribe('globalorders.detail.counts', this.currentLocale(), this.currentGlobalOrder());
});

Template.home.helpers({

	globalorder : function(){
		var t = Template.instance();
		return GlobalOrders.findOne(t.globalorder_id);
	},
	globalorderlocale : function(){
		var t = Template.instance();
		return GlobalOrderLocales.findOne({ globalorder_id : t.globalorder_id });
	},
	isResponsable : function(){
		var t = Template.instance();
		return Roles.userIsInRole(Meteor.userId(), "responsable", t.currentLocale());
	},
	ordersDeparted : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		return count && count.departed;
	},
	ordersTotal : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		console.log('count', t.globalorder_id, count);
		return count && count.total;
	}
});
