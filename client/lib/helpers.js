Template.registerHelper('currentLocale', function(){
    var locale = Session.get('currentLocale');
    return locale && Locales.findOne({ code : locale });
});

Template.registerHelper('currentGlobalOrder', function(){
    var globalorder_id = Session.get('currentGlobalOrder');
    return globalorder_id && GlobalOrders.findOne(globalorder_id);
});

Template.registerHelper('currentGlobalOrderLocale', function(){
    var globalorder_id = Session.get('currentGlobalOrder');
    return globalorder_id && GlobalOrderLocales.findOne({ globalorder_id : globalorder_id });
});

Template.registerHelper('getTableAlias', function(index){
    if(isNaN(index)) return;

    var locale = Locales.findOne({ code : Session.get('currentLocale') });
    return locale && locale.table_aliases[index];
});

Template.registerHelper('formatMoney', function(n){
	return Utils.formatMoney(n);
});

Template.registerHelper('addCdnPrefix', function(path){
    if(path && _.isString(path))
        return 'http://cdn.kbgstaging.org/images/' + path;
});

Template.registerHelper('$gt', function(a,b){
  	if(!isNaN(a) && !isNaN(b))
    	return a > b;
});

Template.registerHelper('$gte', function(a,b){
    if(!isNaN(a) && !isNaN(b))
      return a >= b;
});

Template.registerHelper('$eq', function(a,b){
  	return a === b;
});

Template.registerHelper('$or', function(a,b){
    return a || b;
});

Template.registerHelper('$between', function(a,b,c){
    return a > b && a < c;
});

Template.registerHelper('$join', function(a,b){
    if(!_.isArray(a) || !_.isString(b)) return;
    return a.join(b);
});

Template.registerHelper('$neq', function(a,b){
    return a !== b;
});

Template.registerHelper('$exists', function(a){
  	return typeof a !== 'undefined' && a !== null;
});

Template.registerHelper('$lt', function(a,b){
  	if(!isNaN(a) && !isNaN(b))
    	return a < b;
});

Template.registerHelper('formatDate', function(date, string){
  	if(date instanceof Date) return moment(date).format(string);
    return moment.tz(date, "Europe/Paris").format(string);
});

Template.registerHelper('uppercase', function(string){
  	if(_.isString(string)) return string.toUpperCase();
});

Template.registerHelper('console', function(data){
  	return console.log('test', data);
});

Template.registerHelper('formatGlobalOrderStatus', function(status){
	return Utils.formatGlobalOrderStatus(status);
});

Template.registerHelper('formatOrderStatus', function(order){

    if(!order) return;
    if(order.secPickedUpAt||order.fraisPickedUpAt) return "Parti";
    if(order.secDeliveredAt||order.fraisDeliveredAt||order.tabledAt) return "Table";
    if(order.arrivedAt) return "Au locale";
    return "Pas arrivé";
});

Template.registerHelper('formatTime', function(minutes){
    if(isNaN(minutes)) return;
    var h = Math.round(minutes/60);
    var m = Math.round(minutes % 60);
    return h + 'h '+m+'m';
});

Template.registerHelper('getKeyValue', function(object, key){
    if(!_.isEmpty(object) && _.isString(key)){
        return object[key];
    }
});
