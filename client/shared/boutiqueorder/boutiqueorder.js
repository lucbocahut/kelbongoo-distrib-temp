Template.boutiqueorder.helpers({
	orderItems : function(){
		var params = this.params;
		console.log('this', this);
		return this.order.items.map(function(item){
			return _.extend({ params : params }, item);
		});
	},
	showPayment : function(){
		var amountTotal = getAmountTotal(this.order.items);
		return this.params && this.order && this.params.showPayment && amountTotal;
	},
	amountTotal : function(){
		console.log('this', this);
		return getAmountTotal(this.order.items);
	},
	unitsTotal : function(){
		return getUnitsTotal(this.order.items);
	}
});

Template.boutiqueorder.events({
	'click .accept-payment' : function(e,t){

		if(!this.params.showPayment) return;

		var boutiqueorder = _BoutiqueOrders.findOne(t.data.order._id);
		var email = t.$('#email').val().trim();
		if(email) boutiqueorder.email = email;

		Utils.createModal('order_payment_modal', { boutiqueorder : boutiqueorder, params : t.data.params });
	},
	'click .add-product' : function(e,t){

		if(!this.params.allowUpdate) return;
		
		var options = {
			boutiqueorder : this.order
		};
		Utils.createModal('order_add_product_modal', options);
	}
});

function getAmountTotal(items){
	return items.reduce(function(output, item){
		return output += item.kelbongoo_price * ((100 + item.tva)/100) * item.quantity;
	}, 0);
}

function getUnitsTotal(items){
	return items.reduce(function(output, item){
		return output += item.quantity;
	}, 0);
}