Template.boutiqueorder_list_item.helpers({
	totalValue : function(){
		return this.kelbongoo_price * ((100 + this.tva)/100) * this.quantity;
	}
});

Template.boutiqueorder_list_item.events({
	'change #quantity' : function(e,t){
		var val = Number($(e.currentTarget).val());
		var items = this.items.get();
		var index = items.map(function(item){ return item.producerproduct_id._str; }).indexOf(this.producerproduct_id._str);

		if(!val){
			items.splice(index, 1);
		} else {
			items[index].quantity = val;
		}
		this.items.set(items);
	}
});