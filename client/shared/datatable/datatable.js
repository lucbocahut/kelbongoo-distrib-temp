Template.datatable.onRendered(function(){
  $('#search').focus();
})

Template.datatable.helpers({
  filterActive: function(){
    return this.params &&
          this.filters.reduce(function(acc, option){
            if(option.active && option.name !== 'default'){
              acc = true;
            }
            return acc;
          }, false);
  },
  columns: function(){
    return this.params && this.params.columns;
  },
  rowTemplateName: function(){
    return this.params.rowTemplateName || 'datatable_row_default';
  }
})

Template.datatable.events({
  'click .datatable-filter': function(e,t){
    var filter = $(e.currentTarget).data('filter');
    t.data.params.toggleFilter(filter);
  },
  'keyup #search': function(e,t){
    var search = $(e.currentTarget).val().trim();
    (_.throttle(function(){
      t.data.params.updateSearch(search);
    }, 500))();
  },
  'click .filter-menu': function(e,t){
    e.stopPropagation();
  },
  'click .clear-search': function(e,t){
    t.$('#search').val('');
    t.data.params.updateSearch('');
  }
})
