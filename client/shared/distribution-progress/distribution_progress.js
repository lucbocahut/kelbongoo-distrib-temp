Template.distribution_progress.onCreated(function(){
	this.globalorder_id = Session.get('currentGlobalOrder');
	this.subscribe('globalorders.detail.counts', this.currentLocale(), this.globalorder_id);
});

Template.distribution_progress.helpers({
	ordersArrivedPct : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		var ratio = count && (count.arrived-count.departed)/count.total;
		return count && Math.round(ratio*100);
	},
	ordersDonePct : function(){
		var t = Template.instance();
		var count =  Counts.findOne(t.globalorder_id);
		var ratio = count && count.departed/count.total;
		return count && Math.round(ratio*100);
	}
});
