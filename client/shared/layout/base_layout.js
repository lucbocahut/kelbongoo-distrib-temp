Template.base_layout.events({
	'click .logout' : function(){
		Meteor.logout(function() {
  			FlowRouter.go('/login');
		});
	},
	'touchend .current-globalorder' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		FlowRouter.go('/globalorders');
		return false;
	},
	'touchstart .current-globalorder' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			delete Session.keys['currentLocale'];
			delete Session.keys['currentGlobalOrder'];
			FlowRouter.go('/globalorders');
		}, 800);
		return false;
	},
	'mouseup .current-globalorder' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		FlowRouter.go('/globalorders');
		return false;
	},
	'mousedown .current-globalorder' : function(e,t){

		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			t.pressTimer = undefined;
			delete Session.keys['currentLocale'];
			delete Session.keys['currentGlobalOrder'];
			FlowRouter.go('/globalorders');
		}, 800);
		return false;
	},
	'touchend .company-logo' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
			FlowRouter.go('/');
		}
		return false;
	},
	'touchstart .company-logo' : function(e,t){

		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			Utils.createModal('comments_modal', {
				domain: 'entrepots',
				onValidate: function(data, callback){
					Meteor.call('comments.new', data, callback);
				}
			});
		}, 800);
		return false;
	},
	'mouseup .company-logo' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
			FlowRouter.go('/');
		}
		return false;
	},
	'mousedown .company-logo' : function(e,t){

		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			Utils.createModal('comments_modal', {
				domain: 'entrepots',
				onValidate: function(data, callback){
					Meteor.call('comments.new', data, callback);
				}
			});
		}, 800);
		return false;
	}
});
