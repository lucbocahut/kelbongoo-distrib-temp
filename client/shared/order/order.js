Template.order.helpers({
	amountTotal : function(){
		return this.order.getGlobalAmountTotal();
	},
	unitsTotal : function(){
		var order = this.order;
		if(order){

			var boutiqueorder = order.getBoutiqueOrder();

			return order.getUnitsTotal()
							+ (boutiqueorder ? boutiqueorder.getUnitsTotal() : 0);
		}
	},
	hasPaid : function(){
		return this.order.hasPaid();
	},
	consumerCredits: function(){
		var consumer = this.order.getConsumer();
		return consumer && consumer.getCredits();
	}
});

Template.order.events({
	'click .print-order' : function(e,t){
		return t.data.order.printAtLocale();
	},
	'touchend .amount-total-display' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .amount-total-display' : function(e,t){

		var self = this;

		t.pressTimer = Meteor.setTimeout(function(){

			t.pressTimer = undefined;
			var order = t.data.order;
			var eventTypes = ConsumerCreditEventTypes.find().fetch();
			var globalAmountTotal = order.getGlobalAmountTotal();

			Utils.createModal('order_consumercreditevent_modal', {
				eventTypes: eventTypes,
				consumer_id: order.consumer_id,
				addEvent: function(type, amount, callback){
					Meteor.call('consumers.addCreditEvent', order.consumer_id, type, amount, {
						order_id: order._id,
						globalorder_id: order.globalorder_id
					});
				}
			});

		}, 800);
		return false;
	},
	'mouseup .amount-total-display' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .amount-total-display' : function(e,t){

		var self = this;

		t.pressTimer = Meteor.setTimeout(function(){

			t.pressTimer = undefined;
			var order = t.data.order;
			var eventTypes = ConsumerCreditEventTypes.find().fetch();
			var globalAmountTotal = order.getGlobalAmountTotal();

			Utils.createModal('order_consumercreditevent_modal', {
				event_types: eventTypes,
				amount_total: globalAmountTotal,
				addEvent: function(type, amount, callback){
					Meteor.call('consumers.addCreditEvent', order.consumer_id, type, amount, {
						order_id: order._id,
						globalorder_id: order.globalorder_id
					});
				}
			});

		}, 800);
		return false;
	}
});

Meteor.methods({
	'consumers.addCreditEvent': function(consumer_id, type, amount, meta){
		var consumer = Consumers.findOne(consumer_id);
		consumer.addCreditEvent(type, amount, meta);
	}
})
