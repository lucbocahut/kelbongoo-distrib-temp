Template.order_add_product_modal.onCreated(function() {

    this.quantity = new ReactiveVar(1);
    this.selectedProduct = new ReactiveVar();
    this.selectedProductDetails = new ReactiveVar();

    this.search = new SearchSource('bpps', ['product_name', 'producer_name']);
});

Template.order_add_product_modal.helpers({
    totalValue: function() {
        var t = Template.instance();
        var selectedProductDetails = t.selectedProductDetails.get();
        var quantity = t.quantity.get();
        if (selectedProductDetails) {
            return selectedProductDetails.kelbongoo_price * ((100 + selectedProductDetails.tva) / 100) * (quantity || 0);
        } else {
            return 0;
        }
    },
    quantity: function() {
        var t = Template.instance();
        return t.quantity.get();
    },
    unitPrice: function() {
        var t = Template.instance();
        var selectedProductDetails = t.selectedProductDetails.get();
        if (selectedProductDetails) {
            return selectedProductDetails.kelbongoo_price * ((100 + selectedProductDetails.tva) / 100);
        } else {
            return 0;
        }
    },
    selectedProduct: function() {
        var t = Template.instance();
        return t.selectedProduct.get();
    },
    selectedProductDetails: function() {
        var t = Template.instance();
        return t.selectedProductDetails.get();
    },
    suggestions: function() {
        var t = Template.instance();
        console.log('results', t.search.getData());
        return t.search.getData({
            sort: {
                product_name: 1
            }
        });
    }
});

Template.order_add_product_modal.events({
    'click .add-product-quantity-less': function(e, t) {

        var val = t.quantity.get();
        if (isNaN(val) || val <= 0) return;
        t.quantity.set(val - 1);
    },
    'click .add-product-quantity-more': function(e, t) {

        var val = t.quantity.get();
        var details = t.selectedProductDetails.get();
        console.log('details', details);
        if (isNaN(val) || val < 0 || val === details.quantity_available) return;
        t.quantity.set(val + 1);
    },
    'keyup #producerproduct_id': function(e, t) {

        var val = $(e.currentTarget).val();
        var options = {};
        var getPPIds = function(item) {
            return item.producerproduct_id;
        };

        var itemIds = t.data.boutiqueorder ? t.data.boutiqueorder.items.map(getPPIds) : [];
        var reducedIds = t.data.boutiqueorder && t.data.boutiqueorder.reduced ? t.data.boutiqueorder.reduced.map(getPPIds) : [];
        options.badIds = [].concat(itemIds, reducedIds);
        options.boutique = t.currentLocale();
        options.availableOnly = true;

        t.search.search(val && val.trim(), options);
    },
    'click .producerproduct-list-item': function(e, t) {

        var _id = $(e.currentTarget).data('producerproduct');
        var name = $(e.currentTarget).text().trim();
        var order;

        if (!_id || _id.length !== 24) return;
        producerproduct_id = new Mongo.ObjectID(_id);

        t.search.search('');
        t.selectedProduct.set(_id);
        t.$('#producerproduct_id').val(name);

        Meteor.call('boutiqueorders.getBppDetails', t.currentLocale(), producerproduct_id, function(err, data) {
            console.log('data', data);
            if (err) return Utils.handleResponse(err);
            t.selectedProductDetails.set(_.extend({
                producerproduct_id: producerproduct_id
            }, data));
        });
    },
    'click .validate': function(e, t) {

        var modal = t.$('.modal');
        var quantity = t.quantity.get();

        var bpp = t.selectedProductDetails.get();
        var boutiqueorder = t.data.boutiqueorder || BoutiqueOrders.findOne({
            order_id: t.data.order._id
        });

        var params = {
            quantity: quantity
        };

        if (t.data.order) params.order_id = t.data.order._id;
        if (boutiqueorder) params.boutiqueorder_id = boutiqueorder._id;

        if (t.data.boutiqueorder && t.data.boutiqueorder.temp) {

            t.data.boutiqueorder.addBpp(bpp, quantity, function(err) {
                modal.modal('hide');
            });

        } else {

            Meteor.call('boutiqueorders.upsertWithBpp', t.currentLocale(), bpp.producerproduct_id, params, function(err) {
                if (err) Utils.handleResponse(err);
                modal.modal('hide');
            });
        }
    }
});
