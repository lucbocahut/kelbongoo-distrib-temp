Template.order_consumercreditevent_modal.onCreated(function(){
	this.selectedEventType = new ReactiveVar();
});

Template.order_consumercreditevent_modal.helpers({
	selectedEventType : function(){
		var t = Template.instance();
		return t.eventType.get();
	},
	eventTypes: function(){
		return ConsumerCreditEventTypes.find();
	}
});


Template.order_consumercreditevent_modal.events({

	'click .validate' : function(e,t){
		var amount = Number(t.$('#event_amount'));
		var type = t.selectedEventType.get();
		var $modal = t.$('.modal');

		return t.data.addEvent(t.data.consumer_id, type, amount, function(err){
			if(err){
				return Utils.handleResponse(err);
			}
			$modal.modal('hide');
		});
	},
	'change #event_type' : function(e,t){

		var eventTypeId = new Mongo.ObjectID($(e.currentTarget).val());

		t.selectedEventType.set(eventTypeId);

		Meteor.defer(function(){
			t.$('#event_amount').focus();
		});
	}
});
