Template.order_controls.helpers({
	showPayment : function(){
		return this.params.allowPayment;
	},
	showPaymentModal(){
		var t = Template.instance();
		var order = t.data.order;
		return () => {
			var boutiqueorder = order.getBoutiqueOrder();
			var consumercreditevents = order.getConsumerCreditEvents();

			Utils.createModal('order_payment_modal', {
				order : t.data.order,
				boutiqueorder : boutiqueorder,
				consumercreditevents: consumercreditevents,
				params : t.data.params
			});
		}
	},
	showAddProductModal(){
		return () => {
			Utils.createModal('order_add_product_modal', options);
		}
	}
});

Template.order_controls.events({
	'click .verify-order' : function(e,t){
		if(t.data.order.status !== 0) return;
		Meteor.call('orders.verify', t.currentLocale(), t.data.order._id, Utils.handleResponse);
	},
	'click .send-verification-email' : function(e,t){
		if(t.data.order.status !== 0) return;
		Utils.createModal('order_send_verification_email_modal', { order : t.data.order });
	},
	'click .add-product' : function(e,t){
		if(!t.data.params.allowUpdate) return;
		var options = {
			order : t.data.order
		};
		var boutiqueorder = BoutiqueOrders.findOne({ order_id : t.data.order._id });

		if(boutiqueorder) options.boutiqueorder = boutiqueorder;
		Utils.createModal('order_add_product_modal', options);
	},
	'click .accept-payment' : function(e,t){

		if(!this.params.allowPayment) return;

		var boutiqueorder = BoutiqueOrders.findOne({ order_id : t.data.order._id });
		var consumercreditevents = ConsumerCreditEvents.find({ 'meta.order_id' : t.data.order._id }).fetch();

		Utils.createModal('order_payment_modal', {
			order : t.data.order,
			boutiqueorder : boutiqueorder,
			consumercreditevents: consumercreditevents,
			params : t.data.params
		});
	}
});
