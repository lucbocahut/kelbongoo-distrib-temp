Template.order_list.helpers({

	'orderItems' : function(){

		if(!this.order || !this.order.items||!this.order.items.length) return;

		return this.order.items.map(function(item){
			return _.extend(item, { order : this.order, key : 'items', params : this.params });
		}, this);
	},
	removedItems : function(){

		if(!this.order || !this.order.removed||!this.order.removed.length) return;

		return this.order.removed.map(function(item){
			return _.extend(item, { order : this.order, key : 'removed', params : this.params });
		}, this);
	},
	reducedItems : function(){

		if(!this.order || !this.order.reduced||!this.order.reduced.length) return;

		return this.order.reduced.map(function(item){
			return _.extend(item, { order : this.order, key : 'reduced', params : this.params });
		}, this);
	},
	addedItems : function(){

		if(this.order){

			var boutiqueorder = BoutiqueOrders.findOne({ order_id : this.order._id });
			if(!boutiqueorder || !boutiqueorder.items || !boutiqueorder.items.length) return;

			return boutiqueorder.items.map(function(item){
				return _.extend(item, { order : this.order, boutiqueorder : boutiqueorder, key : 'added', params : this.params });
			}, this);
		}
	},
	addedReducedItems : function(){

		if(this.order){

			var boutiqueorder = BoutiqueOrders.findOne({ order_id : this.order._id });
			if(!boutiqueorder || !boutiqueorder.reduced || !boutiqueorder.reduced.length) return;

			return boutiqueorder.reduced.map(function(item){
				return _.extend(item, { order : this.order, boutiqueorder : boutiqueorder, key : 'added-reduced', params : this.params });
			}, this);
		}
	}
});