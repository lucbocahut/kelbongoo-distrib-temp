Template.order_list_item.helpers({
	totalValue : function(){
		return this.kelbongoo_price * ((100+this.tva)/100) * this.quantity;
	},
	unitPrice : function(){
		return this.kelbongoo_price * ((100+this.tva)/100); 
	},
	reducedPrice : function(){
		if(this.key === 'added-reduced' || this.key === 'reduced'){
			return this.kelbongoo_price * ((100+this.tva)/100) * this.quantity * ((100 - this.percentage)/100);
		}
	},
	reducedPercentage : function(){
		if(this.key === 'added-reduced' || this.key === 'reduced'){
			return this.percentage;
		}
	}
});

Template.order_list_item.events({
	'touchend .quantity-cell' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .quantity-cell' : function(e,t){
		var self = this;
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			if(self.params.allowUpdate){
				Utils.createModal('order_list_item_edit_modal', t.data);
			}
		}, 800);
		return false;
	},
	'mouseup .quantity-cell' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .quantity-cell' : function(e,t){
		var self = this;
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			if(self.params.allowUpdate){
				Utils.createModal('order_list_item_edit_modal', t.data);
			}
		}, 800);
		return false;
	}
});