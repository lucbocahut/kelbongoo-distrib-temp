Template.order_list_item_edit_modal.onCreated(function(){
	this.action = new ReactiveVar('remove');
	this.units = new ReactiveVar(1);
	this.percentage = new ReactiveVar(50);
	console.log('this', this);
	this.quantity = new ReactiveVar(this.data.quantity);
});

Template.order_list_item_edit_modal.helpers({
	action : function(){
		var t = Template.instance();
		return t.action.get();
	},
	showValidation : function(){
		var t = Template.instance();

		if(this.order.status === 1) return true;
		if(this.key === "reduced" || this.key === "removed" || this.key === "added-reduced") return true;

		var action = t.action.get();
		if(!action) return;
		if(action === 'reduce') return t.percentage.get() && t.units.get();
		if(action === 'remove') return t.units.get();
	},
	units : function(){
		if(this.quanity === 1) return;
		return _.range(2,this.quantity+1);
	},
	quantity : function(){
		var t = Template.instance();
		return t.quantity.get();
	},
	selectAction : function(){
		return this.key === 'items' || this.key === 'added';
	},
	selectQuantity : function(){
		return this.key === 'items' || this.key === 'added';
	},
	selectReduction : function(){
		var t = Template.instance();
		var action = t.action.get();
		return (this.key === 'items' || this.key === 'added') && this.order.status > 2 && action === 'reduce';
	},
	removeTitle : function(){
		if(this.key === 'items'){
			if(this.order.status === 1) return 'Mettre à jour';
			if(this.order.status > 2) return 'Manquant';
		}
		if(this.key === 'added') return 'Enlever';
	},
	validateTitle : function(){
		if(this.key === 'reduced' || this.key === 'added-reduced') return "Remettre au prix initial";
		if(this.key === 'removed') return "Remettre dans le panier";
		return "Valider";
	}
});

Template.order_list_item_edit_modal.events({
	'change .action' : function(e,t){
		var val = $(e.currentTarget).prop('id');
		if(val) t.action.set(val);
	},
	'change #percentage' : function(e,t){
		var val = Number($(e.currentTarget).val());
		if(val) t.percentage.set(val);
	},
	'change #units' : function(e,t){
		var val = Number($(e.currentTarget).val());
		t.units.set(val);
	},
	'click .less' : function(e,t){
		var val = Number(t.$('#quantity'));
		var quantity = t.quantity.get();
		if(quantity <= 0) return;
		quantity--;
		t.quantity.set(quantity);
	},
	'click .more' : function(e,t){
		var val = Number(t.$('#quantity'));
		var quantity = t.quantity.get();
		if(quantity < 0 || quantity > 7) return;
		quantity++;
		t.quantity.set(quantity);
	},
	'click .validate' : function(e,t){

		var methodName;
		var args = [t.currentLocale(), t.data.producerproduct_id];
		var $modal = t.$('.modal');
		var action = t.action.get();
		var units = t.units.get();
		var percentage = t.percentage.get();
		var itemsIndex = t.data.order.items.map(function(item){ return item.producerproduct_id._str; }).indexOf(t.data.producerproduct_id._str);

		var methodName;
		var $modal = t.$('.modal');
		var action = t.action.get();
		var units = t.units.get();
		var percentage = t.percentage.get();
		var key = t.data.key;

		// push the order/boutiqueorder _id onto args

		if(key === 'added' || key === 'added-reduced') {
			args.push(t.data.boutiqueorder._id);
		} else {
			args.push(t.data.order._id);
		}

		if(key === 'items'){

			if(action === 'remove') {
				methodName = 'orders.updateItemQuantity';
				args.push(t.data.quantity - units);
			} else {
				methodName = 'orders.reduceItem';
				args.push(units);
				args.push(percentage);
			}
		}
		if(key === 'reduced'){
			methodName = 'orders.unreduceItem';
		}
		if(key === 'removed'){
			methodName = 'orders.updateItemQuantity';
			args.push(t.data.quantity); // the entire item quantity must be put back in the cart
		}
		if(key === 'added'){

			if(action === 'remove') {
				methodName = 'boutiqueorders.updateItemQuantity';
				args.push(t.data.quantity - units);
			} else {
				methodName = 'boutiqueorders.reduceItem';
				args.push(units);
				args.push(percentage);
			}
		}
		if(key === 'added-reduced'){
			methodName = 'boutiqueorders.unreduceItem';
		}

		console.log('methodName', methodName, 'args', args, 't.data', t.data, 'units', units, 'key', key, 'action', action);

		Meteor.apply(methodName, args, function(err){
			Utils.handleResponse(err);
			$modal.modal('hide');
		});
	}
});
