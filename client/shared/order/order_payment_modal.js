Template.order_payment_modal.onCreated(function(){
	this.change = new ReactiveVar(0);
	this.allowValidate = new ReactiveVar(false);
	this.paymentType = new ReactiveVar();
});

Template.order_payment_modal.helpers({
	change : function(){
		var t = Template.instance();
		return t.change.get();
	},
	amountTotal : function(){

		return getTotal(this.order, this.boutiqueorder, this.consumercreditevents);
	},
	allowValidate : function(){
		var t = Template.instance();
		return t.allowValidate.get();
	},
	paymentType : function(){
		var t = Template.instance();
		return t.paymentType.get();
	},
	order : function(){
		return this.order;
	},
	boutiqueorder : function(){
		return this.boutiqueorder;
	}
});


Template.order_payment_modal.events({

	'submit #payment' : function(e,t){

		e.preventDefault();

		if(t.allowValidate.get()){
			validatePayment(e,t);
		}
	},
	'change input[type=radio][name=type]' : function(e,t){

		var val = Number($(e.currentTarget).val());
		if(isNaN(val) || (val !== 1 && val !== 2)) return;

		t.paymentType.set(val);

		if(val === 2){

			t.allowValidate.set(true);

		} else {

			var consumer_paid = Number(t.$('#consumer_paid').val());
			var total = getTotal(t.data.order, t.data.boutiqueorder);

			if(validateChange(consumer_paid, total)){
				t.allowValidate.set(true);
			} else {
				t.allowValidate.set(false);
			}

			Meteor.defer(function(){
				t.$('#consumer_paid').focus();
			});
		}
	},
	'keyup #consumer_paid' : function(e,t){

		var consumer_paid = Number($(e.currentTarget).val());
		var total = getTotal(t.data.order, t.data.boutiqueorder);

		if(validateChange(consumer_paid, total)){
			t.allowValidate.set(true);
		} else {
			t.allowValidate.set(false);
		}

		t.change.set(Math.max(consumer_paid - total, 0));
	}
});

function validatePayment(e,t){

	var modal = t.$('.modal');
	var total = 0;
	var paymentType = t.paymentType.get();
	var methodName = t.data.order ? 'orders.pay' : 'boutiqueorders.noOrderCreateAndPay';
	var args = [Session.get('currentLocale')];

	if(t.data.order) total += t.data.order.amount_total;
	if(t.data.boutiqueorder) total += t.data.boutiqueorder.amount_total;

	t.allowValidate.set(false);

	if(t.data.order){
		args.push(t.data.order._id);
	} else {
		args.push(t.data.boutiqueorder.getInsertData());
	}

	args = args.concat([paymentType, total]);

	Meteor.apply(methodName, args, function(err){
		modal.modal('hide');
		if(err) return Utils.handleResponse(err);
		if(t.data.params.paymentCallback && typeof t.data.params.paymentCallback === 'function'){
			t.data.params.paymentCallback();
		}
	});
}

function getTotal(order, boutiqueorder, consumercreditevents){
	console.log('args', arguments);

	var total = 0;
	if(order) total += order.amount_total;
	if(boutiqueorder) total += boutiqueorder.amount_total;
	if(consumercreditevents) total += consumercreditevents.reduce(function(acc, cr){ return acc += cr.amount }, 0);
	return total;
}

function validateChange(paid, total){
	if(isNaN(paid)||paid<=0) return false;
	return paid >= Math.floor(total*100)/100;
}
