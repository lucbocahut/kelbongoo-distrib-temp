Template.order_special_items_modal.helpers({
  nondispatch(){
    console.log('this.getNondispatch', this.getNondispatch())
    return this.getNondispatch();
  },
  removed(){
    return this.getRemovedItems();
  },
  reduced(){
    return this.getReducedItems();
  }
})
