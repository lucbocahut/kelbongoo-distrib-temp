Template.display_table_amount_total.helpers({
	getAmountTotal : function(){
		var boutiqueorder = BoutiqueOrders.findOne({ order_id : this._id });
		var total = this.amount_total;
		if(boutiqueorder) total += boutiqueorder.amount_total;
		return total;
	}
});