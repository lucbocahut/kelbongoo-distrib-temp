Template.display_table_distribution_status.helpers({
	hasPaid : function(){
		return typeof this.payment_method !== 'undefined';
	},
	hasSec : function(){
		return typeof this.sec_guichet !== 'undefined';
	},
	hasFrais : function(){
		return typeof this.frais_guichet !== 'undefined';
	}
});

Template.display_table_distribution_status.events({
	'click .arrived' : function(e,t){
		e.preventDefault();
		e.stopPropagation();
		Meteor.call('orders.setArrived', t.currentLocale(), this._id);
	}
});
