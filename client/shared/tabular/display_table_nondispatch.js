Template.display_table_nondispatch.helpers({
	nondispatchList : function(){
		var len = this.nondispatch && this.nondispatch.length;
		if(!len) return;
		return this.nondispatch.filter(function(n){ return n.location === 2; }).map(function(n, index){
			if(index === len-1) return _.extend({ last : true }, n);
			return n;
		});
	}
});