Template.display_table_order_status.helpers({
	showPaymentButton : function(){
		return this.fraisDeliveredAt && this.secDeliveredAt;
	}
});

Template.display_table_order_status.events({
	'click .launch-pay-modal' : function(e,t){
		e.stopPropagation();
		Utils.createModal('order_payment_modal', { order : t.data, params : {} });
	}
});