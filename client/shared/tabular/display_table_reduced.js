Template.display_table_reduced.helpers({
	reducedList : function(){
		var len = this.reduced && this.reduced.length;
		if(!len) return;
		return this.reduced.map(function(n, index){
			if(index === len-1) return _.extend({ last : true }, n);
			return n;
		});
	}
});