Template.display_table_removed.helpers({
	removedList : function(){
		var len = this.removed && this.removed.length;
		if(!len) return;
		return this.removed.map(function(n, index){
			if(index === len-1) return _.extend({ last : true }, n);
			return n;
		});
	}
});