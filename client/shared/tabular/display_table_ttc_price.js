Template.display_table_ttc_price.helpers({
	'getTtcPrice' : function(){
		if(this.kelbongoo_price && this.tva){
			return formatMoney(this.kelbongoo_price * ((100 + this.tva)/100));
		}
	}
});