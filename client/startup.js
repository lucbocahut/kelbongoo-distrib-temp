import '/imports/startup/client';

Meteor.startup(function(){

	// shortcut for getting the currentlocale on every template instance
 	Blaze.TemplateInstance.prototype.currentLocale = function(){
 		return Session.get('currentLocale');
 	}
 	Blaze.TemplateInstance.prototype.currentGlobalOrder = function(){
 		return Session.get('currentGlobalOrder');
 	}

	Tracker.autorun(function(){
		var currentGlobalOrder = Session.get('currentGlobalOrder');
		var currentLocale = Session.get('currentLocale');

		if(currentLocale && currentGlobalOrder){
			Meteor.subscribe('globalorders.detail', currentLocale, currentGlobalOrder);
			Meteor.subscribe('globalorders.detail.counts', currentLocale, currentGlobalOrder);
		}
	});

	Counts = new Mongo.Collection('counts');

	console.log('doing this first');
});
