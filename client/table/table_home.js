Template.table_home.onCreated(function(){

	var self = this;
	self.globalorder_id = Session.get('currentGlobalOrder');
	self.currentTable = new ReactiveVar(0);
	self.tail = new ReactiveVar([]);

	self.subscribe('table.base', self.currentLocale());

	self.autorun(function(){
		var table = self.currentTable.get();
		self.subscribe('table.home', self.currentLocale(), self.globalorder_id, table);
	});
});

Template.table_home.helpers({
	tables: function(){
		return LocaleTables.find();
	},
	currentTable : function(){
		var t = Template.instance();
		return LocaleTables.findOne({ number: t.currentTable.get() });
	},
	places : function(){
		var t = Template.instance();
		var table = LocaleTables.findOne({ number: t.currentTable.get() });
		return table && table.getLocalePlaces();
	}
});

Template.table_home.events({
	'change #current_table' : function(e,t){
		var val = Number($(e.currentTarget).val());
		t.currentTable.set(val);
	}
});
