Template.table_place.helpers({
	order: function(){
		return this.getCurrentOrder();
	},
	hasExtraInfo : function(){
		var order = this.getCurrentOrder();
		var nondispatch = order && order.hasNondispatch();
		var missing = order && order.hasRemovedItems();
		return nondispatch || missing;
	}
});

Template.table_place.events({
	'touchend .place-container' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'touchstart .place-container' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			if(t.data.hasReceivedAllBacs()){
				Meteor.call('distribution.markTablePlaceFree', t.currentLocale(), t.data.globalorder_id, t.data._id, function(err){
					if(err) {
						return Utils.handleResponse(err);
					}
				});
			}
		}, 800);
		return false;
	},
	'mouseup .place-container' : function(e,t){
		if(t.pressTimer){
			Meteor.clearTimeout(t.pressTimer);
		}
		return false;
	},
	'mousedown .place-container' : function(e,t){
		t.pressTimer = Meteor.setTimeout(function(){
			t.pressTimer = undefined;
			if(t.data.hasReceivedAllBacs()){
				Meteor.call('distribution.markTablePlaceFree', t.currentLocale(), t.data.globalorder_id, t.data._id, function(err){
					if(err) {
						return Utils.handleResponse(err);
					}
				});
			}
		}, 800);
		return false;
	},
	'click .nondispatch-list': function(e,t){
		var place = LocalePlaces.findOne({ table: this.table, place: this.place });
		var order = place && place.getCurrentOrder();
    Utils.createModal('order_nondispatch_detail_modal', order);
  },
  'click .missing-list': function(e,t){
		var place = LocalePlaces.findOne({ table: this.table, place: this.place });
		var order = place && place.getCurrentOrder();
    Utils.createModal('order_missing_detail_modal', order);
  },
});

Meteor.methods({
	'tables.markPlaceFree' : function(locale, table, place){

		var locale = Locales.findOne({ code: locale });
		var localetable = LocaleTables.findOne({ locale: locale, number: table });
		var localeplace = localetable.getPlace({ number: place });
		var order = localeplace.getCurrentOrder();

		return order.update({ $set: { tableDepartedAt : new Date() }})
						.then(
							function(){ return locale.tableNextOrder(table, place) }
						);
	},
	'orders.markCouloirDelivered': function(order_id, couloir){
		var order = Orders.findOne(order_id);
		var bacs = order.getBacs({ couloir: couloir });
		var queue = [];
		for(var bac of bacs){
			queue.push(function(){ bac.markDelivered() })
		}
		return Promise.all(queue);
	}
})
