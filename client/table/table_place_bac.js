Template.table_place_bac.events({
  'touchend .bac-number' : function(e,t){
    if(t.pressTimer){
      Meteor.clearTimeout(t.pressTimer);
    }
    return false;
  },
  'touchstart .bac-number' : function(e,t){
    t.pressTimer = Meteor.setTimeout(function(){
      t.pressTimer = undefined;
      t.data.markDelivered();
    }, 800);
    return false;
  },
  'mouseup .bac-number' : function(e,t){
    if(t.pressTimer){
      Meteor.clearTimeout(t.pressTimer);
    }
    return false;
  },
  'mousedown .bac-number' : function(e,t){
    t.pressTimer = Meteor.setTimeout(function(){
      t.pressTimer = undefined;
      t.data.markDelivered();
    }, 800);
    return false;
  },
})
