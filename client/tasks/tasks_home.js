Template.tasks_home.onCreated(function(){
	this.subscribe('tasks.home', this.currentLocale(), this.currentGlobalOrder());
});

Template.tasks_home.helpers({
	globalorder : function(){
		var t = Template.instance();
		return GlobalOrders.findOne(t.currentGlobalOrder());
	},
	tasks : function(){
		var t = Template.instance();
		return TaskInstances.find({
			'meta.globalorder_id' : t.currentGlobalOrder(),
			'meta.locale' : t.currentLocale()
		});
	}
});
