Template.tasks_home_list_item.events({
	'change .done' : function(e,t){
		var val = $(e.currentTarget).is(":checked");
		var currentLocale = Session.get('currentLocale');
		var currentGlobalOrder = Session.get('currentGlobalOrder');

		Meteor.call('tasks.updateStatus', currentLocale, currentGlobalOrder, t.data._id, val, function(err){
			if(err) Utils.handleResponse(err);
		});
	}
});
