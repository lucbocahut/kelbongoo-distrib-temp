// see readme for technical details

import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';


let SyncedCollection = Mongo.Collection;

if (Meteor.isServer) {
	const REMOTE_MONGO_URL = process.env.REMOTE_MONGO_URL || false;

	//'mongodb://localhost:3001/meteor';

	const SyncQueue = new Mongo.Collection('sync_queue');
	const remoteCollections = {};
	let pull_in_progress = false;


	// process local queue items
	const syncRemoteCollections = () => {
		if (pull_in_progress) { return false };
		//console.log('syncing remote collections: ', Object.keys(remoteCollections).join(' '));
		SyncQueue.find({}, { sort: { _id: 1 }})
		.fetch()
		.forEach( ({ _id, collectionName, action, doc, selector, modifier, options }) => {
			if (action === 'insert') {
				console.log(' <-> REMOTE insert', _id, collectionName, doc);
				remoteCollections[collectionName].insert(doc)
			}
			if (action === 'update') {
				console.log(' <-> REMOTE update', _id, collectionName, selector, modifier, options );
				remoteCollections[collectionName].update(selector, modifier, options);
			}
			if (action === 'upsert') {
				console.log(' <-> REMOTE upsert', _id, collectionName, selector, modifier, options );
				remoteCollections[collectionName].upsert(selector, modifier, options);
			}
			if (action === 'remove') {
				console.log(' <-> REMOTE remove', _id, collectionName, selector );
				remoteCollections[collectionName].remove(selector);
			}
			SyncQueue.remove({ _id });
		});
	}


	class ServerSyncedCollection extends Mongo.Collection {

		constructor(collectionName, options = {}) {
			super(collectionName, options);
			this.collectionName = collectionName;
			this.db = new MongoInternals.RemoteCollectionDriver(REMOTE_MONGO_URL);
			this.remoteCollection = new Mongo.Collection(collectionName, { _driver: this.db, _suppressSameNameError: true });
			remoteCollections[collectionName] = this.remoteCollection;
			// this.pull();
		}

		// pulls collection from the remote DB
		pull(selector = {}) {
			console.log(' <-> pulling', this.collectionName, selector);

			// first sync remote
			syncRemoteCollections();
			pull_in_progress = true;
			console.time(` <-> pull ${this.collectionName}`);
			// first empty the local collection
			super.remove({});

			// retrieve remote items and insert n at a time
			let count = 0;
			let total = 0;
			let lastId;
			do {
				if (lastId) {
					selector['_id'] = { $gt: lastId };
				}
				// console.log(' .. doing', this.collectionName, selector);
				const records = this.remoteCollection.find(selector, { limit: 5000, sort: { _id: 1 } }).fetch();
				// console.log(records.map( record => record._id ).join(' '));

				records.forEach( record => {
					// console.log(lastId, record);
					//const hmmm = super.findOne({ _id: record._id });
					// if (hmmm) {
					// 	console.log('found', record);
					// 	throw Meteor.Error('dupe');
					// }
					super.insert(record, { bypassCollection2: true });
					lastId = record._id;
				});
				count = records.length;
				total += count;
				console.log(` <-> . ${total} done`);
			} while (count === 5000);


			// returns when done
			console.timeEnd(` <-> pull ${this.collectionName}`);
			pull_in_progress = false;
			return true;
		}

		// we redefine crud operations with a hook to insert into the syncQueue
	  insert(doc, callback) {
	  	console.log(' <-> insert', this.collectionName, doc, callback);
	    const new_callback = (error, insert_id) => {
	      if (error) {
	        throw new Meteor.Error(error);
	      }
	      if (insert_id) {
	      	SyncQueue.insert({
	      		action: 'insert',
	      		collectionName: this.collectionName,
	      		doc,
	      	});
	      }
	      return callback;
	    }
	    return super.insert(doc, new_callback);
	  }

	  update(selector, modifier, options, callback) {
	  	console.log(' <-> update', this.collectionName, selector, modifier, options, callback);
	    const count = super.update(selector, modifier, options, callback);
	    if (count > 0) {
	    	SyncQueue.insert({
	    		action: 'update',
	    		collectionName: this.collectionName,
	    		selector,
	    		modifier,
	    		options,
	    	});
	    }
	  	if (callback) callback(count);
	    return count;
	  }

	 upsert(selector, modifier, options, callback) {
	 	console.log(' <-> upsert', this.collectionName, selector, modifier, options, callback);
	    const count = super.upsert(selector, modifier, options, callback);
	    if (count > 0) {
	    	SyncQueue.insert({
	    		action: 'upsert',
	    		collectionName: this.collectionName,
	    		selector,
	    		modifier,
	    		options,
	    	});
	    }
	  	if (callback) callback(count);
	    return count;
	  }

	  remove(selector, callback) {
	  	console.log(' <-> remove', this.collectionName, selector, callback);
	  	const count = super.remove(selector);
	  	if (count > 0) {
	  		SyncQueue.insert({
	    		action: 'remove',
	  			collectionName: this.collectionName,
	    		selector,
	    	});
	  	}
	  	if (callback) callback(count);
	  	return count;
	  }

	};
	SyncedCollection = ServerSyncedCollection;

	// schedule sync every second
	let syncInterval = Meteor.setInterval(syncRemoteCollections, 5000);

}

export default SyncedCollection;
