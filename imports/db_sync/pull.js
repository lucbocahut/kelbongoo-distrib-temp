import moment from 'moment';

// collections
// import '/imports/collections';

// BoutiqueOrders
// BoutiqueProducerProducts
// BoutiqueProducerProductsEvents
// BoutiqueRestocks
// ConsumerCreditEvents
// ConsumerCreditEventTypes
// Consumers
// DispatchDone
// Employees
// GlobalOrderLocales
// GlobalOrders
// GlobalOrderLocaleProducerProducts
// GlobalOrderProducerProducts
// LocalePlaces
// Locales
// LocaleTables
// OrderBacs
// OrderItems
// OrderReducedItems
// OrderRemovedItems
// Orders
// ProducerProducts
// Producers
// TaskInstances

// pull syncedCollections with your selectors


// let's focus on relevant global orders

const PullFromRemote = () => {

	const REMOTE_MONGO_URL = process.env.REMOTE_MONGO_URL || false;

	console.time(' <-> TOTAL PULL TIME');

	// facilitate some pull selectors
	db = new MongoInternals.RemoteCollectionDriver(REMOTE_MONGO_URL);
	const RemoteGlobalOrders = new Mongo.Collection('globalorders', { _driver: db, _suppressSameNameError: true });

	const GlobalOrdersIds = RemoteGlobalOrders.find({ status : 4 }, {
	  sort : { distribution_date : -1 },
	  limit: 5
	})
	.map(go => go._id);



	// pull'em
	BoutiqueOrders.pull({ globalorder_id: { $in: GlobalOrdersIds } });
	BoutiqueProducerProducts.pull({});
	BoutiqueProducerProductsEvents.pull({});
	BoutiqueRestocks.pull({});
	ConsumerCreditEvents.pull({ "meta.globalorder_id": { $in: GlobalOrdersIds } });
	ConsumerCreditEventTypes.pull({});
	Consumers.pull({});
	DispatchDone.pull({ date: { $gt: moment().subtract(1, 'months').toDate() } });
	Employees.pull({});
	GlobalOrderLocales.pull({});
	GlobalOrders.pull({ _id: { $in: GlobalOrdersIds } });
	GlobalOrderLocaleProducerProducts.pull({ globalorder_id: { $in: GlobalOrdersIds } });
	GlobalOrderProducerProducts.pull({ globalorder_id: { $in: GlobalOrdersIds } });
	LocalePlaces.pull({});
	Locales.pull({});
	LocaleTables.pull({});
	OrderBacs.pull({ deliveredAt: { $gt: moment().subtract(1, 'months').toDate() } });
	OrderItems.pull({});
	OrderReducedItems.pull({});
	OrderRemovedItems.pull({});
	Orders.pull({ globalorder_id: { $in: GlobalOrdersIds } });
	ProducerProducts.pull({});
	Producers.pull({});
	TaskInstances.pull({});

	console.timeEnd(' <-> TOTAL PULL TIME');
}

export default PullFromRemote;