This modules extends the Mongo.Collection class to sync a local collection with a remote master.

__INTRO__
Some preliminary items:
- we assume the local and remote collections share the same name
- collections are pulled from master on startup
- local synced collections are wipped on pull
- subsequent insert, update, upsert and remove operations are *eventually* sent back to the remote master


__USAGE__
```
const SyncedCollection = import('/imports/db_sync');
const MyCollection = new SyncedCollection('collection_name', options);
```
Use MyCollection as you would use a Mongo.Collection instance.


__UNDER THE HOOD__
To sync items we make use of a syncQueue local collection to save our data. After all it's all documents in the end.
All CRUD operations on synced collections are first saved to this local collection. 
Periodical updates runs the queued queries on the remote master before deleting them locally.

These documents can be found in the syncQueue collection (local)
 insert { collectionName, action: 'insert', doc, option }
 update { collectionName, action: 'update', selector, modifier, options }
 upsert { collectionName, action: 'update', selector, modifier, options }
 remove { collectionName, action: 'remove', selector, options }

When a new synced collection is created:
- we immeditely run all queued queries on the remote
- we delete all documents on the local collection
- we insert all existing documents on the remote into the local collection

