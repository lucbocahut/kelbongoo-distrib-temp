// read a file off the SD wifi card and save it to the database periodically
// use the syncedCollection module as well.

import { Mongo } from 'meteor/mongo';
import SyncedCollection from '/imports/db_sync';


const fileBackup = new SyncedCollection('file_backup');

const PATH_TO_FILE = '192.168.1.199/backup';
const site = 'BOR';

const readFile = (path) => {
	const file = fs.read(path);
	fileBackup.insert({ file, site });
}

const readInterval = Meteor.setInterval(readFile, 1 * 60 * 60 * 1000);