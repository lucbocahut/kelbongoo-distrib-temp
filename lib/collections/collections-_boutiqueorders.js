
if(Meteor.isClient){
  var _BoutiqueOrder = function(doc){
    return _.extend(this, doc);
  }

  _.extend(_BoutiqueOrder.prototype, {
    updateEmail : function(email, callback){

      if(email){
        _BoutiqueOrders.update(this._id, { $set : { email : email }}, callback);
      } else {
        _BoutiqueOrders.update(this._id, { $unset : { email : '' }}, callback);
      }

    },
    addBpp : function(bpp, quantity, callback){

      var items = this.items.slice(0);
      var amount_total = this.amount_total;
      var units_total = this.units_total;

      items.push({
        producer_name : bpp.producer_name,
        product_name : bpp.product_name,
        producerproduct_id : bpp.producerproduct_id,
        producer_id : bpp.producer_id,
        unit_display : bpp.unit_display,
        tva : bpp.tva,
        kelbongoo_price : bpp.kelbongoo_price,
        quantity : quantity
      });
      amount_total += quantity * ((100 + bpp.tva)/100) * bpp.kelbongoo_price;
      units_total += quantity;

      _BoutiqueOrders.update(this._id, { $set : { items : items, amount_total : amount_total, units_total : units_total }}, callback);
    },
    updateBppQuantity : function(itemIndex, quantity, callback){

      var items = this.items.slice(0);
      var item = items[itemIndex];
      var amount_total = this.amount_total;
      var units_total = this.units_total;

      amount_total += quantity * ((100 + item.tva)) * item.kelbongoo_price;
      units_total += quantity;

      if(-quantity === item.quantity){
        items.splice(itemIndex, 1);
      } else {
        items[itemIndex].quantity += quantity;
      }

      _BoutiqueOrders.update(this._id, { $set : { items : items, amount_total : amount_total, units_total : units_total }}, callback);
    },
    getInsertData : function(){
      var items = this.items.map(function(item){
        return {
          producerproduct_id : item.producerproduct_id,
          quantity : item.quantity
        };
      });
      return {
        items : items,
        email : this.email,
        globalorder_id : this.globalorder_id
      };
    }
  });

  _BoutiqueOrders = new Mongo.Collection(null, {
    idGeneration : "MONGO",
    transform : function(doc){
      return new _BoutiqueOrder(doc);
    }
  });
}
