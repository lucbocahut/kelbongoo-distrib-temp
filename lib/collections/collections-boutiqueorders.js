import SyncedCollection from '/imports/db_sync';
BoutiqueOrders = new SyncedCollection('boutiqueorders', { idGeneration: 'MONGO' });
//BoutiqueOrders = new Mongo.Collection('boutiqueorders', { idGeneration: 'MONGO' });

var BoutiqueOrdersSchema = new SimpleSchema({

	/*
	* 	BASICS
	*/

	boutique : {
		type : String
	},
	createdAt : {
		type : Date
	},
	order_id : {
		type : Mongo.ObjectID,
		optional : true
	},
	email : {
		type : String,
		optional : true
	},
	globalorder_id : {
		type : Mongo.ObjectID,
		optional : true
	},
	amount_total : {
		type : Number,
		decimal : true,
		optional : true,
		defaultValue : 0
	},
	units_total : {
		type : Number,
		optional : true,
		defaultValue : 0
	},
	paidAt : {
		type : Date,
		optional : true
	},

	/*
	*	ITEMS
	*/

	items : {
		type : [Object],
		optional : true,
		defaultValue : []
	},
	'items.$.producer_name' : {
    	type : String
  	},
  	'items.$.product_name' : {
    	type : String
  	},
  	'items.$.quantity' : {
    	type : Number
  	},
  	'items.$.producer_id' : {
    	type : Mongo.ObjectID,
    	optional : true
  	},
  	'items.$.producerproduct_id' : {
    	type : Mongo.ObjectID
  	},
  	'items.$.unit_display' : {
    	type : String
  	},
  	'items.$.kelbongoo_price' : {
    	type : Number,
    	decimal : true
  	},
  	'items.$.tva' : {
    	type : Number,
    	decimal : true
  	},

  	/*
	*	REDUCED
	*/

	reduced : {
		type : [Object],
		optional : true,
		defaultValue : []
	},
	'reduced.$.producer_name' : {
    	type : String
  	},
  	'reduced.$.product_name' : {
    	type : String
  	},
  	'reduced.$.quantity' : {
    	type : Number
  	},
  	'reduced.$.producer_id' : {
    	type : Mongo.ObjectID,
    	optional : true
  	},
  	'reduced.$.producerproduct_id' : {
    	type : Mongo.ObjectID
  	},
  	'reduced.$.unit_display' : {
    	type : String
  	},
  	'reduced.$.kelbongoo_price' : {
    	type : Number,
    	decimal : true
  	},
  	'reduced.$.tva' : {
    	type : Number,
    	decimal : true
  	},
  	'reduced.$.percentage' : {
  		type : Number
  	},
  	'reduced.$.userId' : {
  		type : String
  	},

  	/*
	*	PAYMENT
	*/

	payment_data : {
		type : Object,
		optional : true
	},
	'payment_data.userId' : {
		type : String,
		optional : true
	},
	'payment_data.method' : {
		type : Number,
		optional : true
	}
});

BoutiqueOrders.attachSchema(BoutiqueOrdersSchema);

BoutiqueOrders.helpers({

});
