import SyncedCollection from '/imports/db_sync';
BoutiqueProducerProductsEvents = new SyncedCollection('boutiqueproducerproductsevents', { idGeneration : 'MONGO' });

var BoutiqueProducerProductsEventsSchema = new SimpleSchema({
	boutique : {
		type : String
	},
	producerproduct_id : {
		type : Mongo.ObjectID
	},
	createdAt : {
		type : Date,
		autoValue : function(){
			if(this.isInsert) return new Date();
		},
		optional : true
	},

	// EVENT TYPES
	//
	// 0 - stock arrival
	// 1 - sortie stock
	// 2 - inventory
	// 3 - remove stock

	type : {
		type : Number
	},
	createdBy : {
		type : String,
		optional : true,
		autoValue : function(){
			if(this.isInsert) return this.userId;
		}
	},
	quantity : {
		type : Number
	},
	comment : {
		type : String,
		optional : true
	}
});

BoutiqueProducerProductsEvents.attachSchema(BoutiqueProducerProductsEventsSchema);
