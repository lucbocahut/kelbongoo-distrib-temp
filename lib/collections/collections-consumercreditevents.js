import SyncedCollection from '/imports/db_sync';
ConsumerCreditEvents = new SyncedCollection('consumercreditevents', { idGeneration : 'MONGO' });

var ConsumerCreditEventsSchema = new SimpleSchema({
  consumer_id: {
    type: Mongo.ObjectID
  },
  type: {
    type: Mongo.ObjectID
  },
  amount: {
    type: Number,
    decimal: true
  },
  createdAt: {
    type: Date
  },
  createdBy: {
    type: String,
    optional: true
  },
  meta: {
    type: Object,
    blackbox: true,
    optional: true
  }
});

ConsumerCreditEvents.attachSchema(ConsumerCreditEventsSchema);
