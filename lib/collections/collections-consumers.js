import SyncedCollection from '/imports/db_sync';
Consumers = new SyncedCollection('consumers');

Consumers.helpers({
  getCredits: function(){
    return this.consumer_credits || 0;
  },
  addCreditEvent: function(type, amount, meta){
    ConsumerCreditEvents.insert({
      consumer_id: this._id,
      type: type,
      amount: amount,
      createdAt: moment().toDate(),
      createdBy: Meteor.userId(),
      meta: meta
    })
  }
})
