import SyncedCollection from '/imports/db_sync';
GlobalOrderLocales = new SyncedCollection('globalorderlocales');

var GlobalOrderLocalesSchema = new SimpleSchema({

	/*
	*	LOCALES STATUS
	*	0 - globalorder
	*	1 - ready for preparation
	*	2 - preparation
	*	3 - ready for distribution
	*	4 - distribution
	*	5 - done
	*	6 - cancelled
	*	7 - error
	*/

	status : {
		type : Number,
		optional : true,
		defaultValue : 0
	},

	/*
	* 	CAISSE FOR THE LOCALE
	*/

	'caisse' : {
		type : Object,
		optional : true
	},
	'caisse.real' : {
		type : Object,
		optional : true
	},
	'caisse.real.cash' :{
		type : Object,
		optional : true
	},
	'caisse.real.cash.value' :{
		type : Number,
		decimal : true,
		optional : true
	},
	'caisse.real.checks' :{
		type : Object,
		optional : true
	},
	'caisse.real.checks.count' :{
		type : Number,
		optional : true
	},
	'caisse.expected' : {
		type : Object,
		optional : true
	},
	'caisse.expected.cash' :{
		type : Object,
		optional : true
	},
	'caisse.expected.cash.count' :{
		type : Number,
		optional : true
	},
	'caisse.expected.cash.value' :{
		type : Number,
		decimal : true,
		optional : true
	},
	'caisse.expected.checks' :{
		type : Object,
		optional : true
	},
	'caisse.expected.checks.count' :{
		type : Number,
		optional : true
	},
	'caisse.expected.checks.value' :{
		type : Number,
		decimal : true,
		optional : true
	}
});

GlobalOrderLocales.attachSchema(GlobalOrderLocalesSchema);

GlobalOrderLocales.helpers({
	tableNextOrder: function(place){
		var nextOrder = this.getCurrentlyWaitingOrders(
									{},
									{ sort: { arrivedAt: 1 }, limit: 1 }
								)[0];
		if(!place){
			place = this.getFreePlace();

		}
		return place.setCurrentOrder(nextOrder);
	},
	getCurrentlyWaitingOrders: function(filter, options){

		var selector = Object.assign({
			globalorder_id: this.globalorder_id,
			locale: this.locale,
			arrivedAt: { $exists: true },
			tabledAt: { $exists: false }
		}, filter);

		return 	this.getAllOrders(selector, options);
	},
	getCurrentlyTabledOrders: function(filter, options){

		var selector = Object.assign({
			globalorder_id: this.globalorder_id,
			locale: this.locale,
			tabledAt: { $exists: true },
			tableDepartedAt: { $exists: false }
		}, filter);

		return 	this.getAllOrders(selector, options);
	},
});
