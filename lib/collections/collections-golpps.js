import SyncedCollection from '/imports/db_sync';
GlobalOrderLocaleProducerProducts = new SyncedCollection('globalorderlocaleproducerproducts', { idGeneration : 'MONGO' });

var GlobalOrderLocaleProducerProductsSchema = new SimpleSchema({
	globalorder_id : {
		type : Mongo.ObjectID
	},
	locale : {
		type : String
	},
	producer_id : {
		type : Mongo.ObjectID,
		optional : true
	},
	producer_name : {
		type : String
	},
	producerproduct_id : {
		type : Mongo.ObjectID
	},
	product_name : {
		type : String
	},
	unit_display : {
		type : String
	},
	// true if the producerproduct's stock_type is equal to 2, i.e. it's stored in the entrepot
	entrepot : {
		type : Boolean,
		optional : true
	},
	// automatically added to make units easier (programmed on producerproduct)
	quantity_default : {
		type : Number,
		defaultValue : 0,
		optional : true
	},
	// added by the locale to be sold as rab
	quantity_added : {
		type : Number,
		defaultValue : 0,
		optional : true
	},
	quantity_sold : {
		type : Number,
		optional : true,
		defaultValue : 0
	},
	quantity_ordered : {
		type : Number,
		defaultValue : 0,
		optional : true
	},
	quantity_expected : {
		type : Number,
		optional : true
	},
	quantity_received : {
		type : Number,
		optional : true
	},

	//
	// UNITS
	// units_done is a count of the units that have been done so far
	// units_completed is true when units_done >= quantity_expected
	//

	units_done : {
		type : Number,
		optional : true
	},
	units_completed : {
		type : Boolean,
		optional : true
	},

	//
	// DISPATCH
	// dispatch_done is a count of the quantity of this product already dispatched
	// dispatch_completed is true when dispatch_done >= quantity_sold
	// dispatch_missing for items not delivered/not found or lacking during the preparation
	// dispatch_errors for items marked as dispatched but not found in the bac during distribution
	//

	dispatch_done : {
		type : Number,
		optional : true
	},
	dispatch_completed : {
		label: 'problematic',
		type : Boolean,
		optional : true,
		custom: function() {
			if ( (this.value !== undefined) && (this.value !== true) && (this.value !== false) ) {
				console.log(this);
				return 'ooops';
			}
		}
	},

	dispatch_missing : {
		type : [Object],
		optional : true,
		defaultValue : []
	},
	'dispatch_missing.$.number' : {
		type : Number,
		optional : true
	},
	'dispatch_missing.$.quantity' : {
		type : Number,
		optional : true
	},
	'dispatch_missing.$.date' : {
		type : Date,
		optional : true
	},
	'dispatch_missing.$.user' : {
		type : String,
		optional : true
	},

	dispatch_errors : {
		type : [Object],
		optional : true,
		defaultValue : []
	},
	'dispatch_errors.$.order_id' : {
		type : Mongo.ObjectID,
		optional : true
	},
	'dispatch_errors.$.quantity' : {
		type : Number,
		optional : true
	},

	// 'direct' products are those who have neither dispatch nor units to do
	// eg jus de pomme, gros pains, cidres etc.
	// this flag ensures that we have checked the quantity_received of these types of products

	direct_validated : {
		type : Boolean,
		optional : true
	}
});

GlobalOrderLocaleProducerProducts.attachSchema(GlobalOrderLocaleProducerProductsSchema);
