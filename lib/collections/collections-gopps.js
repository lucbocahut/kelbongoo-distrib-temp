import SyncedCollection from '/imports/db_sync';
GlobalOrderProducerProducts = new SyncedCollection('globalorderproducerproducts', { idGeneration: 'MONGO' });

var helpers = Object.assign({
  undoOrder: function(quantity){
    return this.update({
      $set: {
        quantity_available: this.quantity_available + quantity,
        quantity_sold: this.quantity_sold - quantity
      }
    });
  },
  unreserve: function(quantity){
    return this.update({
      $set: { quantity_reserved: this.quantity_reserved - quantity }
    })
  },
  reduceQuantitySold: function(quantity){
    return this.update({
      $set: { quantity_sold: this.quantity_sold - quantity }
    });
  }
}, Utils.collectionHelpersBase(GlobalOrderProducerProducts));

GlobalOrderProducerProducts.helpers(helpers);
