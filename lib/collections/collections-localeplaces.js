import SyncedCollection from '/imports/db_sync';
LocalePlaces = new SyncedCollection('localeplaces');

var LocalePlacesSchema = new SimpleSchema({
	locale : {
		type : String
	},
	table : {
		type : Number
	},
	place : {
		type : Number
	},
	active : {
		type : Boolean,
		optional : true,
		defaultValue : true
	},
	order_id : {
		type : Mongo.ObjectID,
		optional : true
	},
	next : {
		type : Mongo.ObjectID,
		optional : true
	}
});

LocalePlaces.attachSchema(LocalePlacesSchema);

var helpers = Object.assign({
	getTable(){
		return LocaleTables.findOne({ locale: this.locale, number: this.table });
	},
	getTableName: function(){
		return this.getTable()
									.getName();
	},
	getCurrentOrder: function(){
		return this.order_id && Orders.findOne(this.order_id);
	},
	hasDeliverableBacs: function(couloir){
		var order = this.getCurrentOrder();
		if(!order){
			return false;
		}
		return order.hasDeliverableBacs(couloir);
	},
	formatForDelivery: function(couloir){
		var order = this.getCurrentOrder().formatForDelivery(couloir);
		var place = this;
		if(!order){
			return;
		}
		return Object.assign(
						place,
						{ order }
					);
	},
	getFormattedPlaceNumber(){
		return this.place + 1;
	}
}, Utils.collectionHelpersBase(LocalePlaces));

LocalePlaces.helpers(helpers);
