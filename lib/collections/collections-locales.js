import SyncedCollection from '/imports/db_sync';
Locales = new SyncedCollection('locales');

var PRINTER_ID = "";
var PRINTER_KEY = "-----BEGIN PRIVATE KEY-----\n"+
"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDq4D7KjO3BZ6QA\n"+
"+ielCtidgtT6CTLmOLsJ2/ek5l6MhJaidT2OgUMzLfy1XVmhb/8BwtKXmlzWowUB\n"+
"ZfQJMrVMNtJM9qeXlKCbwbTIAph9r6raF5TF82twUGT2k8lHJa/JKj7Rl6Hkis5F\n"+
"jidl6aB9cdLckuqAk/38VAmkOKLrnY1QTV9O+Ydu/CeMMib1k0AATlUyJbDF4iHi\n"+
"zzsneg73q9PPsnSKChoSe4UApXkxaM1w3RWj+UAdjLtwN2I878qAAudH5ijimBbL\n"+
"qywGLqBzFMXhsw+2osJfMmrubJDc1AwJZ5t3oQqgCOEcYEk0V8DNcwDwvVFGrwM8\n"+
"XRTAoXCtAgMBAAECggEBAKvxd+Y+HzurzKMufmFTPFqq0ZEttMZXF43TK0rbnrHN\n"+
"ypypqWOue2x3KP34lcoaPBuk7Rgw1/AFLX/ljC5DofdmVrQjCXNtSdpchyS4ybuR\n"+
"I0k/ZBk55xg0soTUkc/05egldhFnqQuvFexTccIosuXCsvPo1aVFMfVL7Db9gz9q\n"+
"E197HUUtq670ELzyrthyRKT6CHb2dGjRuFD5osKkueMWfSTpPFzEJ7KbTvXKwy4x\n"+
"xzYHdE8tXykTWqsqlbe8PoyFVq+uWvmb7t2Q29CLsJpUTLoSQBQUjXN80chCTM3A\n"+
"NKvYVkzvlYe1mBOrpzJjTUbx1nABIy2OC1LLD5KWdoECgYEA+LGjTQDNj4L5OF77\n"+
"2gvTTKGFXchuEg8AEPAWQn93Vj777LbkH4cIi2gt/+2OG9qO+aW0jtglESP0n5R4\n"+
"drq/C5/DBIZvCg1YjogF/JIzoYyJZ2ahHwur1O4ECbxYaBlYy3rsVnmQVIbGvKq5\n"+
"cctoJ1tXfwsqTsLFCmdQ1w684U0CgYEA8cavrkxtXwYkhSAvWVAhATMXVRLoeTDh\n"+
"1gaI8yB3O7U2IZIe6cl6IqAbG9Acf3UJRvhFC4oDKKvAX/PFnftQuNpeuIRHR9sC\n"+
"7LnIiJpafchYAsVUbKwSMOoLAFPT64zMmHcP7/KT5aPPNrkqLgQ6oQ8JDcnk2GwO\n"+
"VjSxDHZnHOECgYBJFgtjyeSfVGJhiqiBQBBs6NsE4K5YaEggsi4YhNVRZ1kqC+Wm\n"+
"zkxYi9lY6TtmPCjgfS4+/I6IJwTxEPUGD4OtqtnpLojbb2ijGp6Hx3+c7mxDEwou\n"+
"0YNCJGDTa6KflTNfOVTTZQ5hiOpG7/Vn19tYFaP1Fhbs5ph5ojtDuHUR0QKBgFuk\n"+
"bOjLLVWsVWFkfq4C0k/LFtf3T8jH/4IJfhVxw+AM4tS8txz0cmf3ee8D5hllujt9\n"+
"VWZ3TJU47LUyCEDGhqVamDamZQJWgHhMPzeQEibFLp72PvbmDgseW0cnfxc0ikRs\n"+
"NM1FjEup2JwseVFx5usvcMYaNbTW2GRSmp1Xr+PhAoGBAKRmCglMTsUhZFgp2ey2\n"+
"xokThSesr5IVDjkZpDcF2lOMHTL4oGVoQsKq1h0xOVPnjeEBmHfCLAuiJZ0Wi6eo\n"+
"odjAK6+COB/HT4RAlhANO4ZO5FhwgRxQ4az+LHyVac+U+kpW3FX2xAYAwIUBWjO5\n"+
"4t3EzWXd8TAWKyPkahS6RVxa\n"+
"-----END PRIVATE KEY-----\n"

Locales.helpers({
  printDocumentWithDefaultPrinter: function(stream, params){

    var request = googleOauthJwt.requestWithJWT();
    var jwt = {
      email : params.email_id,
      key : PRINTER_KEY,
      scopes : ["https://www.googleapis.com/auth/cloudprint"]
    };
    var formData = {
        printerid: PRINTER_ID,
        title: params.print.title,
        content: printStream,
        contentType: params.print.contentType || "application/pdf",
        'ticket[version]': '1.0',
        'ticket[print]': ''
    };

    return new Promise(function(resolve, reject){

      request(
        {
          uri: 'https://www.google.com/cloudprint/search',
          json: true,
          formData: formData,
          jwt: cloudPrintConfig.jwt
        },
        function (err, res, body) {
          if (err) {
            return reject(err);
          } else {
            if (body.success == false) {
              return reject(new Error('unsuccessful submission', body));
            } else {
              resolve(body);
            }
          }
        }
      );
    });
  },
  getTableAlias: function(table){
    return this.table_aliases[table];
  },
  getBacStrandedTimeout(couloir){
    return this.bac_stranded_timeout[couloir];
  }
})
