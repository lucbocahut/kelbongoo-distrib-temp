import SyncedCollection from '/imports/db_sync';
LocaleTables = new SyncedCollection('localetables');

var LocaleTablesSchema = new SimpleSchema({
  locale: {
    type: String,
    min: 3,
    max: 3
  },
  alias: {
    type: String,
    optional: true
  },
  number: {
    type: Number
  }
});

LocaleTables.attachSchema(LocaleTablesSchema);

LocaleTables.helpers({
  getLocalePlaces: function(filter, options){
    var selector = Object.assign(
      { table: this.number },
      filter
    );
    options = Object.assign({ sort: { place: 1 }}, options || {})
    return LocalePlaces.find(selector, options).fetch();
  },
  getCurrentOrders: function(filter, options){
    var places = this.getPlaces(filter, options);
    var orderIds = places.map(function(p){ return p.order_id });
    return Orders.find({ _id: { $in: orderIds }}).fetch();
  },
  getName: function(){
    return this.name;
  },
  getPlacesFree: function(){
    return LocalePlaces.find({ table: this.number, order_id: { $exists: false }}).count();
  }
})
