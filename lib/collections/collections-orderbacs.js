import SyncedCollection from '/imports/db_sync';
class OrderBacsCollection extends SyncedCollection {

  batchMarkDeliveredOrWaiting(deliveredBacIds, waitingBacIds){
    Meteor.call('orderbacs.batchMarkDeliveredOrWaiting', deliveredBacIds, waitingBacIds);
  }

  batchMarkDelivered(ids){
    return new Promise((resolve, reject) => {

      this.update(
        { _id: { $in: ids }},
        { $set: { deliveredAt: moment().toDate() }},
        (err) => {
          if(err){
            return reject(err);
          }
          return resolve();
        }
      );
    })
  }

  batchMarkWaiting(ids){
    return new Promise((resolve, reject) => {

      this.update(
        { _id: { $in: ids }},
        { $set: { waitingAt: moment().toDate() }},
        (err) => {
          if(err){
            return reject(err);
          }
          return resolve();
        }
      );
    })
  }

  batchMarkPickedUp(orderbacIds){
    Meteor.call('orderbacs.batchMarkPickedUp', orderbacIds);
  }

  getReadyBacs(filter = {}, options = {}){
    var selector = Object.assign({
      ready: true,
      pickedUpAt: { $exists: false }
    }, filter);
    return this.find(filter, options).fetch();
  }

  getWaitableBacs(couloir){
    if(Meteor.isClient()){

      return this.find({
        couloir,
        pickedUpAt: { $exists: true },
        deliveredAt: { $exists: false }
      })
      .fetch()
      .filter(orderbac => {
        /* requires LocalePlaces collection */
        return !orderbac.isTabled();
      });
    }
  }

  getDeliverableBacs(couloir){

    if(Meteor.isClient()){

      return this.find({
        couloir,
        pickedUpAt: { $exists: true },
        deliveredAt: { $exists: false }
      })
      .fetch()
      .filter(orderbac => {
        /* requires LocalePlaces collection */
        return orderbac.isTabled();
      });
    }
  }
};

OrderBacs = new OrderBacsCollection('orderbacs');

var OrderBacsSchema = new SimpleSchema({
  order_id: {
    type: Mongo.ObjectID
  },
  couloir: {
    type: Number
  },
  number: {
    type: Number
  },
  is_last_bac: {
    type: Boolean,
    defaultValue: false,
    optional: true
  },
  ready: {
    type: Date,
    optional: true
  },
  pickedUpAt: {
    type: Date,
    optional: true
  },
  waitingAt: {
    type: Date,
    optional: true
  },
  deliveredAt: {
    type: Date,
    optional: true
  }
});

var helpers = Object.assign({
  markReady: function(){
    return this.update({ $set: { arrivedAt: moment().toDate() }});
  },
  unmarkReady: function(){
    return this.update({ $unset: { arrivedAt: '' }});
  },
  markPickedUp: function(){
    return this.update({ $set: { pickedUpAt: moment().toDate() }});
  },
  unmarkPickedUp: function(){
    return this.update({ $unset: { pickedUpAt: '' }});
  },
  markWaiting: function(){
    return this.update({ $set: { waitingAt: moment().toDate() }});
  },
  unmarkWaiting: function(){
    return this.update({ $unset: { waitingAt: '' }});
  },
  markDelivered: function(){
    return this.update({ $set: { deliveredAt: moment().toDate() }});
  },
  unmarkDelivered: function(){
    return this.update({ $unset: { deliveredAt: '' }});
  },
  getOrder: function(){
    return Orders.findOne({ _id: this.order_id });
  },
  getLocalePlace: function(){
    return LocalePlaces.findOne({ order_id: this.order_id });
  },
  getPlaceIndex(){
    var localeplace = this.getLocalePlace();
    return (localeplace.table*10)+(localeplace.place);
  },
  isLastBac(){
    return this.is_last_bac;
  },
  isTabled(){
    return !!this.getLocalePlace();
  },
  isWaiting(){
    return !!this.waitingAt;
  },
  formatForPickup: function(){
    var order = this.getOrder();
    var options = {
      order
    };
    if(this.isLastBac()){
      options.nondispatch = order.getNondispatch(this.couloir);
    }
    return Object.assign(this, options);
  },
  formatForDelivery: function(){
    var localeplace = this.getLocalePlace();
    var order = this.getOrder();
    /* so we can display table data */
    var options = {
      localeplace
    };
    if(this.isLastBac()){
      options.nondispatch = order.getNondispatch(this.couloir);
    }
    return Object.assign(this, options);
  }
}, Utils.collectionHelpersBase(OrderBacs));

OrderBacs.helpers(helpers);

Meteor.methods({
  'orderbacs.markDelivered': function(orderbac_id){
    return OrderBacs.update(orderbac_id, { $set: { deliveredAt: moment().toDate() }});
  },
  'orderbacs.undoPickedUp': function(orderbac_id){
    return OrderBacs.update(orderbac_id, { $unset: { pickedUpAt: '' }});
  },
  'orderbacs.batchMarkPickedUp': function(orderbacIds){
    return OrderBacs.update(
      {
        _id: { $in: orderbacIds },
        ready: true // can only pick up 'ready' bacs...
      },
      { $set: { pickedUpAt: moment().toDate() }},
      { multi: true }
    );
  },
  'orderbacs.batchMarkDeliveredOrWaiting': function(deliveredBacIds, waitingBacIds){
		return OrderBacs.batchMarkDelivered(deliveredBacIds)
            .then(
              () => OrderBacs.batchMarkWaiting(waitingBacIds)
            );
	}
})
