import SyncedCollection from '/imports/db_sync';
OrderItems = new SyncedCollection('orderitems');

var OrderItemsSchema = new SimpleSchema({
  order_id: {
    type: Mongo.ObjectID
  },
  'producer_name' : {
    type : String
  },
  'product_name' : {
    type : String
  },
  'quantity' : {
    type : Number
  },
  'producer_id' : {
    type : Mongo.ObjectID,
    optional : true
  },
  'producerproduct_id' : {
    type : Mongo.ObjectID
  },
  'type' : {
    type : String
  },
  'unit_display' : {
    type : String
  },
  'kelbongoo_price' : {
    type : Number,
    decimal : true
  },
  'tva' : {
    type : Number,
    decimal : true
  }
});

OrderItems.attachSchema(OrderItemsSchema);

var helpers = Object.assign({

  updateQuantity: function(newQuantity){
    return this.update({ $set: { quantity: newQuantity }});
  },
  getGopp: function(){
    return GlobalOrderProducerProducts.findOne({
      globalorder: this.globalorder_id,
      producerproduct_id: this.producerproduct_id
    });
  },
  getTTCPrice: function(){
    var gopp = this.getGopp();
    return gopp.kelbongoo_price * ((100 + gopp.tva)/100) * this.quantity;
  },
  getHTPrice: function(){
    var gopp = this.getGopp();
    return gopp.kelbongoo_price * this.quantity;
  },
  unreserveGopp: function(quantity){
    return this.getGopp().unreserve(quantity);
  },
  reduceGoppQuantitySold: function(quantity){
    return this.getGopp().reduceQuantitySold(quantity);
  }

}, Utils.collectionHelpersBase(OrderItems));

OrderItems.helpers(helpers);
