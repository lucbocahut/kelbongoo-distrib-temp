import SyncedCollection from '/imports/db_sync';
OrderRemovedItems = new SyncedCollection('orderremoveditems', { idGeneration: 'MONGO' });

var OrderRemovedItemsSchema = new SimpleSchema({
  order_id: {
    type: Mongo.ObjectID
  },
  'producer_name' : {
    type : String
  },
  'product_name' : {
    type : String
  },
  'quantity' : {
    type : Number
  },
  'producer_id' : {
    type : Mongo.ObjectID,
    optional : true
  },
  'producerproduct_id' : {
    type : Mongo.ObjectID
  },
  'type' : {
    type : String
  },
  'unit_display' : {
    type : String
  },
  'kelbongoo_price' : {
    type : Number,
    decimal : true
  },
  'tva' : {
    type : Number,
    decimal : true
  }
})

OrderRemovedItems.attachSchema(OrderRemovedItemsSchema)

OrderRemovedItems.helpers({
  update: function(modifier){
    return new Promise(function(resolve,reject){
      OrderItems.update(this._id, modifier, function(err){
        if(err){
          return reject(err);
        }
        return resolve();
      })
    });
  },
  remove: function(){
    return new Promise(function(resolve,reject){
      OrderItems.remove(this._id, function(err){
        if(err){
          return reject(err);
        }
        return resolve();
      })
    });
  },
  updateQuantity: function(newQuantity){
    return this.update({ $set: { quantity: newQuantity }});
  }
});
