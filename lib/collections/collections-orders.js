import SyncedCollection from '/imports/db_sync';
Orders = new SyncedCollection('orders');

var DONE_STATUS = 4;

var OrdersSchema = new SimpleSchema({
	firstname : {
  	type : String,
  	min : 2,
  	max : 60,
  	label : "Nom"
	},
	lastname : {
  	type : String,
  	min : 2,
  	max : 60,
  	label : "Prénom"
	},
	telephone : {
  	type : String,
  	regEx : /^0[1-9]\s[0-9]{2}\s[0-9]{2}\s[0-9]{2}\s[0-9]{2}$/,
  	label : "Téléphone"
	},
	email : {
  	type : String,
  	regEx : /^[^\s@]+@[^\s@]+\.[^\s@]+$/,
  	label : "Adresse mail"
	},
	globalorder_id : {
  	type : Mongo.ObjectID
	},
	distribution_date : {
  	type : Date
	},
	amount_total : {
  	type : Number,
  	decimal : true
	},
	amount_total_HT: {
		type: Number,
		decimal: true
	},
	token : {
  	type : String,
  	min : 17,
  	max : 17
	},
	verified : {
  	type : Boolean,
  	defaultValue : false,
  	optional : true
	},
	verifiedAt : {
  	type : Date,
  	optional : true
	},
	createdAt : {
  	type : Date
	},
	units_total : {
  	type : Number,
  	defaultValue : 0
	},
	locale : {
  	type : String
	},
	status : {
		type : Number,
		defaultValue : 0,
		optional : true
	},

	/*
	* DISTRIBUTION EVENTS
	*/

	arrivedAt : {
		type : Date,
		optional : true
	},
	tabledAt : {
		type : Date,
		optional : true
	},
	tableDepartedAt : {
		type : Date,
		optional : true
	},
	payments: {
		type: Object,
		blackbox: true,
		optional: true
	},
	holdUntil : {
		type : Date,
		optional : true
	},
	tableBackOff: {
		type: Boolean,
		optional: true,
		defaultValue: false
	},
	boutiqueorder_id: {
		type: Mongo.ObjectID,
		optional: true
	}
});

Orders.attachSchema(OrdersSchema);

function addGlobalOrderKeyToItem(item){
	return item && Object.assign(item, { globalorder_id: this.globalorder_id });
}

/*
	orders
	- cancel
		- can only be done if order has not yet been verified...
	- print locale
	- verify
	- savePayment
	- mark arrived
	- mark tabled
	- mark table left
	- mark done (what event triggers this ? i like it !)
		- payment for unpaid order and table left for paid orders…
		- it means that we can more easily separate boutiqueorders and orders…
	- hasReceivedAllBacs(frais/sec)
	- hasPaid
	- hasArrived
	- isTabled
	- (NO addItem, only _addItem, for unremoveItem/unreduceItem)
	- removeItemQuantity
		- i can do this at any time until the order is done
		- state < verification
				- DON’T put in orderremoveditems
				- unreserve gopps
				- adjust totals
				- reduce orderitem quantity
		- verification <= state < compilation
				- put in orderremoveditems
				- remove from quantity_sold in gopps/golpps
				- adjust totals
				- reduce orderitem quantity
		- compilation <= state < done
				- put in orderremoveditems
				- adjust totals
				- reduce orderitem quantity
	- reduceItemQuantity
		- state < compilation
				- NOT possible
		- compilation <= state < done
				- put into orderreduceditems
				- adjust totals
	- unremoveItemQuantity
		- can be done at any time, as long as the orderremoveditem exists
	- unreduceItemQuantity
		- can be done after compilation, as long as the orderreduceditem exists
*/

var helpers = Object.assign(
	{
		cancel: function(){
			if(this.status > 0){
				return Promise.reject(
					new InvalidParamsError('verified orders cannot be cancelled')
				);
			}
			return this.update({ $set: { status: 8 }});
		},
		getBacs: function(filter = {}, options = {}){
			var selector = Object.assign({
				order_id: this._id
			}, filter);
			return OrderBacs.find(selector, options).fetch();
		},
		isWaiting(){
			return this.status === 3 && !!this.tabledAt;
		},
		getFraisBacs(filter, options){
			var selector = Object.assign({
				couloir: 0
			}, filter);
			return this.getBacs(selector);
		},
		getSecBacs(filter, options){
			var selector = Object.assign({
				couloir: 1
			}, filter);
			return this.getBacs(selector);
		},
		getBoutiqueOrder: function(){
			return BoutiqueOrders.findOne(this.boutiqueorder_id);
		},
		getBacNumbers: function(couloir){
			return this.getBacs({ couloir: couloir }).map(function(bac){
				return bac.number
			});
		},
		getMissingUnits(){
			var missing = this.getRemovedItems();
			if(!missing){
				return 0;
			}
			return missing.reduce((acc, item) => {
				return acc += item.quantity;
			}, 0);
		},
		hasNondispatch(){
			return (
				OrderItems.find({ nondispatch: true }).count() +
				OrderReducedItems.find({ nondispatch: true }).count()
			) > 0;
		},
		hasRemovedItems(){
			return OrderRemovedItems.find().count() > 0;
		},
		getAllItems(filter, options){
			return [
				...this.getItems(filter, options),
				...this.getReducedItems(filter, options)
			];
		},
		getNondispatch(couloir){
			var selector = {};
			if(_.isNumber(couloir)){
				selector.couloir = couloir;
			}
			return this.getAllItems(selector)
							.filter(item => item.nondispatch)
							.map(item => ({
								product_name: item.product_name,
								producer_name: item.producer_name,
								unit_display: item.unit_display,
								quantity: item.quantity,
								couloir: item.couloir
							}));
		},
		getNondispatchUnits(){
			var missing = this.getRemovedItems();
			if(!this.nondispatch || !this.nondispatch.length){
				return 0;
			}
			return this.nondispatch.reduce((acc, item) => {
				return acc += item.quantity;
			}, 0);
		},
		getAmountTotal(){
			return this.amount_total;
		},
		getPdfStream: function(){
			var template = _.template(Assets.getText('order-pdf-template.html'));
			return Utils.buildPdfStream(template(this));
		},
		printAtLocale: function(params){
			var locale = this.getLocale();
			var orderPdfStream = this.getPdfStream();
			return locale.printDocumentWithDefaultPrinter(orderPdfStream, params);
		},
		getConsumer(){
			return Consumers.findOne({ emails: this.email });
		},
		getGlobalAmountTotal: function(){

			var boutiqueorder = this.getBoutiqueOrder();
			var consumer = this.getConsumer();
			var boutiqueTotal = boutiqueorder ? boutiqueorder.getAmountTotal() : 0;
			var credits = consumer ? consumer.getCurrentConsumerCredits() : 0;

			return this.getAmountTotal() + boutiqueTotal - credits;
		},
		savePayment: function(params){
			var modifier = { $push: { payments: params }};
			if(this.isTabled()){
				modifier.$set.status = DONE_STATUS;
			}
			return this.update(modifier);
		},
		verify: function(){
			return Promise.resolve(); // not pertinent to distribution app...
		},
		isVerified: function(){
			return this.status >= 1;
		},
		isCompiled: function(){
			return this.status >= 2;
		},
		isArrived: function(){
			return !!this.arrivedAt;
		},
		isTabled: function(){
			return !!this.tabledAt;
		},
		setStatus: function(status){
			return this.update({ $set: { status: status }});
		},
		hasPaid: function(){
			return !!this.payments && this.payments.length > 0;
		},
		getOrderBacs: function(filter, options){
			var selector = Object.assign({ order_id: this._id }, filter);
			return OrderBacs.find(selector, options).fetch();
		},
		hasReceivedAllBacs: function(type){
			var filter = { deliveredAt: { $exists: false }};
			if(type){
				filter.type = type;
			}
			var orderBacs = this.getOrderBacs(filter);
			return orderBacs.length === 0;
		},
		markArrived: function(){
			if(Meteor.isClient){
				return Meteor.call('orders.markArrived', this.locale, this._id);
			}
			else {
				return Promise.resolve().then(
					() => this.update({ $set: { arrivedAt: moment().toDate(), status: 3 }})
				).then(
					() => this.markOrderBacsArrived()
				).then(
					() => this.tryToTable()
				)
			}
		},
		markOrderBacsArrived: function(){
			var promise = Promise.resolve();
			this.getOrderBacs().forEach(function(orderBac){
				promise.then(
					function(){ return orderBac.markArrived() }
				)
			});
			return promise;
		},
		tryToTable: function(){
			var selectedTable = LocalePlaces.aggregate([{
				$match: { locale: this.locale, order_id: { $exists: false }}
			},{
				$group: { _id: '$table', count: { $sum: 1 } }
			}, {
				$project: { table: '$_id', count: 1 }
			}, {
				$sort: { count: -1 }
			},{
				$limit: 1
			}]);
			if(!selectedTable.length){
				return Promise.resolve();
			}
			var table = selectedTable[0].table;
			var place = LocalePlaces.findOne({ locale: this.locale, table: table, order_id: { $exists: false }}, { sort: { place: 1 }, limit: 1 });
			return this.markTabled(place);
		},
		markTabled: function(localeplace){
			return this._update({
				$set: { tabledAt: moment().toDate() }
			})
			.then(
				() => localeplace._update({ $set: { order_id: this._id }})
			);
		},
		markTableLeft: function(){
			var modifier = {
					$set: { tableLeftAt: moment().toDate(), tableBackOff: false }
			};
			if(this.hasPaid()){
				modifier.$set.status = DONE_STATUS;
			}
			return this.update(modifier);
		},
		getItems: function(filter = {}, options = {}){
			var selector = Object.assign({
				order_id: this._id
			}, filter);
			return OrderItems.find(selector, options)
												.fetch()
												.map(addGlobalOrderKeyToItem.bind(this));
		},
		getItem: function(producerproduct_id){
			return this.getItems({ producerproduct_id })[0];
		},
		getRemovedItems: function(filter = {}, options = {}){
			var selector = Object.assign({
				order_id: this._id
			}, filter);
			return OrderRemovedItems.find(selector, options)
															.fetch()
															.map(addGlobalOrderKeyToItem.bind(this));
		},
		getReducedItems: function(filter = {}, options = {}){
			var selector = Object.assign({
				order_id: this._id
			}, filter);
			return OrderReducedItems.find(selector, options)
															.fetch()
															.map(addGlobalOrderKeyToItem.bind(this));
		},

		hasDeliverableBacs: function(couloir){
			return this.getDeliverableBacs(couloir).length > 0;
		},
		getDeliverableBacs: function(couloir){
			return this.getOrderBacs({
				pickedUpAt: { $exists: true },
				deliveredAt: { $exists: false }
			});
		},
		getLocalePlace(){
			return LocalePlaces.findOne({ order_id: this._id });
		},
		getLocaleName(){
			var locale = Locales.findOne({ code: this.locale });
			return locale.name;
		},
		formatWithTable: function(){
			var localeplace = LocalePlaces.findOne({ order_id: order._id });
			if(localeplace){
				order.table = localeplace.table;
				order.place = localeplace.place;
			}
			return order;
		},
		formatForDelivery: function(couloir){
			var bacs = this.getDeliverableBacs(couloir);
			var bacIds = bacs.map(function(bac){ return bac._id });
			return Object.assign(
							this,
							{
								bacs: bacs,
								toggleCurrentBacsDelivered: function(){
									Meteor.call(
										'orders.toggleCurrentBacsDelivered',
										this._id,
										bacIds
									);
								}
							}
						);
		},
		updateTotals: function(){

			var totals = this.getItems().reduce(function(acc, item){
				acc.units_total += item.quantity;
				acc.amount_total += item.getTTCPrice();
				acc.amount_total_HT += item.getHTPrice();
				return acc;
			}, { units_total: 0, amount_total: 0, amount_total_HT: 0 });

			var reducedTotals = this.getReducedItems().reduce(function(acc, reducedItem){
				acc.units_total += reducedItem.quantity;
				acc.amount_total += reducedItem.getTTCPrice();
				acc.amount_total_HT += reducedItem.getHTPrice();
				return acc;
			}, { units_total: 0, amount_total: 0, amount_total_HT: 0 });

			return this.update({
				$set: {
					units_total: totals.units_total+reducedTotals.units_total,
					amount_total: totals.amount_total+reducedTotals.amount_total,
					amount_total_HT: totals.amount_total_HT+reducedTotals.amount_total_HT
				}
			});
		},
		getUnitsTotal(){
			return this.units_total;
		},

		/*
			orders manage their own orderitems, orderreduceditems,
			orderremoveditems and orderbacs
		*/
		removeItemQuantity: function(producerproduct_id, quantity){
			var promise = Promise.resolve();
			var item = this.getItem(producerproduct_id);

			if(!this.isVerified()){

				promise.then(
					function(){ return item.unreserveGopp(quantity) }
				)
			} else {

				promise.then(
					function(){ return this._addOrderRemovedItemQuantity(item, quantity) }
				)

				if(!this.isCompiled()){

					promise.then(
						function(){ return item.reduceGoppQuantitySold(quantity) }
					)
				}
			}

			promise.then(
				// needs to handle the case where the item quantity is now 0
				function(){ return this._removeOrderItemQuantity(quantity) }
			)
			.then(
				function(){ return this.updateTotals() }
			);

			return promise;
		},
		reduceItemQuantity: function(producerproduct_id, quantity, percentage){

			// handle case where existing reduction isn't the same percentage...
			var item = this.getItem(producerproduct_id);
			var promise = Promise.resolve();

			if(!this.isCompiled()){
				return Promise.reject(
					new InvalidParamsError('cannot reduce items in a non-compiled order')
				)
			}

			promise.then(
				// needs to handle the case where the reduced item already exists
				function(){ return this._addOrderReducedItemQuantity(item, quantity, percentage) }
			)
			.then(
				// needs to handle the case where the item quantity is now 0
				function(){ return this._removeOrderItemQuantity(producerproduct_id, quantity) }
			)
			.then(
				function(){ return this.updateTotals() }
			)

			return promise;
		},
		unremoveItemQuantity: function(producerproduct_id, quantity){

			var promise = Promise.resolve();
			var removedItem = this.getRemovedItems({ producerproduct_id: producerproduct_id })[0];

			if(!this.isCompiled()){
				return Promise.reject(
					new InvalidParamsError('cannot unremove items from a non-compiled order')
				)
			}

			promise.then(
				// needs to handle the case where the quantity is now 0
				function(){ return this._removeOrderRemovedItemQuantity(removedItem, quantity) }
			)
			.then(
				// needs to handle the case where the corresponding item exists / does not exist
				function(){ return this._addOrderItemQuantity(producerproduct_id, quantity) }
			)
			.then(
				function(){ return this.updateTotals() }
			)

			return promise;
		},
		fullName: function(){
			if(this.firstname && this.lastname){
				return this.lastname.toUpperCase()+' '+this.firstname;
			}
			else if (this.lastname){
				return this.lastname.toUpperCase();
			}
			else {
				return this.email;
			}
		},
		unreduceItemQuantity: function(producerproduct_id, quantity){

			// handle case where existing reduction isn't the same percentage...
			var reducedItem = this.getReducedItems({ producerproduct_id: producerproduct_id })[0];
			var promise = Promise.resolve();

			if(!this.isCompiled()){
				return Promise.reject(
					new InvalidParamsError('cannot reduce items in a non-compiled order')
				)
			}

			promise.then(
				// needs to handle the case where the quantity is now 0
				function(){ return this._removeOrderReducedItemQuantity(reducedItem, quantity) }
			)
			.then(
				// needs to handle the case where the corresponding item exists / does not exist
				function(){ return this._addOrderItemQuantity() }
			)
			.then(
				function(){ return this.updateTotals() }
			)

			return promise;
		},

		// PRIVATE METHODS

		_addOrderReducedItemQuantity: function(item, quantity, percentage){

			var reducedItem = this.getReducedItems({ producerproduct_id: producerproduct_id })[0];

			if(!reducedItem){
				return this._insertReducedItem(item, quantity, percentage);
			} else {
				return reducedItem.update({ $set: { quantity: item.quantity + quantity }});
			}
		},
		_removeOrderReducedItemQuantity: function(producerproduct_id, quantity){

			var item = this.getReducedItems({ producerproduct_id: producerproduct_id })[0];

			if(item.quantity > quantity){
				return item.update({ $set: { quantity: item.quantity - quantity }});
			}
			else {
				return item.remove();
			}
		},
		// have to handle the not yet exists case !
		_addOrderRemovedItemQuantity: function(item, quantity){

			var removedItem = this.getRemovedItems({ producerproduct_id: producerproduct_id })[0];

			if(!removedItem){
				return this._insertRemovedItem(item, quantity, percentage);
			} else {
				return removedItem.update({ $set: { quantity: item.quantity + quantity }});
			}
		},
		_removeOrderRemovedItemQuantity: function(producerproduct_id, quantity){

			var item = this.getRemovedItems({ producerproduct_id: producerproduct_id })[0];

			if(item.quantity > quantity){
				return item.update({ $set: { quantity: item.quantity - quantity }});
			}
			else {
				return item.remove();
			}
		},
		_addOrderItemQuantity: function(producerproduct_id, quantity){

			var item = this.getItem({ producerproduct_id: producerproduct_id });

			return item.update({ $set: { quantity: item.quantity + quantity }});
		},
		_removeOrderItemQuantity: function(producerproduct_id, quantity){

			var item = this.getItem({ producerproduct_id: producerproduct_id });

			if(item.quantity > quantity){
				return item.update({ $set: { quantity: item.quantity - quantity }});
			}
			else {
				return item.remove();
			}
		},

		_insertReducedItem: function(item, quantity, percentage){

			return new Promise(function(resolve, reject){

				var params = Object.assign({
					quantity: quantity,
					percentage: percentage,
					userId: Meteor.userId()
				}, item);

				OrderReducedItems.insert(params, function(err){
					if(err){
						return reject(err);
					}
					return resolve();
				})
			})
		},
		_insertRemovedItem: function(item, quantity){

			return new Promise(function(resolve, reject){

				var params = Object.assign({
					quantity: quantity,
					userId: Meteor.userId()
				}, item);

				OrderRemovedItems.insert(params, function(err){
					if(err){
						return reject(err);
					}
					return resolve();
				})
			})
		},
		_insertItem: function(params){

			return new Promise(function(resolve, reject){

				OrderItems.insert(params, function(err){
					if(err){
						return reject(err);
					}
					return resolve();
				})
			})
		}
	},
	Utils.collectionHelpersBase(Orders)
);

Orders.helpers(helpers);
