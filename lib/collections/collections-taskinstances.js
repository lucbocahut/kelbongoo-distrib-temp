import SyncedCollection from '/imports/db_sync';
TaskInstances = new SyncedCollection('taskinstances', { idGeneration : 'MONGO' });

TaskInstancesSchema = new SimpleSchema({
	name : {
		type : String
	},
	description : {
		type : String
	},
	context : {
		type : String
	},
	// in minutes
	time : {
		type : Number
	},
	people : {
		type : Number
	},
	task_id : {
		type : Mongo.ObjectID,
		optional : true
	},

	done : {
		type : Date,
		optional : true
	},
	doneBy : {
		type : String,
		optional : true
	},
	tags : {
		type : [String],
		optional : true,
		defaultValue : []
	},
	meta : {
		type : Object,
		blackbox : true,
		optional : true
	}
});

TaskInstances.attachSchema(TaskInstancesSchema);
