/*
*	********
*	ACCOUNTS
*	********
*/

FlowRouter.route('/login', {
	triggersEnter : [
		Utils.checkNotLoggedIn
	],
	action : function(params){
		BlazeLayout.render('base_layout', { view : 'login' });
	}
});

FlowRouter.route('/forgotpassword', {
	triggersEnter : [
		Utils.checkNotLoggedIn
	],
	action : function(params){
		BlazeLayout.render('base_layout', { view : 'forgotpassword' });
	}
});

FlowRouter.route('/reset-password/:token', {
	triggersEnter : [
		Utils.checkNotLoggedIn
	],
	action : function(params){
		BlazeLayout.render('base_layout', { view : 'resetpassword', token : params.token });
	}
});
