FlowRouter.route('/accueil', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'accueil_home' });
    }
});

FlowRouter.route('/accueil/boutiqueorders', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'accueil_boutiqueorders' });
    }
});

FlowRouter.route('/accueil/boutiqueorders/new', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'accueil_boutiqueorders_new' });
    }
});

FlowRouter.route('/accueil/boutiqueorders/:boutiqueorder_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'accueil_boutiqueorders_detail' });
    }
});

FlowRouter.route('/accueil/:order_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkValidObjectID,
		Utils.checkRole
	],
	action: function(params) {

    BlazeLayout.render("base_layout", { view : 'accueil_order' });
  }
});
