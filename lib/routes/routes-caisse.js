FlowRouter.route('/caisse', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'caisse_home' });
    }
});

FlowRouter.route('/caisse/boutiqueorders', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'caisse_boutiqueorders' });
    }
});

FlowRouter.route('/caisse/boutiqueorders/new', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'caisse_boutiqueorders_new' });
    }
});

FlowRouter.route('/caisse/boutiqueorders/:boutiqueorder_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'caisse_boutiqueorders_detail' });
    }
});

FlowRouter.route('/caisse/:order_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkValidObjectID,
		Utils.checkRole
	],
	action: function(params) {

    BlazeLayout.render("base_layout", { view : 'caisse_order' });
  }
});
