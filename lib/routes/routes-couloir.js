FlowRouter.route('/couloir', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'couloir_home' });
    }
});
