// /*
// *	************
// *	GLOBALORDERS
// *	************
// */
//
//
//
// FlowRouter.route('/caisse/boutiqueorders', {
// 	triggersEnter : [
// 		Utils.checkLogin,
// 		Utils.checkGlobalOrder,
// 		Utils.checkRole
// 	],
// 	action: function(params, queryParams) {
//         BlazeLayout.render("base_layout", { view : 'distribution_caisse_boutiqueorders' });
//     }
// });
//
// FlowRouter.route('/caisse/boutiqueorders/new', {
// 	triggersEnter : [
// 		Utils.checkLogin,
// 		Utils.checkGlobalOrder,
// 		Utils.checkRole
// 	],
// 	action: function(params, queryParams) {
//         BlazeLayout.render("base_layout", { view : 'distribution_caisse_boutiqueorders_new' });
//     }
// });
//
// FlowRouter.route('/caisse/boutiqueorders/:boutiqueorder_id', {
// 	triggersEnter : [
// 		Utils.checkLogin,
// 		Utils.checkGlobalOrder,
// 		Utils.checkRole
// 	],
// 	action: function(params, queryParams) {
//         BlazeLayout.render("base_layout", { view : 'distribution_caisse_boutiqueorders_detail' });
//     }
// });
//
// FlowRouter.route('/distribution/:role', {
// 	triggersEnter : [
// 		Utils.checkLogin,
// 		Utils.checkGlobalOrder,
// 		Utils.checkRole,
// 		Utils.checkDistributionQueryParams
// 	],
// 	action: function(params, queryParams) {
//
//         if(params.role === "caisse"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_caisse' });
//
// 		} else if (params.role === "couloir"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_couloir' });
//
// 		} else if (params.role === "table"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_table' });
//
// 		} else if (params.role === "accueil"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_accueil' });
// 		}
//     }
// });
//
// FlowRouter.route('/distribution/:role/:order_id', {
// 	triggersEnter : [
// 		Utils.checkLogin,
// 		Utils.checkGlobalOrder,
// 		Utils.checkValidObjectID,
// 		Utils.checkRole
// 	],
// 	action: function(params) {
//
//         if(params.role === "caisse"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_caisse_order' });
//
// 		} else if (params.role === "couloir"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_couloir_order' });
//
// 		} else if (params.role === "table"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_table_order' });
//
// 		} else if (params.role === "accueil"){
//
// 			BlazeLayout.render("base_layout", { view : 'distribution_accueil_order' });
// 		}
//     }
// });
