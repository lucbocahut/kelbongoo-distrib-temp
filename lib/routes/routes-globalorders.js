FlowRouter.route('/globalorders', {
	triggersEnter : [
		Utils.checkLogin
	],
	action: function(params) {
        BlazeLayout.render("base_layout", { view : 'globalorders' });
    }
});
