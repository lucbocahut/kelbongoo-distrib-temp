FlowRouter.route('/', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder
	],
	action: function(params) {
    BlazeLayout.render("base_layout", { view : 'home' });
  }
});
