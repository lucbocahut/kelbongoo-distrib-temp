FlowRouter.route('/table', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'table_home' });
    }
});

FlowRouter.route('/table/boutiqueorders', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'table_boutiqueorders' });
    }
});

FlowRouter.route('/table/boutiqueorders/new', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'table_boutiqueorders_new' });
    }
});

FlowRouter.route('/table/boutiqueorders/:boutiqueorder_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkRole
	],
	action: function(params, queryParams) {
        BlazeLayout.render("base_layout", { view : 'table_boutiqueorders_detail' });
    }
});

FlowRouter.route('/table/:order_id', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkValidObjectID,
		Utils.checkRole
	],
	action: function(params) {

    BlazeLayout.render("base_layout", { view : 'table_order' });
  }
});
