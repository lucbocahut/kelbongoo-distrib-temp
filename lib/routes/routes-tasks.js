FlowRouter.route('/tasks', {
	triggersEnter : [
		Utils.checkLogin,
		Utils.checkGlobalOrder,
		Utils.checkValidObjectID
	],
	action: function(params) {
        BlazeLayout.render("base_layout", { view : 'tasks_home' });
    }
});
