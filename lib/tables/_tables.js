TabularTables = {};

TabularTables.langOptions = {
    processing :     "Traitement en cours...",
    search :         "",
    lengthMenu :     "Afficher _MENU_ éléments",
    info :           "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
    infoEmpty :      "",
    infoFiltered :   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
    infoPostFix :    "",
    loadingRecords : "Chargement en cours...",
    zeroRecords :    "Aucun &eacute;l&eacute;ment &agrave; afficher",
    emptyTable :     "Pas de donn&eacute;e &agrave; afficher",
    paginate : {
        first :      "Premier",
        previous :   "Pr&eacute;c&eacute;dent",
        next :       "Suivant",
        last :       "Dernier"
    },
    aria : {
        sortAscending :  ": activer pour trier la colonne par ordre croissant",
        sortDescending : ": activer pour trier la colonne par ordre d&eacute;croissant"
    }
}
