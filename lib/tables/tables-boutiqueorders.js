TabularTables.BoutiqueOrders = new Tabular.Table({
    name: "BoutiqueOrders",
    collection: BoutiqueOrders,
    columns: [
        { data : "createdAt", className : 'text-center vertical-center', searchable : false, width : '100px', tmpl: Meteor.isClient && Template.display_table_createdat_date },
        { data : "email", title: "Adresse mail", className : 'vertical-center' },
        { data : "units_total", title: "Unités", className : 'text-center vertical-center hidden-xs', searchable : false, width : '45px' },
        { data : "amount_total", title: "Montant", className : 'text-center vertical-center', searchable : false, width : '60px', tmpl: Meteor.isClient && Template.display_table_amount_total },
        { data : "status", className : 'text-center vertical-center', searchable : false, width : '60px', tmpl: Meteor.isClient && Template.display_table_order_status }
    ],
    pagingType : 'simple',
    pageLength : 20,
    language: TabularTables.langOptions,
    bLengthChange : false,
    order: [[ 0, "desc" ]],
    search : { smart : false }
});
