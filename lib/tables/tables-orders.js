TabularTables.Orders = new Tabular.Table({
    name: "Orders",
    collection: Orders,
    extraFields : ['firstname','name','fraisDeliveredAt','secDeliveredAt'],
    columns: [
        { data : "lastname", title: "Nom", className : 'vertical-center', tmpl : Meteor.isClient && Template.display_table_order_name },
        { data : "email" },
        { data : "telephone" },
        { data : "nondispatch", title: "Non-dispatch", className : 'text-center vertical-center', searchable : false, tmpl: Meteor.isClient && Template.display_table_nondispatch },
        { data : "reduced", title: "Remisé", className : 'text-center vertical-center', searchable : false, tmpl: Meteor.isClient && Template.display_table_reduced },
        { data : "removed", title: "Manquant", className : 'text-center vertical-center', searchable : false, tmpl: Meteor.isClient && Template.display_table_removed },
        { data : "units_total", title: "Unités", className : 'text-center vertical-center hidden-xs', searchable : false, width : '45px' },
        { data : "amount_total", title: "Montant", className : 'text-center vertical-center', searchable : false, width : '60px', tmpl: Meteor.isClient && Template.display_table_amount_total },
        { data : "status", className : 'text-center vertical-center', searchable : false, width : '60px', tmpl: Meteor.isClient && Template.display_table_order_status },
        { data : "status", title: "Status", className : 'text-center vertical-center', searchable : false, width : '60px', tmpl: Meteor.isClient && Template.display_table_distribution_status }
    ],
    pagingType : 'simple',
    pageLength : 20,
    bLengthChange : false,
    language: TabularTables.langOptions,
    order: [[ 0, "asc" ]],
    search : { smart : false }
});
