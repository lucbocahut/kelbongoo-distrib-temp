
class AlertsCollection extends Mongo.Collection {

  createError(params){

    var message = "Erreur inconnu";
    if(params.message){
      message = getAlertMessage(params.message);
    }
    super.insert({
      type : "error",
      message : message
    })
  }

  createSuccess(message){

    message = message || "L'opération a réussie";
    super.insert({
      type : "success",
      message : message
    })
  }
}

function getAlertMessage(message){
	message = message.replace(/[\[\]]/g, '');
	switch(message){
		case "request-denied" : return "Votre demande n'a pas pu être acceptée. Merci de contacter votre responsable";
		case "access-denied" : return "L'accès a été refusé";
		case "method-failed" : return "L'action n'a pas réussie - merci de contacter le responsable informatique";
		case "not-found" : return "Non-trouvé";
		default : return "Il y a eu un erreur : "+message;
	}
}

Alerts = new AlertsCollection(null);
