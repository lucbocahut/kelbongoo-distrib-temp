Package.describe({
  name: 'alerts',
  version: '0.0.1'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4');
  api.use('ecmascript');
  api.use(['mongo','templating','underscore'], 'client');
  api.addFiles([
    'templates-alerts.html',
    'collections-alerts.js',
    'templates-alerts.js'
  ],
  ['client']
  );
  api.export("Alerts", ["client"]);
});
