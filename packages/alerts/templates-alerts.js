Template.alerts.helpers({
  	alerts : function(){
    	return Alerts.find();
  	}
});

Template.alerts.events({
	'click .close' : function(e,t){
		var id = $(e.currentTarget).attr('id');
		Alerts.remove(id);
	}
})
