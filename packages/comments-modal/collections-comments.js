UserComments = new Mongo.Collection('usercomments', { idGeneration: 'MONGO' });
UserCommentTypes = new Mongo.Collection('usercommenttypes', { idGeneration: 'MONGO' });


UserCommentsSchema = new SimpleSchema({
  createdAt: {
    type: Date
  },
  createdBy: {
    type: String
  },
  username: {
    type: String
  },
  comment: {
    type: String,
    optional: true
  },
  type: {
    type: String
  },
  domain: {
    type: String
  },
  image: {
    type: String,
    optional: true
  }
});

UserComments.attachSchema(UserCommentsSchema);
