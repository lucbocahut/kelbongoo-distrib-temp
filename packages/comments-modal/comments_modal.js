Template.comments_modal.onCreated(function(){

	this.showSpeech = new ReactiveVar(false);
	this.selectedType = new ReactiveVar();
	this.comment = new ReactiveVar();
	this.photoUrl = new ReactiveVar();

	if(navigator && navigator.camera){
		this.showPhoto = true;
	}

	this.subscribe('usercommenttypes');
});


Template.comments_modal.onRendered(function(){

	var self = this;

	if(window.plugins) {

		window.plugins.speechRecognition.isRecognitionAvailable(function(){

			window.plugins.speechRecognition.hasPermission(function(hasPermission){

				console.log('hasPermission', hasPermission);
				if(!hasPermission){

					window.plugins.speechRecognition.requestPermission(function(){

						self.showSpeech.set(true);
					});
				}
				else {

					self.showSpeech.set(true);
				}

			}, function(err){

				console.log('err', err);
			});
		});
	}
	else {


	}

});

Template.comments_modal.helpers({

	showSpeech: function(){
		var t = Template.instance();
		return t.showSpeech.get();
	},
	allowValidation: function(){
		var t = Template.instance();
		var comment = t.comment.get();
		var type = t.selectedType.get();
		return comment && type;
	},
	selectedType: function(){
		var t = Template.instance();
		return t.selectedType.get();
	},
	comment: function(){
		var t = Template.instance();
		return t.comment.get();
	},
	showPhoto: function(){
		var t = Template.instance();
		return t.showPhoto;
	},
	commentTypes: function(){
		var t = Template.instance();
		return UserCommentTypes.find({ domains: t.data.domain });
	}
});

Template.comments_modal.events({
	'click .speak' : function(e,t){

		var success = function(matches){
			t.$('#comment').append(matches[0]);
			var val = t.$('#comment').val();
			t.comment.set(val);
		}

		var error = function(err){
			t.$('.modal').modal('hide');
			Utils.handleResponse(err);
		}

		window.plugins && window.plugins.speechRecognition.startListening(success, error, {
			language: "fr-FR",
			matches : 5
		});
	},
	'click .image' : function(e,t){

		var success = function(uri){

			window.resolveLocalFileSystemURL(uri, function(entry) {

				entry.file(function(file){

					handleImageDisplay(t, file);
				});

			});

		}

		var error = function(err){
			t.$('.modal').modal('hide');
			Utils.handleResponse(err);
		}

		navigator.camera.getPicture(success, error, {
			quality: 30,
        	targetWidth: 600,
        	targetHeight: 600,
        	destinationType: 1
        });
	},
	'change #upload' : function(e,t){

		handleImageDisplay(t, $(e.currentTarget)[0].files[0]);

	},
	'input #comment' : function(e,t){
		var val = $(e.currentTarget).val();
		if(!val) {
			t.allowValidation.set(false);
		}
		t.comment.set(val);
	},
	'change #type' : function(e,t){
		var val = $(e.currentTarget).val();
		t.selectedType.set(val);
		$('#comment').focus();
	},
	'submit #comments_form': function(e,t){

		e.preventDefault();

		var comment = t.comment.get();
		var type = t.selectedType.get();

		if(!comment || !type){
			return;
		}

		uploadImage(t.currentImage, function(err, url){

			if(err) {
				t.$('.modal').modal('hide');
				return Utils.handleResponse(err);
			}

			t.data.onValidate({
				comment: comment,
				type: type,
				image: url,
				domain: t.data.domain
			}, function(err){

				t.$('.modal').modal('hide');
				Utils.handleResponse(err);
			});
		});
	}
});

function handleImageDisplay(t, file) {

	var reader = new FileReader();

    reader.onload = function (e) {
    	t.currentImage = dataURItoBlob(e.target.result);
        t.$('.image-preview > img').attr('src', e.target.result);
        t.$('.image-preview').css({ display: 'block' });
    }

    reader.readAsDataURL(file);

}

function uploadImage(file, callback){

	// if no image attached, just upload the comment
	if(!file) return callback();

	var uploader = new Slingshot.Upload("commentsImagesUpload");

	uploader.send(file, function(err, url){
		if(err) console.log('err', JSON.stringify(err));
		callback(err, url);
	});
}

function dataURItoBlob(dataURI) {
    var byteString = atob(dataURI.split(',')[1]);
    var ab = new ArrayBuffer(byteString.length);
    var ia = new Uint8Array(ab);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    return new Blob([ab], { type: 'image/jpeg' });
};
