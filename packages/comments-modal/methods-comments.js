var Future = Npm.require('fibers/future');

Meteor.methods({
	'comments.new' : function(data){

		if(!Meteor.userId()){
			throw new Meteor.Error('access-refused');
		}

		check(data, {
			comment: String,
			type: String,
			image: Match.Optional(String),
			domain: String
		});

		var fut = new Future();

		var insertData = {
			createdAt: new Date(),
			createdBy: Meteor.userId(),
			username: Meteor.user().username,
			comment: data.comment,
			type: data.type,
			domain: data.domain
		};
		if(data.image){
			insertData.image = data.image;
		}

		UserComments.insert(insertData, function(err){
			if(err){
				return fut.error(err);
			}
			fut.return();
		});

		return fut.wait();
	}
});
