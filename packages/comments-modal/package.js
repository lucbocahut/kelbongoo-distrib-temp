Package.describe({
  name: 'comments-modal',
  version: '0.0.1'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4');
  api.use('ecmascript');
  api.use('mongo');
  api.use('aldeed:simple-schema');
  api.use(['templating','blaze'], 'client');
  api.addFiles('comments_modal.html', 'client');
  api.addFiles("comments_modal.js", 'client');
  api.addFiles("methods-comments.js", 'server');
  api.addFiles("publications-comments.js", 'server');
  api.addFiles("collections-comments.js", ['server','client']);

  api.export('UserComments', ['client','server']);
  api.export('UserCommentTypes', ['client','server']);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('comments-modal');
  api.mainModule('comments-modal-tests.js');
});
