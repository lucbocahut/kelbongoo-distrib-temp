function prepend(type, prefix) {
    return function(){
        let args = [].slice.call(arguments);
        if(prefix){
          args.unshift(prefix);
        }
        console[type||'log'].apply(console, args);
    }
}

logger = {
    log: prepend('log', new Date() + ' - log: '),
    info: prepend('info', new Date() + ' - info: '),
    warn: prepend('warn', new Date() + ' - warn: '),
    error: prepend('error', new Date() + ' - error: ')
};
