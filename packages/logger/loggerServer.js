var Winston = Npm.require('winston');

if(!Meteor.isTest){

  var console = new Winston.transports.Console({
      name: "console",
      timestamp: true,
      colorize: true
  });

  logger = new Winston.Logger({
      transports: [
          console
      ]
  });

  var oldLogFunc = logger.error;

  logger.error = function() {
    var args = Array.prototype.slice.call(arguments, 0);
    for(var i = 0; i < args.length; i++) {
      if(args[i] instanceof Error){
        args[i] = { message: args[i].message, stack: args[i].stack }
      }
    }
    return oldLogFunc.apply(this, args);
  }
}
else {
  logger = {
    info: function(){},
    warn: function(){},
    error: function(){}
  }
}
