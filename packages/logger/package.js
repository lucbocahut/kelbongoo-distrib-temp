Package.describe({
  name: 'logger',
  version: '0.0.1'
});

Npm.depends({
  "winston": "2.3.1"
});

Package.onUse(function(api) {
  api.versionsFrom('1.4');
  api.use('ecmascript');
  api.addFiles('loggerClient.js', 'client');
  api.addFiles('loggerServer.js', 'server');
  api.export("logger", ["server","client"]);
});
