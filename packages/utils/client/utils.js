var UtilsClient = {
	createModal : function(modal_name, data, options){
		var rendered;
		if(!Template[modal_name]) {
			// sAlert.error('Modal ' + modal_name + ' not found'); 
		} else {
			rendered = Blaze.renderWithData(Template[modal_name], data, document.body);
			$('#'+modal_name).modal('show');
			$('#'+modal_name).on('hidden.bs.modal', function(){
				Blaze.remove(rendered);
			});
			return rendered;
		}	
	},
	'serializeObject' : function($form, fieldsToOmit){
		if(!$form.length || !($form instanceof $)) throw new Error('Form data invalid');
		fieldsToOmit = fieldsToOmit||[];
		
		var form = $form[0],
			elems = form.elements,
			result = {};
			
		_.each(elems, function(elem, index, all){
			if(!elem.type||!elem.name) return;
			var type = $(elem).prop('type'),
				name = $(elem).prop('name'),
				val = $(elem).val();
			
			if(!name) return;
			if(_.contains(fieldsToOmit, name)) return;
			if (result[name]) {
				// if name already exists, we assume it's an array and we push the new value after
				if (!result[name].push) result[name] = [result[name]];
				result[name].push(getValue(name, type, val));
			} else {
				result[name] = getValue(name, type, val);
			}

		
			// TODO - need to handle deep-from-flat case, ie. names like 'address.street'
			function getValue(name, type, val){
				if(_.contains(['text','radio','textarea','select-one'], type)) {
					if(_.isString(val)||type === 'text') {
						val = $.trim(val+'');
					} else if(!isNaN(val) && val !== '') {
						val = parseFloat(val);
					}
					return val;
				} else if (type === 'select-multiple'){
					if(!val) return [];
					for(var i=0;i<val.length;i++){
						if(!isNaN(val[i]) && val[i] !== '') val[i] = parseFloat(val[i]);
					}
					return val;
				} else if (type === 'checkbox') {
					// all checkboxes should have a value !!!
					if(val === 'true'|| !val) 
						return (val === 'true') ? true : false;
					else
						return val;
				} else {
					if(!isNaN(val) && val !== '') return parseFloat(val);
					else return val;
				}
			}
		});
		return deepFromFlat(result);
	},
	handleResponse : function(err){
		var id;

		if(err) {
            var message = getAlertMessage(err.message);
            id = Alerts.insert({
                type : "error",
                message : message
            });
            $('html,body').scrollTop(0);
        } else {
            id = Alerts.insert({
                type : "success",
                message : "L'opération a réussie"
            });
        }

		Meteor.setTimeout(function(){
			var alert = Alerts.findOne(id);
			if(alert && alert.type !== 'error'){
				Alerts.remove(id);
			}
		}, 5000);

	},
	'confirm' : function(text, callback){
		UtilsClient.createModal('confirm_modal', { text : text, callback : callback });
	}
}

function deepFromFlat(o) {
	if(!o||!_.isObject(o)) return;
	var k, key, oo, part, parts, t;
	oo = {};
	t = void 0;
	parts = void 0;
	part = void 0;
	for (k in o) {
		t = oo;
		parts = k.split(".");
		key = parts.pop();
		while (parts.length) {
			part = parts.shift();
			t = t[part] = t[part] || {};
		}
		t[key] = o[k];
	}
	return oo;
}

function getAlertMessage(message){
	message = message.replace(/[\[\]]/g, '');
	switch(message){
		case "request-denied" : return "Votre demande n'a pas pu être acceptée. Merci de contacter votre responsable";
		case "access-denied" : return "L'accès a été refusé";
		case "method-failed" : return "L'action n'a pas réussie - merci de contacter le responsable informatique";
		case "not-found" : return "Non-trouvé";
		default : return "Il y a eu un erreur : "+message;
	}
}

Utils = _.extend(UtilsClient, UtilsShared);