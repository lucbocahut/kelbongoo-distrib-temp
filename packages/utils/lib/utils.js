UtilsShared = {
    // formatDate : function(date, format){
    //     var monthsShort = "jan_fev_mar_avr_mai_juin_juil_aôut_sept_oct_nov_déc",
    //         monthsLong = "janvier_fevrier_mars_avril_mai_juin_juillet_aôut_septembre_octobre_novembre_décembre",
    //         daysLong = "dimanche_lundi_mardi_mercredi_jeudi_vendredi_samedi";
    //         daysShort = "dim_lun_mar_mer_jeu_ven_sam";

    //     function ensureTwo(number){
    //         var string = number+'';
    //         if(string.length === 1) {
    //             return '0'+string;
    //         }
    //         return string;
    //     }

    //     if(!date) return;

    //     try {
    //         if(typeof date === 'string') date = new Date(date);
    //         if(format === 'DD MM YY') return ensureTwo(date.getDate()) + ' ' + ensureTwo(date.getMonth()+1) + ' ' + date.getFullYear().toString().substr(2);
    //         if(format === 'ddd MM YY') return daysShort.split('_')[date.getDay()] + ' ' + ensureTwo(date.getDate()) + ' ' + ensureTwo(date.getMonth()+1) + ' ' + date.getFullYear().toString().substr(2);
    //         if(format === 'HH.mm') return ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //         if(format === 'DDD MMMM') return daysLong.split('_')[date.getDay()] + ' ' + date.getDate() + ' ' + monthsLong.split('_')[date.getMonth()];
    //         if(format === 'DDD MMMM HH:mm') return daysLong.split('_')[date.getDay()] + ' ' + date.getDate() + ' ' + monthsLong.split('_')[date.getMonth()] + ' ' + ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //         if(format === 'DD MM YYYY') return ensureTwo(date.getDate()) + ' ' + ensureTwo((date.getMonth()+1)) + ' ' + date.getFullYear();
    //         if(format === 'DD MMMM') return ensureTwo(date.getDate()) + ' ' + monthsLong.split('_')[date.getMonth()];
    //         if(format === 'DD MMMM YYYY') return ensureTwo(date.getDate()) + ' ' + monthsLong.split('_')[date.getMonth()] + ' ' + date.getFullYear();
    //         if(format === 'HH:mm') return ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //         if(format === 'DD MM YYYY HH:mm') return ensureTwo(date.getDate()) + ' ' + ensureTwo(date.getMonth()+1) + ' ' + date.getFullYear() + ' ' + ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //         if(format === 'DD MMM YYYY') return ensureTwo(date.getDate()) + ' ' + monthsShort.split('_')[date.getMonth()] + ' ' + date.getFullYear();
    //         if(format === 'DD MMMM YYYY HH:mm') return ensureTwo(date.getDate()) + ' ' + monthsLong.split('_')[date.getMonth()] + ' ' + date.getFullYear() + ' ' + ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //         if(format === 'DDD MMMM YYYY') return daysLong.split('_')[date.getDay()] + ' ' + date.getDate() + ' ' + monthsLong.split('_')[date.getMonth()] + ' ' + date.getFullYear();
    //         if(format === 'DD-MM-YYYY') return ensureTwo(date.getDate()) + '-' + ensureTwo(date.getMonth()+1) + '-' + date.getFullYear().toString();
    //         if(format === 'DD-MM-YYYY HH:mm') return ensureTwo(date.getDate()) + '-' + ensureTwo(date.getMonth()+1) + '-' + date.getFullYear().toString() + ' ' + ensureTwo(date.getHours()) + ':' + ensureTwo(date.getMinutes());
    //     } catch(err) {
    //         console.log('err', err);
    //     }
    // },
    formatDate : function(date, format){
        if(_.isString(date)) return moment.tz(date, "Europe/Paris").format(format);
        return moment(date).format(format);
    },
    formatMoney : function(n){
        var curr = curr === undefined ? "€" : curr,
            c = isNaN(c = Math.abs(c)) ? 2 : c,
            d = d === undefined ? "," : d,
            t = t === undefined ? "." : t,
            s = n < 0 ? "-" : "",
            i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
            j = (j = i.length) > 3 ? j % 3 : 0;
        return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "") + " " + curr;
    },

    // ORDER STATUS
    //
    // 0 - created
    // 1 - validated (incl created)
    // 2 - compiled
    // 3 - arrived locale
    // 4 - sec NOT frais
    // 5 - frais NOT sec
    // 6 - frais AND sec (NOT PAID)
    // 7 - done (i.e. also PAID)
    // 8 - cancelled
    // 9 - error
    //
    // NOT linear !
    // two possibilities :
    // - 0 -> 1 -> 2 -> 3 -> 4 -> 6 -> 7
    // - 0 -> 1 -> 2 -> 3 -> 5 -> 6 -> 7
    // - 0 -> 1 -> 2 -> 3 -> 4 -> 7 (i.e. paid in advance)

    formatOrderStatus : function(status){

        switch(status){
            case 0 : return "Créée";
            case 1 : return "Validée";
            case 2 : return "Compilée";
            case 3 : return "Au local";
            case 4 : return "Fini";
            case 5 : return "En attente";
            case 6 : return "Annulée";
            case 7 : return "Erreur";
        }
    },
    formatGlobalOrderStatus : function(status){

        switch(status){
            case 0 : return "Créée";
            case 1 : return "Prête";
            case 2 : return "En cours";
            case 3 : return "Clôturée";
            case 7 : return "Terminée";
            case 8 : return "Erreur";
            default : return "Locaux";
        }
    },
    formatRestockStatus : function(status){

        /*
        *   STATUS
        *   0 - created
        *   1 - validated (by boutique)
        *   2 - received (by entrepot)
        *   3 - filled (by entrepot)
        *   4 - received (by boutique)
        *   5 - done
        *   6 - cancelled (once we pass stage 1, we cancel rather than deleting)
        *   7 - error
        */

        switch(status){
            case 0 : return "Créée";
            case 1 : return "Envoyé";
            case 2 : return "Reçu";
            case 3 : return "Rempli";
            case 4 : return "Receptionné";
            case 5 : return "Terminé";
            case 6 : return "Annulé";
            case 7 : return "Erreur";
            default : return;
        }
    },
    'categories' : ["fruits, légumes","produits laitiers","volailles","viandes","poissons, escargots","épicerie","pains","boissons"],
    checkRole: checkRole,
    checkDistributionQueryParams: checkDistributionQueryParams,
    checkValidObjectID: checkValidObjectID,
    checkGlobalOrder: checkGlobalOrder,
    checkLogin: checkLogin,
    checkNotLoggedIn: checkNotLoggedIn,
    collectionHelpersBase: function(collection){
      return {
      	update: function(modifier){
          return new Promise((resolve,reject) => {
            collection.update(this._id, modifier, function(err){
              if(err){
                return reject(err);
              }
              return resolve();
            })
          });
        },
        remove: function(){
          return new Promise((resolve,reject) => {
            collection.remove(this._id, function(err){
              if(err){
                return reject(err);
              }
              return resolve();
            })
          });
        }
      }
    }
}

function checkLogin(context, redirect){
	if(!Meteor.loggingIn()&&!Meteor.userId()) {
		return redirect('/login');
	}
}

function checkNotLoggedIn(context, redirect){
	if(Meteor.loggingIn()||Meteor.userId()){
		redirect('/');
	}
}

function checkGlobalOrder(context, redirect){
	var currentGlobalOrder = Session.get('currentGlobalOrder');
	var currentLocale = Session.get('currentLocale');
	if(!currentGlobalOrder || !currentGlobalOrder instanceof Mongo.ObjectID || !currentLocale){
		redirect('/globalorders');
	}
}

function checkValidObjectID(context, redirect){
	if(!context.params) return;
	var p = context.params;
	if(p.order_id && p.order_id.length !== 24){
		return redirect('/error');
	}
	if(p.task_id && p.task_id.length !== 24){
		return redirect('/error');
	}
}

function checkRole(context, redirect){
	return;
}

function checkDistributionQueryParams(context, redirect){
	return;
}
