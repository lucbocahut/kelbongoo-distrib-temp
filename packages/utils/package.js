Package.describe({
    name: "utils",
});

Npm.depends({
    'wkhtmltopdf' : "0.1.5",
    'google-oauth-jwt' : "0.2.0"
});

Package.onUse(function(api) {

    api.versionsFrom("1.0");

    api.use(['underscore','mongo'], ['client','server']);

    api.export('Utils',['client','server']);

    api.addAssets([
        'templates/order-pdf-template.html'
    ],'server');

    api.addFiles([
        "lib/utils.js",
        "server/utils-boutiqueorders.js",
        "server/utils-bpps.js",
        "server/utils-golpps.js",
        "server/utils-orders.js",
        "server/utils-tasks.js",
        "server/utils-distribution.js",
        "server/utils.js"
    ], 'server');

    api.addFiles([
        "lib/utils.js",
        "client/utils.js"
    ], 'client');

});