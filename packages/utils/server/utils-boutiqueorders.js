UtilsBoutiqueOrders = {

	'boutiqueorders.upsertWithBpp' : function(params, callback){

		var _getBoutiqueOrder = Utils.promisifyFunction(getBoutiqueOrder);
		var _addItemToBoutiqueOrder = Utils.promisifyFunction(addItemToBoutiqueOrder);
		var _updateBppQuantityAvailable = Utils.promisifyFunction(Utils['bpps.createEvent']);

		var promise =_getBoutiqueOrder(params)
						.then(function(boutiqueorder){
							return _addItemToBoutiqueOrder(boutiqueorder, params.bpp, params.quantity);
						})
						.then(function(){
							return _updateBppQuantityAvailable(params.bpp, {
								type : 1,
								quantity : params.quantity
							});
						});

		Utils.waitFor(promise, callback);

	},
	// quantity is the quantity i WANT to impose
	'boutiqueorders.updateItemQuantity' : function(boutiqueorder, bpp, itemsIndex, quantity, callback){

		var _updateBoutiqueOrder = Utils.promisifyFunction(updateBoutiqueOrderItem);
		var _updateBppQuantityAvailable = Utils.promisifyFunction(Utils['bpps.createEvent']);

		var delta = quantity - boutiqueorder.items[itemsIndex].quantity;

		var promise = _updateBoutiqueOrder(boutiqueorder, itemsIndex, quantity)
			.then(function(){
				return _updateBppQuantityAvailable(bpp, {
					type : 1,
					quantity : delta
				});
			});

		Utils.waitFor(promise, callback);
		
	},
	// not possible to reduce boutiqueitems by 100%...
	'boutiqueorders.reduceItem' : function(boutiqueorder, itemsIndex, quantity, percentage, callback){

		var items = boutiqueorder.items.slice(0);
		var reduced = boutiqueorder.reduced.slice(0);
		var item = items[itemsIndex];

		if(item.quantity === quantity){

			items.splice(itemsIndex, 1);
			reduced.push(
				_.extend({ 
					percentage : percentage, 
					userId : Meteor.userId() 
				}, item)
			);
		} else {

			items[itemsIndex].quantity -= quantity;
			reduced.push(
				_.extend(
					{ 
						percentage : percentage, 
						userId : Meteor.userId() 
					}, 
					item, 
					{ 
						quantity : quantity 
					}
				)
			);
		}

		var modifier = {
			$set : {
				items : items,
				amount_total : boutiqueorder.amount_total - (item.kelbongoo_price * ((100 + item.tva)/100) * quantity * (percentage/100)),
				reduced : reduced
			}
		};

		BoutiqueOrders.update(boutiqueorder._id, modifier, callback);

	},
	'boutiqueorders.unreduceItem' : function(boutiqueorder, reducedIndex, callback){

		var items = boutiqueorder.items.slice(0);
		var reduced = boutiqueorder.reduced.slice(0);
		var reducedItem = reduced[reducedIndex]
		var itemsIndex = items.map(getPPId).indexOf(reducedItem.producerproduct_id._str);

		if(~itemsIndex){

			items[itemsIndex].quantity += reducedItem.quantity;

		} else {

			items.push(
				_.omit(reducedItem, 'percentage','userId')
			);
		}

		reduced.splice(reducedIndex, 1);
		var modifier = {
			$set : {
				items : items,
				amount_total : boutiqueorder.amount_total + (reducedItem.kelbongoo_price * reducedItem.quantity * ((100 + reducedItem.tva)/100) * (reducedItem.percentage/100)),
				reduced : reduced
			}
		};

		BoutiqueOrders.update(boutiqueorder._id, modifier, callback);

	},
	'boutiqueorders.pay' : function(boutiqueorder_id, paymentMethod, callback){

		var modifier = { 
			$set : { 
				paidAt : new Date(),
				payment_data : {
					userId : Meteor.userId(),
					method : paymentMethod
				}
			}
		};
		BoutiqueOrders.update(boutiqueorder_id, modifier, callback);
	},
	'boutiqueorders.getBppDetails' : function(boutique, producerproduct_id){

		var ppOptions = { 
			fields : { 
				kelbongoo_price : 1,
				tva : 1,
				_id : 0,
				// temp
				producer_name : 1,
				product_name : 1,
				unit_display : 1
			}
		};

		var pp = ProducerProducts.findOne({ _id : producerproduct_id, locales : boutique }, ppOptions);
		if(!pp) throw new Meteor.Error('not-found');
		// var bpp = BoutiqueProducerProducts.findOne({ producerproduct_id : producerproduct_id, boutique : boutique });
		// if(!bpp) throw new Meteor.Error('not-found');

		return _.extend({ quantity_available : 10 }, pp);
	},
}

var getBoutiqueOrder = function(params, callback){

	if(params.boutiqueorder) return callback(null, params.boutiqueorder);

	var insertData = {
		boutique : params.boutique,
		createdAt : new Date()
	};
	if(params.order) {
		insertData.order_id = params.order._id;
		insertData.globalorder_id = params.order.globalorder_id;
		insertData.email = params.order.email;
	} else {
		if(params.email) insertData.email = params.email;
		if(params.globalorder_id) insertData.globalorder_id = params.globalorder_id;
	}

	BoutiqueOrders.insert(insertData, function(err, boutiqueorder_id){
		callback(err, BoutiqueOrders.findOne(boutiqueorder_id));
	});
}



var addItemToBoutiqueOrder = function(boutiqueorder, bpp, quantity, callback){

	console.log('206', arguments);

	var items = boutiqueorder.items.slice(0);
	var item = _.pick(bpp, ['producer_name','product_name','producerproduct_id','unit_display']);
	var pp = ProducerProducts.findOne(bpp.producerproduct_id);
	if(!pp) return callback(new Error('not-found'));

	item.kelbongoo_price = pp.kelbongoo_price;
	item.tva = pp.tva;
	item.quantity = quantity;

	items.push(item);
	console.log('item', item);

	var amount_total = boutiqueorder.amount_total + (item.kelbongoo_price * quantity * ((100 + item.tva)/100));
	var units_total = boutiqueorder.units_total + quantity;

	var modifier = {
		$set : { 
			items : items,
			amount_total : amount_total,
			units_total : units_total
		}
	};
	console.log('227');

	BoutiqueOrders.update(boutiqueorder._id, modifier, callback);
}

var updateBoutiqueOrderItem = function(boutiqueorder, itemsIndex, quantity, callback){

	console.log('163', arguments);

	var items = boutiqueorder.items.slice(0);
	var item = _.extend({}, items[itemsIndex]);
	console.log('item', item);

	if(!quantity){

		items.splice(itemsIndex, 1);

	} else {

		items[itemsIndex].quantity = quantity;
	}


	if(!items.length && !boutiqueorder.reduced.length){

		BoutiqueOrders.remove(boutiqueorder._id, callback);

	} else {

		modifier = {
			$set : {
				items : items,
				amount_total : boutiqueorder.amount_total + (item.kelbongoo_price * (quantity - item.quantity) * ((100 + item.tva)/100)),
				units_total : boutiqueorder.units_total + (quantity - item.quantity)
			}
		};
		console.log('modifier', modifier, 'item', item);

		BoutiqueOrders.update(boutiqueorder._id, modifier, callback);
	}

}

var getPPId = function(item){ 
	return item.producerproduct_id._str; 
};