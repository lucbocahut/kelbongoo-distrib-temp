var _ObjectID = MongoInternals.NpmModule.ObjectID;

UtilsBoutiqueProducerProducts = {

	// *** IMPORTANT ***
	// *** all stock adjustments must pass via this method ! ***

	'bpps.createEvent' : function(bpp, data, callback){

		var queue = [];
		var _updateBpp = Utils.promisifyFunction(updateBpp);
		var _upsertBoutiqueRestock = Utils.promisifyFunction(upsertBoutiqueRestock);
		var _adjustEvents = Utils.promisifyFunction(adjustEventsToNewInventory);
		var _insertNewEvent = Utils.promisifyFunction(insertEvent);

		// update the quantity_available of the boutique producerproduct
		queue.push(
			_updateBpp(bpp, data)
		);

		if(!bpp.temporary && bpp.target_quantity && bpp.quantity_available - data.quantity < bpp.target_quantity){

			// this is the only place we create boutiquerestocks's, so all adjustments to bpp quantities must pass via this method...
			// so 'sorties de stock' for a preparation must in the end call this method on each bpp that was ordered...

			queue.push(
				_upsertBoutiqueRestock(bpp, bpp.target_quantity + data.quantity - bpp.quantity_available)
			);
		}

		if(!bpp.temporary && data.type === 2){

			// if it's an inventory, and the totals don't match up, we have to reconcile them with a special 're-calage' event

			queue.push(
				_adjustEvents(bpp)
			);
		}

		if(!bpp.temporary){

			queue.push(
				_insertNewEvent(bpp, data)
			);
		}

		Utils.waitForAll(queue, callback);

	}
}

var updateBpp = function(bpp, data, callback){
	console.log('args', arguments);

	var modifier = {};

	if(bpp.temporary && bpp.quantity_available === -data.quantity){

		// rab bpp's get removed as soon as they're sold out...
		return BoutiqueProducerProducts.remove(bpp._id, callback);
	}

	if(data.type === 0){

		modifier.$inc = { quantity_available : data.quantity };

		if(data.lots){

			modifier.$addToSet = {
				lots : { $each : data.lots }
			};
		}

	} else if (data.type === 1){

		modifier.$inc = { quantity_available : -data.quantity };

	} else if (data.type === 2){

		modifier.$set = {
			quantity_available : data.quantity,
			last_inventory : new Date()
		};

		if (data.lots){

			modifier.$addToSet = {
				lots : { $each : data.lots }
			};
		}

	} else if (data.type === 3) {

		modifier.$inc = { quantity_available : -data.quantity };

	} else {

		return callback(new Error('invalid-params'));
	}

	BoutiqueProducerProducts.update(bpp._id, modifier, callback);
}

var adjustEventsToNewInventory = function(bpp, callback){

	var lastInventoryCursor = BoutiqueProducerProductsEvents.find({
			producerproduct_id : bpp.producerproduct_id,
			boutique : bpp.boutique
		},{
			sort : { date : -1 },
			limit : 1
		});

	if(!lastInventoryCursor.count()) {
		return callback();
	}

	var lastInventory = lastInventoryCursor.fetch()[0];

	var sinceLastInventoryCursor = BoutiqueProducerProductsEvents.find({
			producerproduct_id : bpp.producerproduct_id,
			boutique : bpp.boutique,
			date : { $gt : lastInventory.date },
			type : { $ne : 2 }
		});

	if(!sinceLastInventoryCursor.count()){
		return callback();
	}

	var reduceEventQuantities = function(output, bppEvent){

		if(bppEvent.type === 0){

			return output += bppEvent.quantity;

		} else if (bppEvent.type === 1 || bppEvent.type === 3){

			return output -= bppEvent.quantity;
		}
	}

	var expectedQuantityAvailable = sinceLastInventoryCursor.fetch().reduce(reduceEventQuantities, lastInventory.quantity||0);

	if(expectedQuantityAvailable !== bpp.quantity_available){

		BoutiqueProducerProductsEvents.insert({
			producerproduct_id : boutiqueproducerproduct.producerproduct_id,
			producer_name : boutiqueproducerproduct.producer_name,
			product_name : boutiqueproducerproduct.product_name,
			producer_id : boutiqueproducerproduct.producer_id,
			type : 4,
			quantity : boutiqueproducerproduct.quantity_available - expectedQuantityAvailable
		}, callback);

	} else {
		return callback();
	}
}

var upsertBoutiqueRestock = function(bpp, quantity, callback){

	var selector = {
		boutique : bpp.boutique,
		producerproduct_id : bpp.producerproduct_id
	};
	var currentBoutiqueRestock = BoutiqueRestocks.findOne({ status : { $lt : 2 }});
	var item = 	_.extend(
					{
						quantity_ordered : quantity
					},
					_.pick(bpp, ['producer_id','producer_name','producerproduct_id','product_name','unit_display', 'tva'])
				);

	if(!currentBoutiqueRestock){

		var insertData = {
			boutique : bpp.boutique,
			entrepot : 'MOZ',
			products : 1,
			units_total : quantity,
			items : [item],
			createdAt : new Date()
		}

		BoutiqueRestocks.insert(insertData, callback);

	} else {

		var items = EJSON.clone(currentBoutiqueRestock.items);
		var index = items.map(function(item){ return item.producerproduct_id._str; }).indexOf(bpp.producerproduct_id._str);
		if(~index) items[index].quantity_ordered += quantity;
		else items.push(item);

		BoutiqueRestocks.update(currentBoutiqueRestock._id, {
			$set : {
				items : items
			},
			$inc : {
				units_total : quantity,
				products : !~index ? 1 : 0
			}
		}, callback);
	}
}

var insertEvent = function(bpp, data, callback){

	var insertData = _.pick(bpp, ['producerproduct_id','boutique']);

	if(data.type === 2) {

		var pastInventoryCursor = BoutiqueProducerProductsEvents.find({
			producerproduct_id : bpp.producerproduct_id,
			entrepot : bpp.entrepot,
			type : 2
		});

		if(!pastInventoryCursor.count()) {
			insertData.comment = "* inventaire initiale *";
		}
	}

	_.extend(insertData, {
		quantity : data.quantity,
		comment : data.comment,
		type : data.type
	});

	if(data.globalorder_id){
		insertData.globalorder_id = data.globalorder_id;
		insertData.locale = data.locale;
	}

	BoutiqueProducerProductsEvents.insert(insertData, callback);
}
