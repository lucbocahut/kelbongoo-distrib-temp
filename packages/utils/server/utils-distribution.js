UtilsDistribution = {
	'distribution.markOrdersPickedUp' : function(locale, globalorder_id, couloir, orders, callback){

		var modifier = { $set : {}};
		if(couloir === 'frais') modifier.$set.fraisPickedUpAt = new Date();
		if(couloir === 'sec') modifier.$set.secPickedUpAt = new Date();

		Orders.update({ _id : { $in : orders }}, modifier, { multi : true }, callback);
	},
	'distribution.markOrdersDelivered' : function(locale, globalorder_id, couloir, orders, callback){

		function getModifier(couloir, type){
			var modifier = { $set : {}};
			var key = couloir;
			if(type === 'delivered') {
				key+='DeliveredAt';
				modifier.$set[key] = new Date();
			} else {
				key+= 'Waiting';
				modifier.$set[key] = true;
			}
			return modifier;
		}

		var _markDelivered = Utils.promisifyFunction(function(deliveredOrders, couloir, callback){
			var modifier = getModifier(couloir, 'delivered');
			Orders.update({ _id : { $in : deliveredOrders }}, modifier, { multi : true }, callback);
		});
		var _markWaiting = Utils.promisifyFunction(function(waitingOrders, couloir, callback){
			var modifier = getModifier(couloir, 'waiting');
			Orders.update({ _id : { $in : waitingOrders }}, modifier, { multi : true }, callback);
		});

		var deliveredOrders = [];
		var waitingOrders = [];

		orders.forEach(function(o){

			var place = LocalePlaces.find({ locale : locale, order_id : o }).count();
			if(place){
				deliveredOrders.push(o);
			} else {
				waitingOrders.push(o);
			}
		});
		console.log('deliveredOrders', deliveredOrders);
		console.log('waitingOrders', waitingOrders);

		var promise = _markDelivered(deliveredOrders, couloir)
						.then(function(){
							return _markWaiting(waitingOrders, couloir);
						});

		Utils.waitFor(promise, callback);
	},
	'distribution.markTablePlaceFree' : function(localeplace, globalorder_id, callback){
		console.log('57', arguments);
		
		// if order's bacs were 'waiting', remove the waiting labels...
		var _updateOrder = Utils.promisifyFunction(function(order_id, callback){

			var modifier = { 
				$set : { tableDepartedAt : new Date() },
				$unset : {
					secWaiting : '',
					fraisWaiting : ''
				}
			};

			Orders.update(order_id, modifier, callback);
		});
		var _resetTablePlace = Utils.promisifyFunction(UtilsDistribution['distribution.resetTablePlace']);

		var promise = _updateOrder(localeplace.order_id)
						.then(function(){
							return _resetTablePlace(localeplace, globalorder_id);
						});

		Utils.waitFor(promise, callback);
	},
	'distribution.resetTablePlace' : function(localeplace, globalorder_id, callback){

		console.log('args 84', arguments);

		var queue = [];

		var _markNextOrderTabled = Utils.promisifyFunction(function(newOrderId, callback){
			console.log('86', arguments);

			Orders.update(newOrderId, { $set : { tabledAt : new Date() }}, callback);
		});

		var _updateLocalePlace = Utils.promisifyFunction(function(params, callback){
			console.log('92', arguments);

			var modifier = { $unset : { next : '' }};

			if(params.newOrderId){
				modifier.$set = { order_id : params.newOrderId };
			} else {
				modifier.$unset = { order_id : '' };
			}

			LocalePlaces.update(params.localeplace_id, modifier, function(err){
				console.log('103', arguments);
				callback(err);
			});
		});

		if(localeplace.next){

			console.log('110');

			queue.push(_updateLocalePlace({
				newOrderId : localeplace.next,
				localeplace_id : localeplace._id
			}));

		} else {

			var currentOrderIds = LocalePlaces.find({
				locale : localeplace.locale,
				order_id : { $exists : true }
			}).map(function(lp){ return lp.order_id; });
			
			var ordersSelector = { 
				globalorder_id : globalorder_id, 
				locale : localeplace.locale,
				arrivedAt : { $exists : true },
				tabledAt : { $exists : false },
				_id : { $nin : currentOrderIds }
			};
			console.log('ordersSelector', ordersSelector);
			var ordersOptions = { sort : { arrivedAt : 1 }, limit : 1, fields : { _id : 1 }};
			var modifier = {};
			var newOrderId;

			var newOrderCursor = Orders.find(ordersSelector, ordersOptions);
			console.log('newOrderCursor', newOrderCursor.fetch());

			if(newOrderCursor.count()){

				newOrderId = newOrderCursor.fetch()[0]._id;
				queue.push(_markNextOrderTabled(newOrderId));
				queue.push(_updateLocalePlace({
					localeplace_id : localeplace._id,
					newOrderId : newOrderId
				}));

			} else {
				
				queue.push(_updateLocalePlace({
					localeplace_id : localeplace._id
				}));
			}
			
		}

		Utils.waitForAll(queue, callback);
	}
};