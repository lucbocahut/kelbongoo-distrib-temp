UtilsGolpps = {

	// if percentage === 100, this MUST REMOVE FROM THE DISPATCH
	// this makes a reduction on ALL orders who ordered a certain golpp
	'golpps.reduceAllOrders' : function(locale, globalorder, producerproduct_id, percentage, callback){

		var ordersCursor = Orders.find({
			globalorder_id : globalorder._id,
			locale : locale,
			items : { $elemMatch : { producerproduct_id : producerproduct_id }}
		});
		var _reduceAllUnits = Utils.promisifyFunction(reduceAllUnits);
		var _reduceOrderItem = Utils.promisifyFunction(reduceOrderItem);
		var queue = [];

		if(globalorder.status < 2 || globalorder.status > 6) return callback(new Error('invalid-params'));

		ordersCursor.forEach(function(order){

			var index = order.items.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);
			var quantity = order.items[index].quantity;

			queue.push(
				_reduceOrderItem(order, index, quantity, percentage)
			);
		});

		queue.push(
			_reduceAllUnits(locale, globalorder, producerproduct_id, percentage)
		);

		Utils.waitForAll(queue, callback);
	},

	// update quantity to add to rab for the golpp

	'golpps.updateQuantityAdded' : function(golpp, quantity, callback){

		var bppSelector = {
			producerproduct_id : golpp.producerproduct_id,
			boutique : golpp.locale
		};
		var bppData = _.pick(golpp, ['producer_name','product_name','unit_display','producerproduct_id','producer_id']);
		bppData.quantity_available = quantity;

		var bppModifier = {
			$set : bppData
		};
		var golppModifier = {
			$set : {
				locale_added : quantity
			}
		};
		var _upsertBpp = Utils.promisifyFunction(function(selector, modifier, callback){
			BoutiqueProducerProducts.upsert(selector, modifier, callback);
		});
		var _updateGolpp = Utils.promisifyFunction(function(golpp_id, modifier, callback){
			GlobalOrderLocaleProducerProducts.update(golpp_id, modifier, callback);
		});

		var promise = upsertBpp(bppSelector, bppModifier)
						.then(function(){
							return _updateGolpp(golpp._id, golppModifier);
						});

		Utils.waitFor(promise, callback);

	},
	'golpps.getUnfinished' : function(base){

		var selector = _.extend({
			$or : [
				{ dispatch_completed : false }, 
				{ units_completed : false }
			]
		}, base);

		var options = { 
			fields : {
				producer_name : 1,
				product_name : 1,
				producerproduct_id : 1,
				producer_id : 1,
				globalorder_id : 1,
				locale : 1,
				unit_display : 1,
				units_done : 1,
				dispatch_done : 1,
				quantity_expected : 1,
				quantity_sold : 1,
				couloir : 1,
				dispatch_completed : 1,
				units_completed : 1,
				bocaux_validated : 1
			},
			sort : { producer_name : 1, product_name : 1 }
		};

		var output = GlobalOrderLocaleProducerProducts.find(selector, options).fetch();
		console.log('missing', output);
		return output;
	},
	'golpps.getExtra' : function(base){

		var selector = _.extend({ 
			quantity_received : { $exists : true }
		}, base);

		var options = { 
			fields : {
				producer_name : 1,
				product_name : 1,
				producerproduct_id : 1,
				producer_id : 1,
				globalorder_id : 1,
				locale : 1,
				unit_display : 1,
				units_done : 1,
				dispatch_done : 1,
				quantity_expected : 1,
				quantity_received : 1,
				quantity_sold : 1,
				couloir : 1,
				dispatch_completed : 1,
				units_completed : 1,
				bocaux_validated : 1
			},
			sort : { producer_name : 1, product_name : 1 }
		};

		var reducer = function(memo, item){
			if(item.quantity_received > item.quantity_sold) memo.push(item);
			return memo;
		};

		return GlobalOrderLocaleProducerProducts.find(selector, options).fetch().reduce(reducer, []);
	},
	'golpps.handleUnfinished' : function(warnings, callback){
		console.log('golpps.handleUnfinished 138', warnings);
		var queue = [];
		var _handleUnfinishedDispatch = Utils.promisifyFunction(handleUnfinishedDispatch);
		var _handleUnfinishedNonDispatchUnits = Utils.promisifyFunction(handleUnfinishedNonDispatchUnits);


		warnings.unfinished.forEach(function(golpp){

			if(golpp.dispatch_completed === false){

				queue.push(_handleUnfinishedDispatch(golpp));

			} else if (golpp.units_completed === false){

				queue.push(_handleUnfinishedNonDispatchUnits(golpp));

			}
			
		});

		Utils.waitForAll(queue, callback, warnings);

	},
	'golpps.handleExtra' : function(locale, originalExtra, clientExtra, callback){

		var queue = [];
		var extra = mergeExtras(originalExtra, clientExtra);
		console.log('mergedExtra', extra);
		var _addExtraToLocaleRab = Utils.promisifyFunction(addExtraToLocaleRab);

		extra.forEach(function(item){
			console.log('item', item);

			queue.push(_addExtraToLocaleRab(locale, item));
		});

		Utils.waitForAll(queue, callback);
	},

	/*
	*
	* *** DISPATCH METHODS ***
	*
	*/

	'golpps.dispatchOne' : function(golpp, number, callback){

		var queue = [];

		var waiting = DispatchQueue.findOne({ golpp_id : golpp._id, number : number });
		var done = DispatchDone.findOne({ golpp_id : golpp._id, number : number });

		if(waiting.quantity === 1) queue.push(_removeDispatchQueue(waiting));
		else queue.push(_downDispatchQueue(waiting, 1));

		if(done) queue.push(_upDispatchDone(done, 1));
		else queue.push(_insertDispatchDone(golpp, number, 1));

		queue.push(_upGolppDispatchDone(golpp, 1));

		Utils.waitForAll(queue, callback);

	},
	'golpps.undispatchBac' : function(golpp, number, quantity, callback){

		var queue = [];

		var waiting = DispatchQueue.findOne({ golpp_id : golpp._id, number : number });
		var done = DispatchDone.findOne({ golpp_id : golpp._id, number : number });

		if(done.quantity === 1) queue.push(_removeDispatchDone(done));
		else queue.push(_downDispatchDone(done, 1));

		if(waiting) queue.push(_upDispatchQueue(done, 1));
		else queue.push(_insertDispatchQueue(golpp, number, 1));

		queue.push(_downGolppDispatchDone(golpp, 1));

		Utils.waitForAll(queue, callback);

	},
	'golpps.undispatchAll' : function(golpp, callback){

		var queue = [];

		DispatchDone.find({ golpp_id : golpp._id }).forEach(function(done){

			var waiting = DispatchQueue.findOne({ golpp_id : golpp._id, number : done.number });

			if(waiting){
				queue.push(_upDispatchQueue(golpp, done.quantity));
			} else {
				queue.push(_insertDispatchQueue(golpp, done.number, done.quantity));
			}

			queue.push(_removeDispatchDone(done));
		});

		queue.push(_downGolppDispatchDone(golpp, golpp.dispatch_done));

		Utils.waitForAll(queue, callback);

	},
	'golpps.unitsDoneUpOne' : function(golpp, callback){

		var modifier = {
			$inc : {
				units_done : 1,
				quantity_received : 1
			}
		};
		if(golpp.units_done + 1 >= golpp.quantity_expected) modifier.$set = { units_completed : true };
		
		GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
	},
	'golpps.unitsDoneDownOne' : function(golpp, callback){

		var modifier = {
			$inc : {
				units_done : -1,
				quantity_received : -1
			}
		};

		if(golpp.units_done - 1 < golpp.quantity_expected) modifier.$set = { units_completed : false };
		
		GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
	},
	'golpps.unitsDoneAll' : function(golpp, callback){

		var modifier = {
			$set : {
				units_done : golpp.quantity_expected,
				quantity_received : golpp.quantity_expected,
				units_completed : true
			}
		};
		
		GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
	},

	// this is for non-dispatch/non-unit items, usually non-dispatch bocaux...
	
	'golpps.updateQuantityReceived' : function(golpp, quantity, callback){

		var modifier = {
			$set : {
				quantity_received : quantity
			}
		};
		if(typeof golpp.direct_validated !== 'undefined'){
			modifier.$set.direct_validated = true;
		}
		
		GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
	},
	'golpps.updateRealWeight' : function(order, golpp, dispatchItem, realWeight, callback){

		var _markDispatchItemAdjusted = Utils.promisifyFunction(function(dispatchItem, callback){
			DispatchQueue.update(dispatchItem._id, { $set : { adjusted : true }}, callback);
		});
		var _unmarkDispatchItemAdjusted = Utils.promisifyFunction(function(dispatchItem, callback){
			DispatchQueue.update(dispatchItem._id, { $set : { adjusted : false }}, callback);
		});
		var _reduceOrderItem = Utils.promisifyFunction(function(order, golpp, realWeight, callback){

			Utils['order.reduceItem'](order, golpp.producerproduct_id, 1, (realWeight/golpp.unit_weight)*100, callback);
		});
		var _unreduceOrderItem = Utils.promisifyFunction(function(order, golpp, realWeight, callback){

			var reducedIndex = order.reduced.map(function(r){ return r.producerproduct_id._str; }).indexOf(golpp.producerproduct_id._str);

			Utils['order.reduceItem'](order, golpp.producerproduct_id, -1, order.reduced[reducedIndex].percentage, callback);
		});

		var queue = [];

		if(realWeight !== golpp.unit_weight){
			queue.push(_markDispatchItemAdjusted(dispatchItem));
			queue.push(_reduceOrderItem(order, golpp, realWeight));

		} else {
			queue.push(_unmarkDispatchItemAdjusted(dispatchItem));
			queue.push(_unreduceOrderItem(order, golpp, realWeight));
		}

		Utils.waitForAll(queue, callback);
	}
}

// HELPERS

var mergeExtras = function(original, client){

	var output = [];
	var index;

	for(var i = 0;i < original.length; i++){
		index = client.map(function(e){ return e.producerproduct_id._str; }).indexOf(original[i].producerproduct_id._str);
		output.push(
			_.extend({ 
				boutiqueQuantity : ~index ? client[index].quantity : original[i].quantity_received - original[i].quantity_sold
			}, original[i])
		);
	}
	return output;
}

var removeDispatchQuantityFromOrder = function(order, gopp, quantityToRemove, callback){

	var itemsIndex = order.items.map(function(i){ return i.producerproduct_id._str; }).indexOf(gopp.producerproduct_id._str);
	var initialQuantity = order.items[itemsIndex].quantity;

	Utils['orders.updateItemQuantity'](order, itemsIndex, gopp, initialQuantity - quantityToRemove, callback);
};

var pushRemainingToMissingAndSetCompleted = function(golpp, remaining, callback){

	var modifier = {
		$set : { 
			dispatch_missing : remaining, 
			dispatch_completed : true 
		}
	};
	if(typeof golpp.units_done !== 'undefined' && !golpp.units_completed){
		modifier.$set.units_completed = true;
	}
	GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
}

var handleUnfinishedDispatch = function(golpp, callback){

	var _pushRemainingToMissingAndSetCompleted = Utils.promisifyFunction(pushRemainingToMissingAndSetCompleted);
	var _removeGolppDispatchQueue = Utils.promisifyFunction(function(golpp, callback){
		DispatchQueue.remove({ golpp_id : golpp._id }, callback);
	});
	var _removeDispatchQuantityFromOrder = Utils.promisifyFunction(removeDispatchQuantityFromOrder);

	var remaining = DispatchQueue.find({ golpp_id : golpp._id }, { fields : { _id : 0, number : 1, quantity : 1 }}).fetch();
	var gopp = GlobalOrderProducerProducts.findOne({ globalorder_id : golpp.globalorder_id, producerproduct_id : golpp.producerproduct_id });
	var queue = [];

	// push all remaining DispatchQueue items for this golpp to 'dispatch_missing'
	queue.push(_pushRemainingToMissingAndSetCompleted(golpp, remaining));
	queue.push(_removeGolppDispatchQueue(golpp));
	
	remaining.forEach(function(item){

		// identify the order via dispatchqueue bac number
		var selector = {
			globalorder_id : golpp.globalorder_id,
			locale : golpp.locale
		};
		if(golpp.couloir === 0) selector['bacs.frais'] = item.number;
		if(golpp.couloir === 1) selector['bacs.sec'] = item.number;

		var order = Orders.findOne(selector);
		if(!order) throw new Error('order-not-found');

		queue.push(_removeDispatchQuantityFromOrder(order, gopp, item.quantity));
	});

	Utils.waitForAll(queue, callback);
}

var handleUnfinishedNonDispatchUnits = function(golpp, callback){
	// set units_completed = true
	// choose the necessary number of orders and remove from their non-dispatch...
	console.log('handleUnfinishedNonDispatchUnits not yet implemented');
	callback();
}

var addExtraToLocaleRab = function(locale, item, callback){

	var bpp = BoutiqueProducerProducts.findOne({ 
		producerproduct_id : item.producerproduct_id,
		boutique : locale
	});

	if(bpp){

		BoutiqueProducerProducts.update(bpp._id, {
			$addToSet : { lots : 'GLOBALE_'+item.globalorder_id._str },
			$inc : { quantity_available : item.boutiqueQuantity }
		}, callback);

	} else {

		var insertData = _.extend(
			{ 
				quantity_available : item.boutiqueQuantity, 
				boutique : locale,
				lots : ['GLOBALE_'+item.globalorder_id._str]
			},
			_.pick(item, 'producer_id','producerproduct_id','product_name','producer_name','unit_display')
		);
		BoutiqueProducerProducts.insert(insertData, callback);
	}
}

var reduceAllUnits = function(locale, globalorder, producerproduct_id, percentage, callback){

	var _removeAllGolppDispatch = Utils.promisifyFunction(function(golpp_id, callback){
		DispatchQueue.remove({ golpp_id : golpp_id }, callback);
	});
	var golpp = GlobalOrderLocaleProducerProducts.findOne({
		locale : locale,
		globalorder_id : globalorder._id,
		producerproduct_id : producerproduct_id
	});
	var dispatchStarted = DispatchDone.find({ golpp_id : golpp._id }).count() > 0 || golpp.dispatch_missing.length;
	var queue = [];

	if(golpp.reduced.length) {
		return callback(new Error('reduced-items-already-exist'));
	}
	if(globalorder.status > 4 && percentage === 100 && dispatchStarted) {
		return callback(new Error('dispatch-already-started'));
	}
	
	if(globalorder.status > 4 && percentage === 100){

		queue.push(_removeAllGolppDispatch(golpp._id));
	}

	/*
	*	*** WHAT IS GOING ON HERE ??? ***
	*/
	queue.push(new Promise(function(resolve, reject){

		var modifier = {
			$set : {
				reduced : [{
					quantity : golpp.quantity_sold,
					percentage : percentage
				}]
			}
		};

		if(globalorder.status > 4 && percentage === 100){

			modifier.$set.dispatch_missing = DispatchQueue.find({

				golpp_id : golpp._id 

			}).map(function(d){ 

				return _.extend({

					user : Meteor.userId(),
					date : new Date()
				}, d);
			});
		}

		GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, function(err){
			if(err) return reject(err);
			resolve();
		});

	}));

	Utils.waitForAll(queue, callback);

}

// DISPATCH HELPERS

function promisifyFunction(fn){

	if(typeof fn !== 'function') throw new Error('invalid-argument');
	
	return function(){
		var self = this;
		var args = Array.from(arguments);

		return new Promise(function(resolve, reject){
			args.push(function(err, output){
				if(err) return reject(err);
				resolve(output);
			});
			fn.apply(self, args);
		});
	}
}

var _removeDispatchQueue = promisifyFunction(function(waiting, callback){
	DispatchQueue.remove(waiting._id, callback);
});
var _downDispatchQueue = promisifyFunction(function(waiting, quantity, callback){
	DispatchQueue.update(waiting._id, { $inc : { quantity : -quantity }}, callback);
});
var _upDispatchDone = promisifyFunction(function(done, quantity, callback){
	DispatchDone.update(done._id, { $inc : { quantity : quantity }}, callback);
});
var _insertDispatchDone = promisifyFunction(function(golpp, number, quantity, callback){

	var insertData = {
		golpp_id : golpp._id,
		number : number,
		quantity : quantity,
		user : Meteor.userId(),
		date : new Date()
	};

	DispatchDone.insert(insertData, callback);
});
var _upGolppDispatchDone = promisifyFunction(function(golpp, quantity, callback){

	var modifier = {
		$inc : { dispatch_done : 1 }
	};

	if(golpp.dispatch_done + 1 >= golpp.quantity_sold){
		modifier.$set = { dispatch_completed : true };
	}

	if(typeof golpp.units_done === 'undefined'){
		modifier.$inc.quantity_received = 1;
	}

	GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
});
var _removeDispatchDone = promisifyFunction(function(done, callback){
	DispatchDone.remove(done._id, callback);
});
var _upDispatchQueue = promisifyFunction(function(waiting, quantity, callback){
	DispatchQueue.update(waiting._id, { $inc : { quantity : quantity }}, callback);
});
var _downDispatchDone = promisifyFunction(function(done, quantity, callback){
	DispatchDone.update(done._id, { $inc : { quantity : -quantity }}, callback);
});
var _insertDispatchQueue = promisifyFunction(function(golpp, number, quantity, callback){

	var insertData = {
		golpp_id : golpp._id,
		number : number,
		quantity : quantity
	};

	DispatchQueue.insert(insertData, callback);
});
var _downGolppDispatchDone = promisifyFunction(function(golpp, quantity, callback){

	var modifier = {
		$inc : { dispatch_done : -quantity }
	};

	if(golpp.dispatch_done - 1 < golpp.quantity_sold){
		modifier.$set = { dispatch_completed : false };
	}
	if(typeof golpp.units_done === 'undefined'){
		modifier.$inc.quantity_received = -quantity;
	}

	GlobalOrderLocaleProducerProducts.update(golpp._id, modifier, callback);
});
