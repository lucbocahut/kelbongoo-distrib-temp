var wkhtmltopdf = Npm.require('wkhtmltopdf');
var googleOauthJwt = Npm.require('google-oauth-jwt');

UtilsOrders = {

	/*
	*	** NOTE ***
	*	'quantity' here refers to the quantity we WANT the item to have now
	*/
	'orders.print' : function(order, callback){

		return order.print();
	},

	'orders.createConsumerCreditEvent' : function(order, params, callback){

		return order.createConsumerCreditEvent(params);
	},

	'orders.updateItemQuantity' : function(order, itemsIndex, gopp, quantity, callback){

		console.log('args', arguments);

		var queue = [];
		var items = order.items.slice(0);

		var _pushToDispatchErrors = Utils.promisifyFunction(pushToDispatchErrors);
		var _updateOrderItemQuantity = Utils.promisifyFunction(updateOrderItemQuantity);

		var diff = ~itemsIndex ? quantity - items[itemsIndex].quantity : quantity;

		// STEP 1 : update the order items list...

		queue.push(_updateOrderItemQuantity(order, itemsIndex, gopp, diff));

		if (diff < 0) {

			/*
			* 	*** during distribution ***
			*	we call this method when all/part of an item is *missing* only, i.e. cannot increase
			* 	in this case, it is a dispatch error
			*/

			queue.push(_pushToDispatchErrors(order._id, gopp, order.locale, -diff));
		}

		Utils.waitForAll(queue, callback);

	},
	'orders.reduceItem' : function(order, producerproduct_id, quantity, percentage, callback){

		var items = order.items.slice(0);
		var reduced = order.reduced.slice(0);
		var itemsIndex = items.map(getPPIdString).indexOf(producerproduct_id._str);
		var reducedIndex = reduced.map(getPPIdString).indexOf(producerproduct_id._str);
		var unit_price;

		if(quantity < 0){

			// it's an un-reduce

			var reducedItem = reduced.splice(reducedIndex, 1)[0];

			if(~itemsIndex){

				items[itemsIndex].quantity -= reducedItem.quantity;

			} else {

				reducedItem = _.omit(reducedItem, 'user_id','percentage');
				items.push(reducedItem);
			}

			unit_price = reducedItem.kelbongoo_price * ((100 + reducedItem.tva)/100);

		} else {

			var item = items[itemsIndex];

			if(item.quantity === quantity){

				items.splice(itemsIndex, 1);

			} else {

				items[itemsIndex].quantity -= quantity;
			}

			if(~reducedIndex){

				reduced[reducedIndex].quantity += quantity;
			} else {

				_.extend(item, { quantity : quantity, percentage : percentage, userId : Meteor.userId() });
				reduced.push(item);
			}

			unit_price = item.kelbongoo_price * ((100 + item.tva)/100);
		}


		var amount_total = order.amount_total - (unit_price * quantity * (percentage/100));
		var units_total = percentage === 100 ? units_total - quantity : units_total;
		var modifier = {
			$set : {
				amount_total : amount_total,
				units_total : units_total,
				reduced : reduced,
				items : items
			}
		};

		Orders.update(order._id, modifier, callback);
	},

	/*
		things to check with payment :
		- payment check
		- payment cash
		boutique
		- add new OK
		- add/remove quantity OK
		- remove last cancels boutiqueorder OK
		- cannot go past quantity_available (or 4 max) OK
		- totals update correctly (also for payment !)
		- reduce/unreduce OK
		normal
		- add/remove quantity OK
		- remove last quantity removes from items OK
		- totals update correctly (also for payment !)
		- remove/put back OK
		- reduce/unreduce OK
	*/

	'orders.pay' : function(order_id, params, callback){

		var queue = [];
		var _payOrder = Utils.promisifyFunction(payOrder);
		var _payBoutiqueOrder = Utils.promisifyFunction(Utils['boutiqueorders.pay']);

		queue.push(_payOrder(order_id, params.method));

		if(params.boutiqueorder_id){
			queue.push(_payBoutiqueOrder(params.boutiqueorder_id, params.method));
		}

		Utils.waitForAll(queue, callback);

	},
	// if quantity is negative, we are giving back a deposit item
	'orders.updateCaution' : function(order, deposit, quantity, callback){

		var cautions = order.cautions;
		var value = deposit.unit_value * quantity;
		var modifier = {
			$set : {
				cautions : cautions + value,
				amount_total : order.amount_total + value
			}
		};

		Orders.update(order._id, modifier, callback);
	},
	'orders.setArrived' : function(order, callback){

		var _updateOrder = Utils.promisifyFunction(function(order_id, callback){
			Orders.update(order_id, { $set : { status : 3, arrivedAt : new Date() }}, callback);
		});
		var _tryToTableOrder = Utils.promisifyFunction(tryToTableOrder);

		var promise = _updateOrder(order._id)
						.then(function(){
							return _tryToTableOrder(order);
						});

		Utils.waitFor(promise, callback);

	},
	'orders.markSecDelivered' : function(order, callback){

		Orders.update(order._id, {
			$set : {
				secDeliveredAt : new Date()
			},
			$unset : {
				secWaiting : ''
			}
		}, callback);

	},
	'orders.markFraisDelivered' : function(order, callback){

		Orders.update(order._id, {
			$set : {
				fraisDeliveredAt : new Date()
			},
			$unset : {
				fraisWaiting : ''
			}
		}, callback);

	},
	'orders.putOnHold' : function(orders, date, callback){

		var ids = orders.map(function(order){ return order._id; });
		var modifier = {
			$set : {
				holdUntil : date,
				status : 5
			}
		};

		Orders.update({ _id : { $in : ids }}, modifier, { multi : true }, callback);
	},
	'orders.markUndoTableDepartedError' : function(order, localeplace, callback){

		// remove tableDepartedAt
		// put the user back at his table, removing the current person and placing them in 'next'...

		var _removeTableDeparted = Utils.promisifyFunction(function(order, callback){
			Orders.update(order._id, { $unset : { tableDepartedAt : '' }}, callback);
		});
		var _shiftCurrentPlaceOrders = Utils.promisifyFunction(function(order, localeplace, callback){
			var modifier = { $set : { order_id : order._id }};
			if(localeplace.order_id){
				modifier.$set.next = localeplace.order_id;
			}
			LocalePlaces.update(localeplace._id, modifier, callback);
		});

		var promise = _removeTableDeparted(order)
						.then(function(){
							return _shiftCurrentPlaceOrders(order, localeplace);
						});

		Utils.waitFor(promise, callback);

	},
	'orders.undoOrderArrival' : function(order, callback){

		// remove user from table (if there is one)
		// undo tabledAt (if exists) and arrivedAt
		// mark the order 'error_return_bacs'

		var _removeOrderFromTable = Utils.promisifyFunction(function(order, callback){

			var lp = LocalePlaces.findOne({ order_id : order._id });
			console.log('lp', lp);
			if(lp){
				Utils['distribution.resetTablePlace'](lp, order.globalorder_id, callback);
			} else {
				callback();
			}

		});

		var _setOrderBacsError = Utils.promisifyFunction(function(order, callback){
			console.log('227', arguments);
			var modifier = {
				$unset : { arrivedAt : '' },
				$set : { status : 2 }
			};
			if(order.tabledAt) modifier.$unset.tabledAt = '';
			if(order.secPickedUpAt) modifier.$unset.secPickedUpAt = '';
			if(order.fraisPickedUpAt) modifier.$unset.fraisPickedUpAt = '';
			if(order.secDeliveredAt) modifier.$unset.secDeliveredAt = '';
			if(order.fraisDeliveredAt) modifier.$unset.fraisDeliveredAt = '';
			if(order.secWaiting) modifier.$unset.secWaiting = '';
			if(order.fraisWaiting) modifier.$unset.fraisWaiting = '';

			if(order.secPickedUpAt || order.fraisPickedUpAt || order.secDeliveredAt || order.fraisDeliveredAt){
				modifier.$set.error_return_bacs = true;
			}

			Orders.update(order._id, modifier, callback);
		});

		var promise = _removeOrderFromTable(order)
						.then(function(){
							return _setOrderBacsError(order);
						});

		Utils.waitForAll(promise, callback);
	}
}

// **********************
// *** HELPER METHODS ***
// **********************

var pushToDispatchErrors = function(order_id, gopp, locale, quantity, callback){

	var selector = _.extend({ locale : locale }, _.pick(gopp, 'globalorder_id','producerproduct_id'));
	var golpp = GlobalOrderLocaleProducerProducts.findOne(selector, { fields : { couloir : 1 }});
	if(!golpp) return callback();

	var errors = (golpp.dispatch_errors && golpp.dispatch_errors.slice(0)) || [];
	var eInd = errors.map(function(e){ return e.order_id._str; }).indexOf(order_id._str);

	if(~eInd) {
		errors[eInd].quantity += quantity;
	} else {
		errors.push({
			order_id : order_id,
			quantity : quantity
		});
	}

	GlobalOrderLocaleProducerProducts.update(selector, { $set : { dispatch_errors : errors }}, callback);
}

var clearDispatchErrors = function(order_id, gopp, locale, quantity, callback){

	var selector = _.extend({ locale : locale }, _.pick(gopp, 'globalorder_id','producerproduct_id'));
	var golpp = GlobalOrderLocaleProducerProducts.findOne(selector, { fields : { dispatch_errors : 1 }});
	if(!golpp) return callback();

	var errors = (golpp.dispatch_errors && golpp.dispatch_errors.slice(0)) || [];
	var eInd = errors.map(function(e){ return e.order_id._str; }).indexOf(order_id._str);

	if(!~eInd) return callback();

	if(errors[eInd].quantity > quantity){
		errors[eInd].quantity -= quantity;
	} else {
		errors.splice(eInd, 1);
	}

	GlobalOrderLocaleProducerProducts.update(selector, { $set : { dispatch_errors : errors }}, callback);
}

// *** ADD/REMOVE ITEM HELPERS ***

var updateOrderItemQuantity = function(order, itemsIndex, gopp, diff, callback){

	var items = order.items.slice(0);
	var removed = order.removed.slice(0);

	var fields = ['producer_name','product_name','producerproduct_id','kelbongoo_price','tva','unit_display','type'];

	var item = ~itemsIndex ? items[itemsIndex] : _.pick(gopp, fields);

	var removedIndex = removed.map(getPPIdString).indexOf(item.producerproduct_id._str);
	var removedItem = ~removedIndex && removed[removedIndex];

	var amount_total = order.amount_total + (diff * (((100 + item.tva)/100) * item.kelbongoo_price));
	var units_total = order.units_total + diff;

	var _updateOrder = Utils.promisifyFunction(function(order_id, modifier, callback){
		Orders.update(order_id, modifier, callback);
	});
	var _clearDispatchErrors = Utils.promisifyFunction(clearDispatchErrors);
	var queue = [];


	if(!~itemsIndex && diff > 0){

		// if there is no itemIndex, i cannot remove anything, so diff > 0
		// we are either adding a new item or *unremoving* when all had been removed from items...
		// this when order.status < 2, or when we are 'unremoving'

		items.push(_.extend({ quantity : diff }, item));

		if(~removedIndex){

			if(removedItem.quantity === diff){
				removed.splice(removedIndex,1);
			} else {
				removed[removedIndex].quantity -= diff;
			}

			queue.push(_clearDispatchErrors(order._id, gopp, order.locale, diff));
		}

	} else {

		// implicit : ~itemsIndex

		if(diff < 0){

			// we are removing quantity...

			if(item.quantity === -diff){
				// if we are removing all of the quantity for the item, splice it
				items.splice(itemsIndex, 1);
			} else {
				// reduce the item quantity
				items[itemsIndex].quantity += diff;
			}

			if(order.status > 0){

				if(~removedIndex){
					// increase the removed quantity
					removed[removedIndex].quantity -= diff;

				} else {
					// add the item to the removed array
					removed.push(_.extend({ quantity : -diff }, item));
				}
			}

		} else {

			// we are adding quantity to an already existing item

			items[itemsIndex].quantity += diff;

			if(~removedIndex){

				if(removedItem.quantity === diff){
					removed.splice(removedIndex,1);
				} else {
					removed[removedIndex].quantity -= diff;
				}

				queue.push(_clearDispatchErrors(order._id, gopp, order.locale, diff));

			}

			// what is this ??? we are adding quantity... why would we push to removed ?
			// else {

			// 	removed.push(_.extend({ quantity : -diff }, item));
			// }
		}
	}

	var modifier = {
		$set : {
			items : items,
			amount_total : amount_total,
			units_total : units_total,
			removed : removed
		}
	};
	console.log('475', modifier);
	queue.push(_updateOrder(order._id, modifier));

	Utils.waitForAll(queue, callback);
}

// *** PAY ORDER HELPERS ***

var payOrder = function(order_id, method, callback){

	var modifier = {
		$set : {
			payment_data : {
				method : method,
				userId : Meteor.userId()
			},
			paidAt : new Date(),
			status : 4
		}
	};

	Orders.update(order_id, modifier, callback);
}

// *** MISC HELPERS ***

var pushItemsToRab = function(order, callback){

	var queue = [];

	order.items.forEach(function(item){

		var bpp = BoutiqueProducerProducts.findOne({
			locale : order.locale,
			producerproduct_id : item.producerproduct_id
		});
		var _createBpp = Utils.promisifyFunction(Utils['bpps.create']);
		var _updateBpp = Utils.promisifyFunction(Utils['bpps.updateQuantityAvailable']);
		var queue = [];

		if(!bpp){

			var pp = ProducerProducts.findOne(item.producerproduct_id);
			queue.push(_createBpp(order.locale, pp, quantity));

		} else {

			queue.push(_updateBpp(bpp, bpp.quantity_available + item.quantity));
		}
	});

	Utils.waitForAll(queue, callback);
}

var tryToTableOrder = function(order, callback){

	var _updateLocalePlace = Utils.promisifyFunction(function(order_id, place_id, callback){
		LocalePlaces.update(place_id, { $set : { order_id : order_id }}, callback);
	});
	var _markOrderTabled = Utils.promisifyFunction(function(order_id, callback){
		Orders.update(order_id, { $set : { tabledAt : new Date() }}, callback);
	});
	var place = LocalePlaces.findOne({
		locale : order.locale,
		active : true,
		order_id : { $exists : false }
	}, {
		sort : { table : 1, place : 1 }
	});
	var queue = [];


	if(place){
		queue.push(_updateLocalePlace(order._id, place._id));
		queue.push(_markOrderTabled(order._id));
	} else {
		queue.push(Promise.resolve());
	}

	Utils.waitForAll(queue, callback);
}

var getPPIdString = function(item){
	return item.producerproduct_id._str;
}

var mapOrderItems = function(item){
    var unit_price = item.kelbongoo_price * ((100 + item.tva)/100);
    return {
        producer_name : item.producer_name,
        product_name : item.product_name,
        unit_price : Utils.formatMoney(unit_price),
        unit_display : item.unit_display,
        quantity : item.quantity,
        value : Utils.formatMoney((unit_price * item.quantity))
    };
}

var reduceBacs = function(output, item, index, array){
    return output += item + "" + (index === array.length-1 ? "" : "-");
};
var mapNonDispatch = function(item){
    return item.product_name + " (" + item.producer_name + ") " + (item.quantity > 1 ? " ( x " + item.quantity + " )" : "");
};

var getOrderPdfStream = function(template, order){

	var data = {
        firstname : order.firstname,
        lastname : order.lastname,
        email : order.email,
        telephone : order.telephone,
        amount_total : Utils.formatMoney(order.amount_total),
        units_total : order.units_total,
        distribution_date : Utils.formatDate(order.distribution_date, "DDD MMMM YYYY"),
        items : order.items.map(mapOrderItems),
        removed : order.removed.map(mapOrderItems),
        bacsFrais : order.bacs && order.bacs.frais ? order.bacs.frais.reduce(reduceBacs,"") : [],
        bacsSec : order.bacs && order.bacs.sec ? order.bacs.sec.reduce(reduceBacs,"") : [],
        nondispatch : order.nondispatch ? order.nondispatch.map(mapNonDispatch) : []
    };

    var context = _.template(template);

    return wkhtmltopdf(context(data), { orientation : "Landscape" });
}
