UtilsTasks = {
	'tasks.updateStatus' : function(task_id, isDone, callback){

		var modifier = {};
		if(isDone){
			modifier.$set = { completedAt : new Date() };
		} else {
			modifier.$unset = { completedAt : '' };
		}

		TaskInstances.update(task_id, modifier, callback);
	}
};