Utils = _.extend({
		promisifyFunction : promisifyFunction,
		waitFor : waitFor,
		waitForAll : waitForAll,
		buildRegExp : buildRegExp,
		methodResponseHandler : methodResponseHandler
	},
	UtilsShared,
	UtilsBoutiqueOrders,
	UtilsGolpps,
	UtilsOrders,
	UtilsTasks,
	UtilsDistribution,
	UtilsBoutiqueProducerProducts
);

function buildRegExp(searchText) {
    var words = searchText.trim().split(/[ \-\:]+/);
    var exps = _.map(words, function(word) {
        return "(?=.*" + word + ")";
    });
    var fullExp = exps.join('') + ".+";
    return new RegExp(fullExp, "i");
}

function promisifyFunction(fn){

	if(typeof fn !== 'function') throw new Error('invalid-argument');

	return function(){
		var self = this;
		var args = Array.from(arguments);

		return new Promise(function(resolve, reject){
			args.push(function(err, output){
				if(err) return reject(err);
				resolve(output);
			});
			fn.apply(self, args);
		});
	}
}

function waitFor(promise, callback, options){

	if(typeof options === 'function') return callback(new Error('invalid-argument'));

	promise
		.then(function(output){
			var arg = getOutputArgument(output, options);
			if(arg) return callback(null, arg);
			callback();
		}, function(err){
			callback(err);
		});
};

function waitForAll(queue, callback, options){

	if(typeof options === 'function') return callback(new Error('invalid-argument'));

	var promise = queue && queue.length ? Promise.all(queue) : Promise.resolve();

	promise.then(function(output){
			var arg = getOutputArgument(output, options, callback);
			if(arg) return callback(null, arg);
			callback();
		}, function(err){
			callback(err);
		});
};

function methodResponseHandler(fut, error, result){

	if(!fut || fut instanceof Error){

		error = fut;
		result = error;
		fut = null;

		if(error) throw new Meteor.Error('method-failed', error.message);

	} else if(error){

		if(error.errorType && error.errorType === 'Meteor.Error') {
			// we have already prepared a pretty error message, so keep it
			return fut.throw(error);
		} else {
			return fut.throw(new Meteor.Error('method-failed', error.message));
		}

	} else {
		fut.return(result);
	}
}

function getOutputArgument(output, options, callback){

	if(!output){
		return options;
	}
	if(!options){
		return output;
	}
	if(_.isObject(output) && !_.isArray(output) && _.isObject(options) && !_.isArray(options)){
		return _.extend({}, output, options);
	}
	return options;
}
