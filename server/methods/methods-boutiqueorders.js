var Future = Npm.require('fibers/future');

Meteor.methods({

	'boutiqueorders.upsertWithBpp' : function(boutique, producerproduct_id, params){
		console.log('6', arguments);

		check(boutique, String);
		check(producerproduct_id, Mongo.ObjectID);
		check(params, {
			quantity : Match.Optional(Number),
			order_id : Match.Optional(Mongo.ObjectID),
			boutiqueorder_id : Match.Optional(Mongo.ObjectID),
			email : Match.Optional(String),
			globalorder_id : Match.Optional(Mongo.ObjectID)
		});
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		if(!params.quantity) params.quantity = 1;

		var newParams = {
			boutique : boutique,
			quantity : params.quantity
		};
		if(params.email) newParams.email = params.email;
		if(params.globalorder_id) newParams.globalorder_id = params.globalorder_id;

		var bpp = BoutiqueProducerProducts.findOne({
			boutique : boutique,
			producerproduct_id : producerproduct_id,
			quantity_available : { $gte : params.quantity }
		});
		if(!bpp) throw new Meteor.Error('not-found');

		newParams.bpp = bpp;

		if(params.order_id){

			// *if* an order_id is given, check the order exists
			var order = Orders.findOne(params.order_id); 
			if(!order) throw new Meteor.Error('not-found');

			// *** no i think this should be ok - boutiqueorders are the context of distribution, not status 0-2...
			// check item is not already in order (items/removed/reduced)
			// var itemsInd = order.items.map(getPPIdString).indexOf(producerproduct_id._str);
			// var removedInd = order.removed.map(getPPIdString).indexOf(producerproduct_id._str);
			// var reducedInd = order.reduced.map(getPPIdString).indexOf(producerproduct_id._str);
			// if(~itemsInd || ~removedInd || ~reducedInd) throw new Meteor.Error('already-exists');

			newParams.order = order;
		}

		if(params.boutiqueorder_id){

			// *if* a boutiqueorder_id is given, check the boutiqueorder exists
			var boutiqueorder = BoutiqueOrders.findOne(params.boutiqueorder_id);
			if(!boutiqueorder) throw new Meteor.Error('not-found');

			// check item is not already in boutiqueorder (items/reduced)			
			var bItemsIndex = boutiqueorder.items.map(getPPIdString).indexOf(producerproduct_id._str);
			var bReducedIndex = boutiqueorder.reduced.map(getPPIdString).indexOf(producerproduct_id._str);
			if(~bItemsIndex || ~bReducedIndex) throw new Meteor.Error('already-exists');

			newParams.boutiqueorder = boutiqueorder;
		}

		var fut = new Future();

		Utils['boutiqueorders.upsertWithBpp'](newParams, Utils.methodResponseHandler.bind(null, fut));

		return fut.wait();
	},
	'boutiqueorders.updateItemQuantity' : function(boutique, producerproduct_id, boutiqueorder_id, quantity){

		check(boutique, String);
		check(boutiqueorder_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		check(quantity, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		if(quantity > 8 || quantity < 0) throw new Meteor.Error('invalid-params');

		var boutiqueorder = BoutiqueOrders.findOne({ boutique : boutique, _id : boutiqueorder_id });
		if(!boutiqueorder) throw new Meteor.Error('not-found');

		var itemsIndex = boutiqueorder.items.map(getPPIdString).indexOf(producerproduct_id._str);
		if(!~itemsIndex) throw new Meteor.Error('not-found');
		
		var bpp = BoutiqueProducerProducts.findOne({
			boutique : boutique,
			producerproduct_id : producerproduct_id,
			quantity_available : { $gt : (quantity - boutiqueorder.items[itemsIndex].quantity) }
		});
		if(!bpp) throw new Meteor.Error('not-available');

		var fut = new Future();

		Utils['boutiqueorders.updateItemQuantity'](boutiqueorder, bpp, itemsIndex, quantity, Utils.methodResponseHandler.bind(null, fut));

		return fut.wait();
	},
	'boutiqueorders.reduceItem' : function(boutique, producerproduct_id, boutiqueorder_id, quantity, percentage){

		check(boutique, String);
		check(boutiqueorder_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		check(quantity, Number);
		check(percentage, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var boutiqueorder = BoutiqueOrders.findOne({ boutique : boutique, _id : boutiqueorder_id });
		if(!boutiqueorder) throw new Meteor.Error('not-found');

		var itemsIndex = boutiqueorder.items.map(getPPIdString).indexOf(producerproduct_id._str);
		if(!~itemsIndex) throw new Meteor.Error('not-found');
		if(boutiqueorder.items[itemsIndex].quantity < quantity || quantity < 1) throw new Meteor.Error('invalid-params');

		var fut = new Future();

		Utils['boutiqueorders.reduceItem'](boutiqueorder, itemsIndex, quantity, percentage, Utils.methodResponseHandler.bind(null, fut));

		return fut.wait();
	},
	'boutiqueorders.unreduceItem' : function(boutique, producerproduct_id, boutiqueorder_id){

		check(boutique, String);
		check(boutiqueorder_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var boutiqueorder = BoutiqueOrders.findOne({ boutique : boutique, _id : boutiqueorder_id });
		if(!boutiqueorder) throw new Meteor.Error('not-found');

		var reducedIndex = boutiqueorder.reduced.map(getPPIdString).indexOf(producerproduct_id._str);
		if(!~reducedIndex) throw new Meteor.Error('not-found');

		var fut = new Future();

		Utils['boutiqueorders.unreduceItem'](boutiqueorder, reducedIndex, Utils.methodResponseHandler.bind(null, fut));

		return fut.wait();
	},
	'boutiqueorders.getBppDetails' : function(boutique, producerproduct_id){

		check(boutique, String);
		check(producerproduct_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		return Utils['boutiqueorders.getBppDetails'](boutique, producerproduct_id);
	},
	'boutiqueorders.noOrderCreateAndPay' : function(boutique, data, paymentType, total){

		check(boutique, String);
		check(data, {
			items : [{
				producerproduct_id : Mongo.ObjectID,
				quantity : Number
			}],
			globalorder_id : Mongo.ObjectID,
			email : Match.Optional(String)
		});
		check(paymentType, Number);
		check(total, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var items = [];
		var errors = [];
		var amountTotal = 0;
		var unitsTotal = 0;			

		data.items.forEach(function(item){
			var pp = ProducerProducts.findOne(item.producerproduct_id);
			var bpp = BoutiqueProducerProducts.findOne({ boutique : boutique, producerproduct_id : item.producerproduct_id });
			if(!pp || !bpp || bpp.quantity_available < item.quantity) return errors.push(item);
			items.push({
				producer_name : bpp.producer_name,
				product_name : bpp.product_name,
				producerproduct_id : bpp.producerproduct_id,
				producer_id : bpp.producer_id,
				unit_display : bpp.unit_display,
				tva : pp.tva,
				kelbongoo_price : pp.kelbongoo_price,
				quantity : item.quantity
			});
			unitsTotal += item.quantity;
			amountTotal += (pp.kelbongoo_price * ((100 + pp.tva)/100)) * item.quantity;
		});

		if(errors.length){
			throw new Meteor.Error('invalid-params', errors);
		}

		var fut = new Future();

		var insertData = {
			items : items,
			email : data.email,
			boutique : boutique,
			units_total : unitsTotal,
			amount_total : amountTotal,
			createdAt : new Date(),
			globalorder_id : data.globalorder_id,
			paidAt : new Date(),
			payment_data : {
				method : paymentType,
				userId : Meteor.userId()
			}
		};

		BoutiqueOrders.insert(insertData, function(err, boutiqueorder_id){
			if(err) return fut.throw(err);
			fut.return(boutiqueorder_id);
		});

		return fut.wait();
	}
});

function getPPIdString(item){
	return item.producerproduct_id._str;
}