var Future = Npm.require('fibers/future');

Meteor.methods({
	'consumers.getData' : function(consumer_id){

		check(consumer_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var consumer = Consumers.findOne(consumer_id);
		if(!consumer) throw new Meteor.Error('not-found');

		return Utils['consumers.getData'](consumer);
	}
});