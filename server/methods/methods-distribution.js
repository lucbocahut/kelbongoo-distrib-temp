var Future = Npm.require('fibers/future');

Meteor.methods({

	'distribution.markOrdersPickedUp' : function(locale, globalorder_id, couloir, orders){

		check(locale, String);
		check(globalorder_id, Mongo.ObjectID);
		check(couloir, String);
		check(orders, [Mongo.ObjectID]);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var fut = new Future();

		Utils['distribution.markOrdersPickedUp'](locale, globalorder_id, couloir, orders, Utils.methodResponseHandler.bind(undefined, fut));
	
		return fut.wait();
	},
	'distribution.markOrdersDelivered' : function(locale, globalorder_id, couloir, orders){
		
		check(locale, String);
		check(globalorder_id, Mongo.ObjectID);
		check(couloir, String);
		check(orders, [Mongo.ObjectID]);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var fut = new Future();

		Utils['distribution.markOrdersDelivered'](locale, globalorder_id, couloir, orders, Utils.methodResponseHandler.bind(undefined, fut));
	
		return fut.wait();
	},
	'distribution.markTablePlaceFree' : function(locale, globalorder_id, order_id){

		check(locale, String);
		check(globalorder_id, Mongo.ObjectID);
		check(order_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var localeplace = LocalePlaces.findOne({ locale : locale, active : true, order_id : order_id });
		if(!localeplace){
			throw new Meteor.Error('not-found');
		}

		Utils['distribution.markTablePlaceFree'](localeplace, globalorder_id, Utils.methodResponseHandler);
	}
});