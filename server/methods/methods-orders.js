var Future = Npm.require('fibers/future');

Meteor.methods({

	'orders.print' : function(locale, order_id){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ locale : locale, _id : order_id });
		if(!order){
			throw new Meteor.Error('not-found');
		}

		this.unblock();

		Utils['orders.print'](order, Utils.methodResponseHandler);
	},
	'orders.createConsumerCreditEvent' : function(locale, order_id, adjustment){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ locale : locale, _id : order_id });
		if(!order){
			throw new Meteor.Error('not-found');
		}

		this.unblock();

		Utils['orders.createConsumerCreditEvent'](order, adjustment, Utils.methodResponseHandler);
	},

	'orders.markArrived' : function(locale, order_id){
		console.log('args', arguments);
		check(locale, String);
		check(order_id, Mongo.ObjectID);
		if(!Meteor.userId()){
			throw new Meteor.Error('access-denied');
		}

		var order = Orders.findOne({ locale : locale, _id : order_id });
		if(!order){
			throw new Meteor.Error('not-found');
		}

		return order.markArrived();
	},
	'orders.reduceItem' : function(locale, producerproduct_id, order_id, quantity, percentage){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		check(quantity, Number);
		check(percentage, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ _id : order_id, locale : locale });
		if(!order) throw new Meteor.Error('not-found');

		var itemsIndex = order.items.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);
		if(!~itemsIndex) throw new Meteor.Error('not-found');

		var fut = new Future();

		Utils['orders.reduceItem'](order, producerproduct_id, quantity, percentage, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();
	},
	'orders.unreduceItem' : function(locale, producerproduct_id, order_id){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ _id : order_id, locale : locale });
		if(!order) throw new Meteor.Error('not-found');

		var reducedIndex = order.reduced.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);
		if(!~reducedIndex) throw new Meteor.Error('not-found');

		var quantity = order.reduced[reducedIndex].quantity;
		var percentage = order.reduced[reducedIndex].percentage;

		var fut = new Future();

		Utils['orders.reduceItem'](order, producerproduct_id, -quantity, percentage, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();
	},

	/*
	*	unremove the *entire* units for a given product
	*/

	'orders.unremoveItem' : function(locale, producerproduct_id, order_id){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne(order_id);
		if(!order) throw new Meteor.Error('not-found');

		var removedIndex = order.removed.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);
		if(!~removedIndex) throw new Meteor.Error('not-found');

		var itemsIndex = order.items.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);

		var gopp = GlobalOrderProducerProducts.findOne({
			globalorder_id : order.globalorder_id,
			producerproduct_id : producerproduct_id
		});
		if(!gopp) throw new Meteor.Error('not-found');

		var quantity = order.removed[removedIndex].quantity;
		if(~itemsIndex) quantity += order.items[itemsIndex].quantity;

		var fut = new Future();

		Utils['orders.updateItemQuantity'](order, itemsIndex, gopp, quantity, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();
	},
	'orders.markCouloirDistributed' : function(locale, order_id, couloir){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(couloir, Match.OneOf(0,1));
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ locale : locale, _id : order_id });
		if(!order) throw new Meteor.Error('not-found');

		Utils['orders.markCouloirDistributed'](order, couloir, Utils.methodResponseHandler);
	},
	'orders.updateItemQuantity' : function(locale, producerproduct_id, order_id, quantity){

		console.log('255', arguments);

		check(locale, String);
		check(producerproduct_id, Mongo.ObjectID);
		check(order_id, Mongo.ObjectID);
		check(quantity, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ _id : order_id, locale : locale, $or : [{ status : 1 }, {status : { $gt : 2, $lt : 7 }}] });
		if(!order) throw new Meteor.Error('not-found');

		var index = order.items.map(function(item){ return item.producerproduct_id._str; }).indexOf(producerproduct_id._str);
		if(~index && order.items.length === 1 && quantity === 0) {
			throw new Meteor.Error('last-order-item'); // i.e. would result in an empty order
		}

		var gopp = GlobalOrderProducerProducts.findOne({
			producerproduct_id : producerproduct_id,
			globalorder_id : order.globalorder_id
		});

		var diff = ~index ? quantity - order.items[index].quantity : quantity;
		if(diff === 0) throw new Meteor.Error('invalid-params');
		if(!~index && diff < 0) throw new Meteor.Error('invalid-params');

		if(!gopp || (diff > 0 && gopp.quantity_available < diff)) throw new Meteor.Error('insufficient-quantity');

		var fut = new Future();

		Utils['orders.updateItemQuantity'](order, index, gopp, quantity, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();
	},
	'orders.addProducerProduct' : function(locale, producerproduct_id, order_id, quantity){

		console.log('arguments', arguments);
		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(producerproduct_id, Mongo.ObjectID);
		check(quantity, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({

			_id : order_id,
			locale : locale,
			status : { $lt : 2 }
		});
		if(!order) throw new Meteor.Error('not-found');

		var gopp = GlobalOrderProducerProducts.findOne({

			producerproduct_id : producerproduct_id,
			globalorder_id : order.globalorder_id,
			quantity_available : { $gt : quantity }
		});
		if(!gopp) throw new Meteor.Error('not-found');

		var fut = new Future();
		console.log('347');
		Utils['orders.updateItemQuantity'](order, -1, gopp, quantity, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();
	},
	'orders.pay' : function(locale, order_id, method, amount_total){

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(method, Number);
		check(amount_total, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');
		if(!_.contains([1,2], method)) throw new Meteor.Error('invalid-params');

		var options = { fields : { _id : 1, amount_total : 1 }};

		var order = Orders.findOne({ locale : locale, _id : order_id }, options);
		if(!order) throw new Meteor.Error('not-found');

		var total = order.amount_total;

		var boutiqueorder = BoutiqueOrders.findOne({ order_id : order_id }, options);
		if(boutiqueorder) total += boutiqueorder.amount_total;

		if(total !== amount_total) throw new Meteor.Error('invalid-params');

		var params = {
			method : method
		};
		if(boutiqueorder) params.boutiqueorder_id = boutiqueorder._id;

		var fut = new Future();

		Utils['orders.pay'](order_id, params, Utils.methodResponseHandler.bind(undefined, fut));

		return fut.wait();

	},
	'orders.getBppDetails' : function(boutique, producerproduct_id){

		check(boutique, String);
		check(producerproduct_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		return Utils['orders.getBppDetails'](boutique, producerproduct_id);
	},
	'orders.markUndoTableDepartedError' : function(locale, order_id, table, place){

		console.log('243', arguments);

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		check(table, Number);
		check(place, Number);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ locale : locale, _id : order_id });
		if(!order) throw new Meteor.Error('not-found');
		console.log('order', order);

		if(!order.tabledAt || !order.tableDepartedAt){
			throw new Meteor.Error('invalid-params');
		}

		var lp = LocalePlaces.findOne({ table : table, place : place });
		if(!lp){
			throw new Meteor.Error('invalid-params');
		}

		Utils['orders.markUndoTableDepartedError'](order, lp, function(err){
			if(err) throw new Meteor.Error('method-failed', err.message);
		});
	},
	'orders.undoOrderArrival' : function(locale, order_id){

		console.log('args', arguments);

		check(locale, String);
		check(order_id, Mongo.ObjectID);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var order = Orders.findOne({ locale : locale, _id : order_id, arrivedAt : { $exists : true }});
		if(!order) throw new Meteor.Error('not-found');

		Utils['orders.undoOrderArrival'](order, function(err){
			if(err) throw new Meteor.Error('method-failed', err.message);
		});
	}
});
