var Future = Npm.require('fibers/future');

Meteor.methods({
	'tasks.updateStatus' : function(locale, globalorder_id, task_id, isDone){

		check(locale, String);
		check(globalorder_id, Mongo.ObjectID);
		check(task_id, Mongo.ObjectID);
		check(isDone, Boolean);
		if(!Meteor.userId()) throw new Meteor.Error('access-denied');

		var task = TaskInstances.findOne({ _id : task_id, 'meta.locale' : locale });
		if(!task) throw new Meteor.Error('not-found');

		var fut = new Future();

		Utils['tasks.updateStatus'](task._id, isDone, function(err){
			if(err) throw new Meteor.Error('method-failed', err.message);
			fut.return();
		});

		return fut.wait();
	}
});