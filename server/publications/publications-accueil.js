Meteor.publish('accueil.home.list', function(locale, globalorder_id, filter){
  console.log('args', arguments);
  check(locale, String);
  check(globalorder_id, Mongo.ObjectID);
  check(filter, Object);

  var selector = {
    globalorder_id,
    locale
  };

  if(filter.type === 'default'){
    selector.arrivedAt = { $exists:false },
    selector.verified = true;
    selector.status = 2;
  }
  if(filter.type === 'all-globalorder'){
    delete selector.locale;
  }
  if(filter.search){
    selector.$or = [
      { lastname: { $regex: filter.search, $options: 'i' } },
      { firstname: { $regex: filter.search, $options: 'i' } },
      { email: { $regex: filter.search, $options: 'i' } },
      { telephone: { $regex: filter.search, $options: 'i' } }
    ]
  };
  var options = {
    limit: 10,
    sort: { lastname: 1, firstname: 1 }
  }
  console.log('selector', selector);

  return Orders.find(selector, options);
});

Meteor.publish('accueil.home.recent', function(locale, globalorder_id){

  check(locale, String);
  check(globalorder_id, Mongo.ObjectID);

  return [
    Orders.find({ globalorder_id, locale, arrivedAt: { $exists: true }, tableDepartedAt: { $exists: false }}, { sort: { tabledAt: -1 }}),
    LocalePlaces.find({ locale }),
    LocaleTables.find({ locale })
  ]
});
