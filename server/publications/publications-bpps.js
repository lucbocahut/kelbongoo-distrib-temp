Meteor.publish('bpps.detail', function(boutique, producerproduct_id){

    check(producerproduct_id, Mongo.ObjectID);
    check(boutique, String);

    if(!this.userId) this.error(new Meteor.Error('access-refused'));

    return [
        BoutiqueProducerProducts.find({ producerproduct_id : producerproduct_id, boutique : boutique }),
        ProducerProducts.find({ _id : producerproduct_id })
    ];
});

Meteor.publish('bpps.active', function(boutique){

    check(boutique, String);

    if(!this.userId) this.error(new Meteor.Error('access-refused'));
    var bppCursor = BoutiqueProducerProducts.find({ 
        boutique : boutique, 
        quantity_available : { $gt : 0 }
    }, { 
        fields : { 
            producer_name : 1, product_name : 1, producerproduct_id : 1, unit_display : 1, quantity_available : 1 
        }
    });
    var ppIds = bppCursor.map(function(bpp){ return bpp.producerproduct_id; });

    return [
        bppCursor,
        ProducerProducts.find({ _id : { $in : ppIds }}, { kelbongoo_price : 1, tva : 1 })
    ];
});

SearchSource.defineSource('bpps', function(searchText, params) {

    var options = { 
        limit : 10,
        fields : {
            producer_name : 1,
            product_name : 1,
            // producerproduct_id : 1,
            // quantity_available : 1,
            unit_display : 1
        }
    };

    if(searchText) {
        var regExp = Utils.buildRegExp(searchText);
        var selector = {
            // boutique : params.boutique
            $or : [
                { product_name : regExp },
                { producer_name : regExp }
            ]
        };
        // if(params.hideTemporary){
        //     selector.temporary = false;
        // }
        if(params.badIds){
            selector.producerproduct_id = { $nin : params.badIds };
        }
        // if(params.availableOnly){
        //     selector.quantity_available = { $gt : 0 };
        // }
        // return BoutiqueProducerProducts.find(selector, options).fetch();
        return ProducerProducts.find(selector, options).fetch();
    } else {
        return [];
    }
});