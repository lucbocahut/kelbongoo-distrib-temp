Meteor.publish('caisse.home', function(locale, globalorder_id, filter){
  check(locale, String);
  check(globalorder_id, Mongo.ObjectID);
  check(filter, Object);

  var selector = {
    globalorder_id,
    locale
  };

  if(filter.type === 'default'){
    selector.tableDepartedAt = { $exists:true },
    selector.verified = true;
    selector.status = 3;
  }
  if(filter.type === 'all-globalorder'){
    delete selector.locale;
  }
  if(filter.search){
    selector.$or = [
      { lastname: { $regex: filter.search, $options: 'i' } },
      { firstname: { $regex: filter.search, $options: 'i' } },
      { email: { $regex: filter.search, $options: 'i' } },
      { telephone: { $regex: filter.search, $options: 'i' } }
    ]
  };
  var options = {
    limit: 10,
    sort: { lastname: 1, firstname: 1 }
  }
  console.log('selector', selector);

  return Orders.find(selector, options);
})
