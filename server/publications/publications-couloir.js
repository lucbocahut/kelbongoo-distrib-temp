Meteor.publishComposite('couloir.home', function(locale, globalorder_id, couloir){
  console.log('couloir.home args', arguments);
    check(locale, String);
    check(globalorder_id, Mongo.ObjectID);
    if(!this.userId){
      this.error(new Meteor.Error('access-refused'));
    }

    this.unblock();

    return {
        find : function(){

            this.unblock();

            return Orders.find({
              locale,
              globalorder_id,
              status: 3
            });
        },
        children : [{

          find : function(order){

              this.unblock();
              return OrderBacs.find({ couloir, order_id: order._id })
          }
        }, {
          find : function(order){

              this.unblock();
              return OrderItems.find({ order_id: order._id })
          }
        }, {
          find : function(order){

              this.unblock();
              return OrderReducedItems.find({ order_id: order._id })
          }
        }, {
          find: function(order){
            this.unblock();
            return LocalePlaces.find({ order_id: order._id });
          }
        }]
    };
});
