// Meteor.publish('home', function(locale, globalorder_id, couloir){
//
//     check(locale, String);
//     check(globalorder_id, Mongo.ObjectID);
//     if(!this.userId) this.error(new Meteor.Error('access-refused'));
//
//     return [
//         GlobalOrders.find({ locales : locale, _id : globalorder_id })
//     ];
// });

// Meteor.publishComposite("caisse.home", function (tableName, ids, fields) {
//
//     check(tableName, String);
//     check(ids, Array);
//     check(fields, Match.Optional(Object));
//
//     this.unblock();
//
//     return {
//
//         find: function () {
//
//             this.unblock();
//
//             return Orders.find({ _id : { $in : ids }}, { fields : fields });
//         },
//
//         children: [{
//
//             find: function(order) {
//
//                 this.unblock(); // requires meteorhacks:unblock package
//                 // Publish the related user
//                 return BoutiqueOrders.find({ order_id : order._id }, { fields : { amount_total : 1, order_id : 1 }});
//             }
//         }]
//     };
// });

Meteor.publish('caisse.boutiqueorders.detail', function(locale, boutiqueorder_id){

    check(locale, String);
    check(boutiqueorder_id, Mongo.ObjectID);
    if(!this.userId) this.error(new Meteor.Error('access-refused'));

    return BoutiqueOrders.find({ boutique : locale, _id : boutiqueorder_id });
});

// Meteor.publish('couloir.home', function(locale, globalorder_id){
//
//     check(locale, String);
//     check(globalorder_id, Mongo.ObjectID);
//     if(!this.userId) this.error(new Meteor.Error('access-refused'));
//
//     return [
//         LocalePlaces.find({ locale : locale, active : true }),
//         Orders.find({
//             arrivedAt : { $exists : true },
//             globalorder_id : globalorder_id,
//             locale : locale,
//             $or : [
//                 { fraisDeliveredAt : { $exists : false }},
//                 { secDeliveredAt : { $exists : false }},
//             ]
//         })
//     ];
// });

// Meteor.publishComposite('table.home', function(locale, globalorder_id, table){
//
//     check(locale, String);
//     check(globalorder_id, Mongo.ObjectID);
//     check(table, Number);
//     if(!this.userId) this.error(new Meteor.Error('access-refused'));
//
//     this.unblock();
//
//     return {
//         find : function(){
//
//             this.unblock();
//
//             return LocalePlaces.find({ locale : locale, table : table });
//         },
//         children : [{
//
//             find : function(place){
//
//                 this.unblock();
//                 return Orders.find({ _id : place.order_id, locale : locale, globalorder_id : globalorder_id })
//             }
//         }]
//     };
// });
//
// Meteor.publish('accueil.home', function(locale, globalorder_id){
//
//     check(locale, String);
//     check(globalorder_id, Mongo.ObjectID);
//     if(!this.userId) this.error(new Meteor.Error('access-refused'));
//
//     return [
//         Locales.find({ code : locale }),
//         LocalePlaces.find({ locale : locale }),
//         Orders.find({ arrivedAt : { $exists : true }, globalorder_id : globalorder_id, locale : locale })
//     ];
// });
