Meteor.publish('globalorders', function(){

    if(!this.userId) {
      return this.error(new Meteor.Error('access-refused'));
    }

    var goCursor = { status : 4 };
    var goOptions = {
        sort : { distribution_date : -1 },
        limit: 5
    };

    var goIds = GlobalOrders.find(goCursor, goOptions)
                  .map(go => go._id);
    console.log('goIds', goIds);

    var golCursor = GlobalOrderLocales.find({
        status : 4,
        globalorder_id : { $in : goIds }
    }, {
        fields : { locale : 1, distribution : 1, globalorder_id : 1 }
    });

    return [
        golCursor,
        Locales.find({}, { fields : { name : 1, code : 1, table_aliases : 1 }})
    ];
});

Meteor.publish('globalorders.detail', function(locale, globalorder_id){

    check(locale, String);
    check(globalorder_id, Mongo.ObjectID);
    if(!this.userId) this.error(new Meteor.Error('access-refused'));

    return [
        GlobalOrderLocales.find({ globalorder_id : globalorder_id }, { fields : { distribution : 1, globalorder_id : 1, status : 1 }}),
        GlobalOrders.find(globalorder_id),
        Locales.find({ code : locale }),
        LocaleTables.find({ locale, active: true }),
        LocalePlaces.find({ locale, active: true })
    ];
});

Meteor.publish('globalorders.detail.counts', function(locale, globalorder_id){

    check(locale, String);
    check(globalorder_id, Mongo.ObjectID);
    if(!this.userId){
      this.error(new Meteor.Error('access-refused'));
    }

    var self = this;
    var init = true;

    var total = Orders.find({
        globalorder_id : globalorder_id,
        status : { $gt : 1, $lt : 5 },
        locale : locale
    }).count();

    var arrivedCount = 0;
    var departedCount = 0;

    var ordersHandle = Orders.find({
        globalorder_id : globalorder_id,
        status : { $gt : 1, $lt : 5 },
        locale : locale,
        arrivedAt : { $exists : true }
    }, { fields : { tableDepartedAt : 1 }}).observe({

        added: function (doc) {

            arrivedCount++;
            if(doc.tableDepartedAt) departedCount++;
            if (!init){
                self.changed("counts", globalorder_id, { arrived : arrivedCount, departed : departedCount });
            }
        },
        changed : function(doc){
            departedCount++;
            if (!init){
                self.changed("counts", globalorder_id, { arrived : arrivedCount, departed : departedCount });
            }
        }
    });

    init = false;
    self.added("counts", globalorder_id, { arrived : arrivedCount, departed : departedCount, total : total });
    self.ready();


    self.onStop(function () {
        ordersHandle.stop();
    });
});
