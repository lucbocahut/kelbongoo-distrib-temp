Meteor.publish('home', function(locale_code){

    check(locale_code, String);
    if(!this.userId) this.error(new Meteor.Error('access-refused'));

    var selector = { 
        locales : locale_code,
        status : { $gt : 0, $lt : 7 }
    };
    var options = { 
        sort : { distribution_date : -1 },
        fields : { distribution_date : 1, status : 1 }
    };

    return GlobalOrders.find(selector, options);
});