Meteor.publish('orders.base', function(){
  return ConsumerCreditEventTypes.find();
})

Meteor.publishComposite('orders.detail', function(locale, order_id, consumer_id){
  console.log('args orders.detail', arguments);
    check(locale, String);
    check(order_id, Mongo.ObjectID);
    check(consumer_id, Match.Optional(Mongo.ObjectID));
    if(!this.userId) {
      this.error(new Meteor.Error('access-refused'));
    }

    this.unblock();

    return {
        find : function(){

            this.unblock();
            console.log('count', Orders.find({ locale : locale, _id: order_id }).count())

            return Orders.find({ locale : locale, _id: order_id });
        },
        children : [{

            find : function(order){

                this.unblock();
                return BoutiqueOrders.find({ order_id : order._id })
            }
        }, {

            find : function(order){

                this.unblock();
                return ConsumerCreditEvents.find({ 'meta.order_id' : order_id })
            }
        }]
    };
});

SearchSource.defineSource('orders.bpps', function(searchText, params) {

    var options = {
        limit : 6,
        fields : {
            producer_name : 1,
            product_name : 1,
            producerproduct_id : 1,
            quantity_available : 1
        }
    };

    if(searchText) {
        var regExp = Utils.buildRegExp(searchText);
        var selector = {
            product_name : regExp,
            boutique : params.boutique
        };
        if(params.badIds){
            selector.producerproduct_id = { $nin : params.badIds };
        }
        if(params.availableOnly){
            selector.quantity_available = { $gt : 0 };
        }

        return BoutiqueProducerProducts.find(selector, options).fetch();
    } else {
        return [];
    }
});
