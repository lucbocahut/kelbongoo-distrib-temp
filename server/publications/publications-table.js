Meteor.publish('table.base', function(locale){
  return LocaleTables.find();
})

Meteor.publishComposite('table.home', function(locale, globalorder_id, table){

    check(locale, String);
    check(globalorder_id, Mongo.ObjectID);
    check(table, Number);
    if(!this.userId){
      this.error(new Meteor.Error('access-refused'));
    }

    this.unblock();

    return {
        find : function(){

            this.unblock();

            return LocalePlaces.find({ locale : locale, table : table }, { sort: { number: 1 }});
        },
        children : [{

            find : function(place){

                this.unblock();
                return Orders.find({ _id : place.order_id, locale : locale, globalorder_id : globalorder_id })
            },
            children: [{
              find: function(order){
                this.unblock();
                return OrderItems.find({ order_id: order._id });
              }
            },{
              find: function(order){
                this.unblock();
                return OrderReducedItems.find({ order_id: order._id });
              }
            }, {
              find: function(order){
                this.unblock();
                return OrderRemovedItems.find({ order_id: order._id });
              }
            },{
              find: function(order){
                this.unblock();
                return OrderBacs.find({ order_id: order._id });
              }
            }]
        }]
    };
});
